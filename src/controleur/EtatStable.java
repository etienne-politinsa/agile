package controleur;

import modele.EnsembleDeTournees;
import vue.Fenetre;


/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatStable extends EtatDefaut {

	@Override
	public void calculerTournees(EnsembleDeTournees edt, Controleur c, Fenetre fenetre) {
		c.setEtatCourant(c.etatCalculEnCours);
		Thread fred = new Thread(new MyThreadPerso(c, edt));
		fred.start();

		fenetre.ActiverBouton(Fenetre.CHARGER_PLAN, false);
		fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, false);
		fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
		fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.INTERROMPRE_LIVRAISON, true);
	}
	
	@Override
	public void ajouter(Controleur c) {
		c.setEtatCourant(c.etatAjoutNoeud);
		c.getFenetre().afficheMessage("Veuillez cliquer là où vous voulez ajouter une livraison.");
		c.getFenetre().ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, false);
		c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.CHARGER_PLAN, false);
	}

	@Override
	public void deplacer(Controleur c) {
		c.setEtatCourant(c.etatDeplacerPdl);
		c.getFenetre().afficheMessage("Veuillez cliquer sur le point de livraison à changer.");
		c.getFenetre().ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, false);
		c.getFenetre().ActiverBouton(Fenetre.CHARGER_PLAN, false);
		c.getFenetre().ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		c.getFenetre().ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
	}

	@Override
	public void supprimer(Controleur c) {
		Fenetre fenetre = c.getFenetre();
		fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.CHARGER_PLAN, false);
		fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, false);
		fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
		fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		c.setEtatCourant(c.etatSupprimer);
		fenetre.afficheMessage("Veuillez cliquer sur le point de livraison à supprimer.");
	}


	@Override public void undo(ListeDeCdes l) {
		try {
			l.undo();
		} catch (Exception e) {
		}
	}

	@Override public void redo(ListeDeCdes l) {
		try {
			l.redo();
		} catch (Exception e) {
		}
	}
}
