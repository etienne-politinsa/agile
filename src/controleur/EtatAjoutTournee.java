package controleur;

import modele.Noeud;
import modele.PointDeLivraison;
import modele.Tournee;
import vue.Fenetre;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatAjoutTournee extends EtatDefaut {

	private PointDeLivraison pdl;
	private int duree;
	private Tournee tournee;

	private String messageErreurSelection = "Le point de livraison sélectionné n'est pas accessible.";

	@Override
	public void clicGauche(double longi, double lat, Controleur c) {
		Fenetre fenetre = c.getFenetre();
		duree = fenetre.getDureeAddPdl();
		Noeud noeud = c.etatAjoutNoeud.getNoeud();
		tournee = c.getEnsembleDeTournees().getTourneeLaPlusProche(longi, lat);
		if(tournee==null) {
			try {
				c.getListeDeCdes().ajoute(new CdeAjout(tournee, c.getEnsembleDeTournees(), c.getPlan(), noeud, c.getDemandeDeLivraison(), duree));
				pdl = c.getDemandeDeLivraison().getPointDeLivraisonParNoeud(noeud);
				fenetre.afficheMessage(EtatAjoutModif.messageFlecheEntree);
				c.setEtatCourant(c.etatAjoutModif);
			} catch (Exception e) {
				fenetre.afficheMessage(messageErreurSelection);
				try {
					c.getListeDeCdes().annule();
				} catch (Exception ex) {
				}
				c.setEtatCourant(c.etatStable);
				fenetre.ActiverBouton(Fenetre.CHARGER_PLAN, true);
				fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
				fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
				fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, true);
				fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
				fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
				fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
			} finally {
				fenetre.cleanNoeudTemporaire();
			}
		}
		else {
			try {
				c.getListeDeCdes().ajoute(new CdeAjout(tournee, c.getEnsembleDeTournees(), c.getPlan(), noeud, c.getDemandeDeLivraison(), duree));
				pdl = c.getDemandeDeLivraison().getPointDeLivraisonParNoeud(noeud);
				c.getFenetre().afficheMessage(EtatAjoutModif.messageFlecheEntree);
				c.setEtatCourant(c.etatAjoutModif);
			} catch (Exception e) {
				fenetre.afficheMessage(messageErreurSelection);
				try {
					c.getListeDeCdes().annule();
				} catch (Exception ex) {
				}
				c.setEtatCourant(c.etatStable);
				fenetre.ActiverBouton(Fenetre.CHARGER_PLAN, true);
				fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
				fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
				fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
				fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
				fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
				fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
			} finally {
				fenetre.cleanNoeudTemporaire();
			}
		}
	}

	public PointDeLivraison getPdl() {
		return pdl;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public void setPdl(PointDeLivraison pdl) {
		this.pdl = pdl;
	}

	public Tournee getTournee() {
		return tournee;
	}
}
