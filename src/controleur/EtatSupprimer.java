package controleur;

import modele.PointDeLivraison;
import modele.Tournee;
import vue.Fenetre;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatSupprimer extends EtatDefaut {

	private PointDeLivraison pdl;
	private Tournee t;
	@Override
	public void clicGauche(double longi, double lat, Controleur c) {
		if(c.getEnsembleDeTournees().getListeTournee().size() == 0) {
			c.getFenetre().afficheMessage("Il n'y a rien à supprimer.");
			c.getFenetre().ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
			c.setEtatCourant(c.etatStable);
			return;
		}
		pdl = c.getDemandeDeLivraison().getPointDeLivraisonLePlusProche(longi, lat);
		t = c.getEnsembleDeTournees().getTourneeParPointDeLivraison(pdl);
		try {
			c.getListeDeCdes().ajoute(new CdeSupprime(pdl, c.getPlan(), t, c.getEnsembleDeTournees(), c.getDemandeDeLivraison(), pdl));
			c.getFenetre().afficheMessage("Choisissez une action...");
		} catch (Exception e) {
			e.printStackTrace();
			c.getFenetre().afficheMessage("Erreur lors de la suppression du point de livraison.");
			try {
				c.getListeDeCdes().annule();
			} catch (Exception ex) {
			}
		} finally {
			c.getFenetre().ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
			c.getFenetre().ActiverBouton(Fenetre.CALCULER_TOURNEES, true);
			c.getFenetre().ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.CHARGER_PLAN, true);
			if(c.getEnsembleDeTournees().getListeTournee().size() > 1) {
				c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
			}
			else { 
				c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
			}
			c.getFenetre().setDesequilibre(c.getEnsembleDeTournees().isDesequilibre());
			c.setEtatCourant(c.etatStable);

		}
	}
}
