package controleur;

import java.util.ArrayList;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Noeud;
import modele.Pair;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Tournee;
import modele.Trajet;

/**
 * @author Etienne DELAHAYE & Loïc CASTELLON
 *
 */
public class CdeAjout implements Commande {

	private Tournee tournee;
	private PointDeLivraison pdl;
	private int position = -1;
	private int duree;
	private EnsembleDeTournees edt;
	private Plan p;
	private Noeud noeud;
	private DemandeDeLivraison dedel;

	/**
	 * Commande d'ajout d'un point de livraison
	 * @param tournee
	 * @param edt
	 * @param p
	 * @param noeud
	 * @param dedel
	 * @param d
	 */
	public CdeAjout(Tournee tournee, EnsembleDeTournees edt, Plan p, Noeud noeud, DemandeDeLivraison dedel, int d) {
		this.tournee = tournee;
		this.edt = edt;
		this.p = p;
		this.noeud = noeud;
		this.dedel = dedel;
		duree = d;
	}


	@Override
	public void doCde() throws Exception {
		if (tournee==null || tournee.getListePointTrajet().size() == 0) {
			Double d = Double.valueOf(0.0);
			ArrayList<Pair<PointDeLivraison,Trajet>> pair = new ArrayList<Pair<PointDeLivraison,Trajet>>();
			tournee = new Tournee(d,0,pair);
			pdl = edt.ajouterPointDeLivraisonNouvelleTournee(noeud, duree, p, dedel.getEntrepot(),tournee);
		}
		else {
			pdl = edt.ajouterPointDeLivraison(noeud, duree, p, tournee);
			int positionActuelle = tournee.getIndexOfPointDeLivraison(pdl);
			if (position != -1) {
				while (positionActuelle != position) {
					edt.avancerPointDeLivraison(pdl);
					positionActuelle = tournee.getIndexOfPointDeLivraison(pdl);

				}
				position = positionActuelle;
			}
		}
	}

	@Override
	public void undoCde() throws Exception {
		position = tournee.getIndexOfPointDeLivraison(pdl);
		tournee.setEstSelectionne(false);
		edt.supprimerPointDeLivraison(pdl, p, tournee);
	}
}
