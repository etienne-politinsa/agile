package controleur;

import modele.Noeud;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatAjoutNoeud extends EtatDefaut {
	private Noeud noeud;

	@Override
	public void clicGauche(double longi, double lat, Controleur c) {
		noeud = c.getPlan().getNoeudLePlusProche(longi, lat);
		c.getFenetre().afficherNoeud(noeud);
		c.setEtatCourant(c.etatAjoutTournee);
		c.getFenetre().afficheMessage("Veuillez cliquer sur la tournee qui devra récupérer le nouveau point de livraison.");
	}

	public Noeud getNoeud() {
		return noeud;
	}
}