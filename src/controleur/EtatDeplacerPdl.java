package controleur;

import modele.PointDeLivraison;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatDeplacerPdl extends EtatDefaut {
	private PointDeLivraison pdl;

	@Override
	public void clicGauche(double longi, double lat, Controleur c) {
		pdl = c.getDemandeDeLivraison().getPointDeLivraisonLePlusProche(longi, lat);
		c.getFenetre().afficheMessage("Veuillez cliquer sur la tournée qui récupérera le point de livraison.");
		c.setEtatCourant(c.etatDeplacerTournee);
	}

	public PointDeLivraison getPdl() {
		return pdl;
	}
}
