package controleur;

/**
 * @author Etienne DELAHAYE & Loïc CASTELLON
 *
 */
interface Commande {
	public void doCde() throws Exception;
	public void undoCde() throws Exception;
}