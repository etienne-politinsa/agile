package controleur;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Plan;
import vue.Fenetre;

/**
 * @author Etienne DELAHAYE
 *
 */
public class Controleur {
	private Etat etatCourant;

	// Objets du modèle
	private Plan plan;
	private DemandeDeLivraison demandeDeLivraison;
	private EnsembleDeTournees ensembleDeTournees;
	private Fenetre fenetre;

	// Etat
	final EtatInit etatInit = new EtatInit();
	final EtatCalculEnCours etatCalculEnCours = new EtatCalculEnCours();
	final EtatStable etatStable = new EtatStable();
	final EtatAjoutNoeud etatAjoutNoeud = new EtatAjoutNoeud();
	final EtatAjoutTournee etatAjoutTournee = new EtatAjoutTournee();
	final EtatAjoutModif etatAjoutModif = new EtatAjoutModif();
	final EtatSupprimer etatSupprimer = new EtatSupprimer();
	final EtatDeplacerPdl etatDeplacerPdl = new EtatDeplacerPdl();
	final EtatDeplacerTournee etatDeplacerTournee = new EtatDeplacerTournee();

	// Commande
	private ListeDeCdes listeDeCdes = new ListeDeCdes(); 

	/**
	 * 
	 * @param plan
	 * @param demandeDeLivraison
	 * @param esd
	 */
	public Controleur(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees esd) {
		etatCourant = etatInit;
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		this.ensembleDeTournees = esd;
		fenetre = new Fenetre(plan,demandeDeLivraison, esd, this);
		fenetre.ActiverBouton(Fenetre.CHARGER_PLAN, true);
		fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, false);
		fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
		fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.INTERROMPRE_LIVRAISON, false);
	}

	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Charger un Plan"
	 */
	public void chargerPlan() {
		etatCourant.chargerPlan(fenetre, this);
	}
	
	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Charger une Demande de Livraison"
	 */
	public void chargerLivraisons() {
		etatCourant.chargerLivraisons(this);
	}
	
	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Renseigner le nombre de livreurs"
	 */
	public void renseignerNbLivreurs(){
		etatCourant.renseignerNbLivreurs(demandeDeLivraison, fenetre, this);
	}
	
	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Calculer les tournées"
	 */
	public void chargerTournees(){
		etatCourant.calculerTournees(ensembleDeTournees, this, fenetre);
	}

	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Ajouter une livraison"
	 */
	public void ajouter() {
		etatCourant.ajouter(this);
	}

	/**
	 * Méthode appelée par la fenêtre après un clique gauche
	 * @param longi
	 * @param lat
	 */
	public void clicGauche(double longi, double lat) {
		etatCourant.clicGauche(longi, lat, this);
	}

	/**
	 * Méthode appelée par la fenêtre après un clique droit
	 * @param longi
	 * @param lat
	 */
	public void clicDroit(double longi, double lat) {
		etatCourant.clicDroit(longi, lat, this);
	}

	/**
	 * Méthode appelée par la fenêtre après un Ctrl+clique gauche
	 * @param longi
	 * @param lat
	 */
	public void controleClicGauche(double longi, double lat) {
		etatCourant.controleClicGauche(longi, lat, this);
	}

	/**
	 * Méthode appelée par la fenêtre après un appui sur flèche haut
	 */
	public void flecheHaut() {
		etatCourant.flecheHaut(this);
	}

	/**
	 * Méthode appelée par la fenêtre après un appui sur flèche bas
	 */
	public void flecheBas() {
		etatCourant.flecheBas(this);
	}

	/**
	 * Méthode appelée par la fenêtre après un appui sur Entrée
	 */
	public void finModif() {
		etatCourant.finModif(this);
	}

	/**
	 * Méthode appelée par la vue en cas de clic sur la Liste sur "Tout voir"
	 * @param afficheEntrepot
	 * @param afficheLivraison
	 * @param afficheTournee
	 * @param isAll
	 */
	public void controleAffichageEnsembleVueTextuelle(boolean afficheEntrepot, boolean afficheLivraison, boolean afficheTournee, boolean isAll) {
		etatCourant.controleAffichageEnsembleVueTextuelle(afficheEntrepot, afficheLivraison, afficheTournee,isAll, this);
	}

	/**
	 * Méthode appelée par la vue en cas de clic sur la Liste sur une entité en particulier
	 * @param index
	 * @param type
	 */
	public void selectionEntiteParticuliere(int index, String type) {
		etatCourant.selectionEntiteParticuliere(index, type, this);
	}

	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Supprimer une livraison"
	 */
	public void supprimer() {
		etatCourant.supprimer(this);
	}

	/**
	 * Méthode appelée par la fenêtre après un clique sur le bouton "Déplacer une livraison"
	 */
	public void deplacer() {
		etatCourant.deplacer(this);
	}
	/**
	 * Méthode appelée par la vue en cas de clic sur le bouton "Interrompre le calcul"
	 * */
	public void stopCalcul() {
		etatCourant.stopCalcul(this);
	}

	/**
	 * Methode appelée par la fenetre quand l'utilisateur fait un Ctrl+Z
	 */
	public void undo(){
		etatCourant.undo(listeDeCdes);
	}

	/**
	 * Methode appelee par fenetre quand l'utilisateur fait un Ctrl+Y
	 */
	public void redo(){
		etatCourant.redo(listeDeCdes);
	}

	/**
	 * Change l'état interne du Controleur
	 * @param etat
	 */
	void setEtatCourant(Etat etat) {
		etatCourant = etat;
	}

	public Plan getPlan() {
		return plan;
	}

	/**
	 * Change le plan du controleur
	 * @param plan
	 */
	void setPlan(Plan plan) {
		this.plan = plan;
	}

	/**
	 * 
	 * @return demandeDeLivraison
	 */
	DemandeDeLivraison getDemandeDeLivraison() {
		return demandeDeLivraison;
	}

	/**
	 * 
	 * @param demandeDeLivraison
	 */
	void setDemandeDeLivraison(DemandeDeLivraison demandeDeLivraison) {
		this.demandeDeLivraison = demandeDeLivraison;
	}

	/**
	 * 
	 * @return ensembleDeTournees
	 */
	EnsembleDeTournees getEnsembleDeTournees() {
		return ensembleDeTournees;
	}

	/**
	 * 
	 * @param ensembleDeTournees
	 */
	void setEnsembleDeTournees(EnsembleDeTournees ensembleDeTournees) {
		this.ensembleDeTournees = ensembleDeTournees;
	}

	/**
	 * 
	 * @return fenetre
	 */
	Fenetre getFenetre() {
		return fenetre;
	}

	/**
	 * 
	 * @param fenetre
	 */
	void setFenetre(Fenetre fenetre) {
		this.fenetre = fenetre;
	}

	/**
	 * 
	 * @return liste de Commandes
	 */
	ListeDeCdes getListeDeCdes() {
		return listeDeCdes;
	}

	/**
	 * @return etatCourant Etat actuel du controleur
	 */
	Etat getEtatCourant() {
		return etatCourant;
	}
}
