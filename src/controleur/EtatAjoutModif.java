package controleur;

import modele.PointDeLivraison;
import vue.Fenetre;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatAjoutModif extends EtatDefaut {

	static String messageFlecheEntree = "Utilisez flèche haut/bas pour changer la position de la livraison dans la tournée. Appuyer sur Entrée pour valider.";
	static String messageErreur = "Vous ne pouvez pas modifier ce point de livraison dans cette direction. Utilisez les flèches ou Entrée pour valider.";
	@Override
	public void flecheHaut(Controleur c) {
		PointDeLivraison pdl = c.etatAjoutTournee.getPdl();
		try {
			c.getEnsembleDeTournees().avancerPointDeLivraison(pdl);
			c.getFenetre().afficheMessage(messageFlecheEntree);
		} catch (Exception e) {
			c.getFenetre().afficheMessage(messageErreur);
		}
	}

	@Override
	public void flecheBas(Controleur c) {
		PointDeLivraison pdl = c.etatAjoutTournee.getPdl();
		try {
			c.getEnsembleDeTournees().reculerPointDeLivraison(pdl);
			c.getFenetre().afficheMessage(messageFlecheEntree);
		} catch (Exception e) {
			c.getFenetre().afficheMessage(messageErreur);
		}
	}

	@Override
	public void finModif(Controleur c) {
		c.setEtatCourant(c.etatStable);
		c.getFenetre().afficheMessage("Choisissez une action...");
		if(c.getEnsembleDeTournees().getListeTournee().size() > 1) {
			c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
		}
		else {
			c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		}
		c.getFenetre().ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
		c.getFenetre().ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
		c.getFenetre().ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
		c.getFenetre().ActiverBouton(Fenetre.CALCULER_TOURNEES, true);
		c.getFenetre().ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
		c.getFenetre().ActiverBouton(Fenetre.CHARGER_PLAN, true);
		c.getFenetre().setDesequilibre(c.getEnsembleDeTournees().isDesequilibre());
	}
}