package controleur;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Tournee;
import modele.Troncon;
import vue.Fenetre;
import xml.DeserialiseurXMLDemandeDeLivraison;
import xml.DeserialiseurXMLPlan;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatDefaut implements Etat {

	@Override
	public void chargerPlan(Fenetre fenetre, Controleur c) {
		reinitialiser(c, true);
		try {
			DeserialiseurXMLPlan.chargerPlan(c.getPlan());
			c.setEtatCourant(c.etatInit);
			fenetre.afficheMessage("Plan chargé.");
			fenetre.setDesequilibre(false);
			fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
		} catch (Exception e) {
			fenetre.afficheMessage("Erreur lors de l'ouverture du plan.");
			fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, false);
		}
	}

	@Override
	public void chargerLivraisons(Controleur c) {
		Fenetre fenetre = c.getFenetre();
		reinitialiser(c, false);
		try {
			DeserialiseurXMLDemandeDeLivraison.chargerDemandeDeLivraison(c.getDemandeDeLivraison(), c.getPlan());
			c.setEtatCourant(c.etatStable);
			fenetre.afficheMessage("Livraisons chargées.");
			fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
			fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, true);
			c.setEnsembleDeTournees(new EnsembleDeTournees(c.getPlan(), c.getDemandeDeLivraison()));
			fenetre.setEnsembleDeTournees(c.getEnsembleDeTournees());
		} catch (Exception e) {
			fenetre.afficheMessage("Erreur lors du chargement des livraisons.");
		}
	}

	@Override
	public void renseignerNbLivreurs(DemandeDeLivraison d, Fenetre fenetre, Controleur c) {
		d.setNombreLivreurs(fenetre.getValueNbLivreur());
		fenetre.setNbLivreursDsFenetre(fenetre.getValueNbLivreur());
		c.setEtatCourant(c.etatStable);
		fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, true);
		fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
	}

	@Override
	public void calculerTournees(EnsembleDeTournees edt, Controleur c, Fenetre f) {}

	@Override
	public void stopCalcul(Controleur c) {}

	private void reinitialiser(Controleur c, boolean plan) {
		Fenetre fenetre = c.getFenetre();
		fenetre.setTourneeSelectionee(null);
		if (plan) {
			c.setEtatCourant(c.etatInit);
			c.setPlan(new Plan(agile.Agile.largeurPlan, agile.Agile.hauteurPlan));
			fenetre.setPlan(c.getPlan());
		}
		else {
			c.setEtatCourant(c.etatInit);
		}
		c.setDemandeDeLivraison(new DemandeDeLivraison());
		fenetre.setDemandeDeLivraison(c.getDemandeDeLivraison());
		c.setEnsembleDeTournees(new EnsembleDeTournees(c.getPlan(), c.getDemandeDeLivraison()));
		fenetre.setEnsembleDeTournees(c.getEnsembleDeTournees());
		fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, false);
		fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
		fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, false);
		fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
	}

	@Override
	public void clicGauche(double longi, double lat, Controleur c) {
		Troncon tronconProche = c.getPlan().getTronconLePlusProche(longi, lat);
		if(tronconProche!=null)
			c.getFenetre().selectionnerTroncon(tronconProche);

	}

	@Override
	public void controleClicGauche(double longi, double lat, Controleur c) {
		PointDeLivraison pdl = c.getDemandeDeLivraison().getPointDeLivraisonLePlusProche(longi,lat);
		int index=0;
		if(pdl!=null) {
			for (int i=0; i<c.getDemandeDeLivraison().getPointsDeLivraison().size(); i++) {
				c.getDemandeDeLivraison().setEstSelectionne(i, false);
				if(pdl.getId().equals(c.getDemandeDeLivraison().getPointsDeLivraison().get(i).getId())) {
					index = i;
				}
			}
			c.getDemandeDeLivraison().setEstSelectionne(index, true);
			c.getFenetre().controleAffichageEnsembleVueTextuelle(false, true, false);
		}	
	}

	@Override
	public void clicDroit(double longi, double lat, Controleur c) {
		Tournee tourneeProchee = (c.getEnsembleDeTournees()).getTourneeLaPlusProche(longi, lat);

		if(tourneeProchee != null)
		{
			c.controleAffichageEnsembleVueTextuelle(false,false,true,false);
			c.selectionEntiteParticuliere(c.getEnsembleDeTournees().getIndex(tourneeProchee),"tournee");
			c.getFenetre().selectionnerTournee(tourneeProchee);
		}
	}

	@Override
	public void ajouter(Controleur c) {}

	@Override
	public void supprimer(Controleur c) {}

	@Override
	public void deplacer(Controleur c) {}

	@Override
	public void controleAffichageEnsembleVueTextuelle(boolean afficheEntrepot, boolean afficheLivraison, boolean afficheTournee, boolean isAll, Controleur c) {

		for (int i=0; i < c.getDemandeDeLivraison().getPointsDeLivraison().size(); i++) {
			c.getDemandeDeLivraison().setEstSelectionne(i, false);
		}
		for (int i=0; i < c.getEnsembleDeTournees().getListeTournee().size(); i++) {
			c.getEnsembleDeTournees().setEstSelectionne(i, false);
		}


		if (afficheEntrepot && isAll) {
			c.getDemandeDeLivraison().setEntrepotSelectionne(afficheEntrepot);
		}

		if (afficheLivraison && isAll) {
			for (int i=0; i < c.getDemandeDeLivraison().getPointsDeLivraison().size(); i++) {
				c.getDemandeDeLivraison().setEstSelectionne(i, true);
			}
		}			
		if (afficheTournee && isAll) {
			for (int i=0; i < c.getEnsembleDeTournees().getListeTournee().size(); i++) {
				c.getEnsembleDeTournees().setEstSelectionne(i, true);
			}
		}

		c.getFenetre().controleAffichageEnsembleVueTextuelle(afficheEntrepot, afficheLivraison, afficheTournee);
	}

	@Override
	public void selectionEntiteParticuliere(int index, String type, Controleur c) {
		if (type.equals("tournee")) {
			for (int i=0; i<c.getEnsembleDeTournees().getListeTournee().size(); i++) {
				c.getEnsembleDeTournees().setEstSelectionne(i, false);
			}
			c.getEnsembleDeTournees().setEstSelectionne(index, true);
		}
		else if (type.equals("livraison")) {
			for (int i=0; i<c.getDemandeDeLivraison().getPointsDeLivraison().size(); i++) {
				c.getDemandeDeLivraison().setEstSelectionne(i, false);
			}
			c.getDemandeDeLivraison().setEstSelectionne(index, true);
		}
	}

	@Override
	public void flecheHaut(Controleur c) {}

	@Override
	public void flecheBas(Controleur c) {}

	@Override
	public void finModif(Controleur c) {}

	@Override
	public void undo(ListeDeCdes listeDeCdes) {}

	@Override
	public void redo(ListeDeCdes listeDeCdes) {}
}
