package controleur;

import modele.EnsembleDeTournees;

/**
 * Class qui implemente Runnable et dont la méthode run lance un calcul
 * de TSP et gère son retour lorsqu'il est terminé (normalement ou par interruption) 
 * 
 * @author Etienne DELAHAYE
 *
 */
class MyThreadPerso implements Runnable {

	Controleur c;
	EnsembleDeTournees edt;

	MyThreadPerso(Controleur c, EnsembleDeTournees edt) {
		this.c = c;
		this.edt = edt;
	}

	@Override
	public void run() {
		boolean done = edt.calculer();
		try {
			edt.miseAJourMeilleureSolution();
		} catch (Exception e) {
			c.getFenetre().afficheMessage("Aucune solution n'a été trouvée pour le moment.");
			done = false;
		}
		if (done) {
			if (c.getEtatCourant() == c.etatCalculEnCours) {
				c.getFenetre().afficheMessage("Choisissez une action...");
				c.stopCalcul();
			}
		}
	}

}
