package controleur;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Noeud;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Tournee;

/**
 * @author Etienne DELAHAYE & Loïc CASTELLON
 *
 */
public class CdeSupprime implements Commande {

	private Tournee tournee;
	private PointDeLivraison pdl;
	private int position = -1;
	private int duree;
	private EnsembleDeTournees edt;
	private Plan p;
	private Noeud noeud;
	private DemandeDeLivraison dedel;

	/**
	 * Commande de suppresion d'un point de livraison
	 * @param pdl
	 * @param plan
	 * @param t
	 * @param edt
	 * @param dedel
	 * @param noeud
	 */
	public CdeSupprime (PointDeLivraison pdl, Plan plan, Tournee t, EnsembleDeTournees edt, DemandeDeLivraison dedel, Noeud noeud) {
		this.pdl = pdl;
		this.p = plan;
		this.tournee = t;
		this.edt = edt;
		this.duree = pdl.getDuree();
		this.dedel = dedel;
		this.noeud = noeud;
	}

	public void doCde() throws Exception {
		position = tournee.getIndexOfPointDeLivraison(pdl);
		tournee.setEstSelectionne(false);
		edt.supprimerPointDeLivraison(pdl, p, tournee);
	}

	public void undoCde() throws Exception {
		if (tournee.getListePointTrajet().size() == 0) {
			pdl = edt.ajouterPointDeLivraisonNouvelleTournee(noeud, duree, p, dedel.getEntrepot(),tournee);
		}
		else {
			pdl = edt.ajouterPointDeLivraison(noeud, duree, p, tournee);
			int positionActuelle = tournee.getIndexOfPointDeLivraison(pdl);
			if (position != -1) {
				while (positionActuelle != position) {
					edt.avancerPointDeLivraison(pdl);
					positionActuelle = tournee.getIndexOfPointDeLivraison(pdl);

				}
				position = positionActuelle;
			}
		}
	}

}
