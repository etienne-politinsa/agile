package controleur;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Noeud;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Tournee;

/**
 * @author Etienne DELAHAYE & Loïc CASTELLON
 *
 */
public class CdeDeplace implements Commande {

	private Tournee newTournee;
	private Tournee oldTournee;
	private PointDeLivraison pdl;
	private int oldPosition = -1;
	private int newPosition = -1;
	private int duree;
	private EnsembleDeTournees edt;
	private Plan p;
	private Noeud noeud;

	/**
	 * Commande de déplacement d'un point de livraison
	 * @param newTournee
	 * @param oldTournee
	 * @param edt
	 * @param p
	 * @param noeud
	 * @param dedel
	 * @param d
	 */
	public CdeDeplace (Tournee newTournee, Tournee oldTournee, EnsembleDeTournees edt, Plan p, Noeud noeud, DemandeDeLivraison dedel, int d) {
		this.newTournee = newTournee;
		this.oldTournee = oldTournee;
		this.edt = edt;
		this.p = p;
		this.noeud = noeud;
		duree = d;
	}

	@Override
	public void doCde() throws Exception {

		pdl = edt.ajouterPointDeLivraison(noeud, duree, p, newTournee);
		int positionActuelle = newTournee.getIndexOfPointDeLivraison(pdl);
		if (newPosition != -1) {
			while (positionActuelle != newPosition) {
				edt.avancerPointDeLivraison(pdl);
				positionActuelle = newTournee.getIndexOfPointDeLivraison(pdl);

			}
			newPosition = positionActuelle;
		}

		//suppression
		oldPosition = oldTournee.getIndexOfPointDeLivraison(pdl);
		edt.supprimerPointDeLivraison(pdl, p, oldTournee);


	}

	@Override
	public void undoCde() throws Exception {
		//suppresion
		newPosition = newTournee.getIndexOfPointDeLivraison(pdl);
		edt.supprimerPointDeLivraison(pdl, p, newTournee);

		//ajout
		pdl = edt.ajouterPointDeLivraison(noeud, duree, p, oldTournee);
		int positionActuelle = oldTournee.getIndexOfPointDeLivraison(pdl);
		if (oldPosition != -1) {
			while (positionActuelle != oldPosition) {
				edt.avancerPointDeLivraison(pdl);
				positionActuelle = oldTournee.getIndexOfPointDeLivraison(pdl);

			}
			oldPosition = positionActuelle;
		}
	}
}
