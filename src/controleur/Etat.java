package controleur;


import modele.DemandeDeLivraison;

import modele.EnsembleDeTournees;

import vue.Fenetre;


/**
 * @author Etienne DELAHAYE
 *
 */
public interface Etat {

	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Charger un Plan"
	 * @param fenetre la fenetre sur laquelle charger le plan
	 * @param c le controleur contenant l'état
	 */
	public void chargerPlan(Fenetre fenetre, Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Charger une Demande de Livraison"
	 * @param c le controleur contenant l'état
	 */
	public void chargerLivraisons(Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Renseigner le nombre de livreurs"
	 * @param d la demande de livraison à modifier
	 * @param fenetre la fenetre sur  laquelle l'utilisateur interagit
	 * @param c le controleur contenant l'état
	 */
	public void renseignerNbLivreurs(DemandeDeLivraison d, Fenetre fenetre, Controleur c);


	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Calculer les tournées"
	 * @param edt l'EnsembleDeTournees qui va calculer les tournées 
	 * @param c le controleur contenant l'état
	 */
	public void calculerTournees(EnsembleDeTournees edt, Controleur c, Fenetre f);

	/**
	 * Méthode appelée par le controleur en cas de clic sur le bouton "Interrompre le calcul"
	 * * @param c le controleur contenant l'état
	 */
	public void stopCalcul(Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique gauche
	 * @param longi longitude du plan correspondant au point du clic
	 * @param lat latitude du plan correspondant au point du clic
	 * @param c le controleur contenant l'état
	 */
	public void clicGauche(double longi, double lat, Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique droit
	 * @param longi longitude du plan correspondant au point du clic
	 * @param lat latitude du plan correspondant au point du clic
	 * @param c le controleur contenant l'état
	 */
	public void clicDroit(double longi, double lat, Controleur c);

	/**
	 * Méthode appelée par le controleur après un Ctrl+clique gauche
	 * @param longi longitude du plan correspondant au point du clic
	 * @param lat latitude du plan correspondant au point du clic
	 * @param c le controleur contenant l'état
	 */
	public void controleClicGauche(double longi, double lat, Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Ajouter une livraison"
	 * @param c le controleur contenant l'état
	 */
	public void ajouter(Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Supprimer une livraison"
	 * @param c le controleur contenant l'état
	 */
	public void supprimer(Controleur c);

	/**
	 * Méthode appelée par le controleur après un clique sur le bouton "Déplacer une livraison"
	 * @param c le controleur contenant l'état
	 */
	public void deplacer(Controleur c);


	/**
	 * Méthode appelée par le controleur en cas de clic sur la Liste sur "Tout voir"
	 * @param afficheEntrepot
	 * @param afficheLivraison
	 * @param afficheTournee
	 * @param isAll
	 * @param c le controleur contenant l'état
	 */
	public void controleAffichageEnsembleVueTextuelle(boolean afficheEntrepot, boolean afficheLivraison, boolean afficheTournee, boolean isAll, Controleur c);


	/**
	 * @param index
	 * @param type
	 * @param c le controleur contenant l'état
	 */
	public void selectionEntiteParticuliere(int index, String type, Controleur c);

	/**
	 * Méthode appelée par le controleur après un appui sur flèche haut
	 * @param c le controleur contenant l'état
	 */
	public void flecheHaut(Controleur c);


	/**
	 * Méthode appelée par le controleur après un appui sur flèche bas
	 * @param c le controleur contenant l'état
	 */
	public void flecheBas(Controleur c);

	/**
	 * Méthode appelée par le controleur après un appui sur Entrée
	 * @param c le controleur contenant l'état
	 */
	public void finModif(Controleur c);

	/**
	 * Methode appelée par la fenetre quand l'utilisateur fait un Ctrl+Z
	 * @param listeDeCdes 
	 */
	public void undo(ListeDeCdes listeDeCdes);

	/**
	 * Methode appelee par fenetre quand l'utilisateur fait un Ctrl+Y
	 * @param listeDeCdes la liste de commande 
	 */
	public void redo(ListeDeCdes listeDeCdes);

}