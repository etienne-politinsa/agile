package controleur;

/**
 * Classe qui implémente EtatDefaut et qui ne surcharge aucune méthode.
 * Elle permet d'instancier un Etat qui ne fait rien par défaut.
 * 
 * @author Etienne DELAHAYE
 *
 */
public class EtatInit extends EtatDefaut {

}
