package controleur;

import java.util.LinkedList;

/**
 * Très largement copié de placo 
 * @author C.Solnon
 *
 */
public class ListeDeCdes {
	private LinkedList<Commande> liste;
	private int indiceCrt;

	public ListeDeCdes(){
		indiceCrt = -1;
		liste = new LinkedList<Commande>();
	}

	/**
	 * Ajout de la commande c a la liste this
	 * @param c
	 */
	public void ajoute(Commande c) throws Exception {
		int i = indiceCrt+1;
		while(i < liste.size()){
			liste.remove(i);
		}
		indiceCrt++;
		liste.add(indiceCrt, c);
		c.doCde();
	}

	/**
	 * Annule temporairement la dernière commande ajoutée (cette commande pourra être remise dans la liste avec redo)
	 */
	public void undo() throws Exception {
		if (indiceCrt >= 0){
			Commande cde = liste.get(indiceCrt);
			indiceCrt--;
			cde.undoCde();
		}
	}

	/**
	 * Supprime definitivement la dernière commande ajoutée (cette commande ne pourra pas être remise dans la liste avec redo)
	 */
	public void annule() throws Exception {
		if (indiceCrt >= 0){
			liste.remove(indiceCrt);
			indiceCrt--;
		}
	}

	/**
	 * Remet dans la liste la dernière commande annulée avec undo
	 */
	public void redo() throws Exception {
		if (indiceCrt < liste.size()-1){
			indiceCrt++;
			Commande cde = liste.get(indiceCrt);
			cde.doCde();
		}
	}

	/**
	 * Supprime definitivement toutes les commandes de liste
	 */
	public void reset(){
		indiceCrt = -1;
		liste.clear();  
	}
}
