package controleur;

import vue.Fenetre;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatCalculEnCours extends EtatDefaut {

	@Override
	public void stopCalcul(Controleur c) {
		Fenetre fenetre = c.getFenetre();
		c.getEnsembleDeTournees().stopCalcul(true);

		if(c.getEnsembleDeTournees().getListeTournee().size() > 1) {
			fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
		}
		else { 
			fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, false);
		}
		
		c.setEtatCourant(c.etatStable);
		fenetre.ActiverBouton(Fenetre.CHARGER_PLAN, true);
		fenetre.ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
		fenetre.ActiverBouton(Fenetre.CALCULER_TOURNEES, false);
		fenetre.ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
		fenetre.ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
		fenetre.ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
		fenetre.ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
		fenetre.ActiverBouton(Fenetre.INTERROMPRE_LIVRAISON, false);
	}
}