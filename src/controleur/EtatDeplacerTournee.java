package controleur;

import modele.PointDeLivraison;
import modele.Tournee;
import vue.Fenetre;

/**
 * @author Etienne DELAHAYE
 *
 */
public class EtatDeplacerTournee extends EtatDefaut {

	@Override
	public void clicGauche(double longi, double lat, Controleur c) {
		PointDeLivraison pdl = c.etatDeplacerPdl.getPdl();
		Tournee oldTournee = c.getEnsembleDeTournees().getTourneeParPointDeLivraison(pdl);
		Tournee newTournee = c.getEnsembleDeTournees().getTourneeLaPlusProche(longi, lat);
		int duree = pdl.getDuree();

		try {
			c.getListeDeCdes().ajoute(new CdeDeplace(newTournee, oldTournee, c.getEnsembleDeTournees(), c.getPlan(), pdl, c.getDemandeDeLivraison(), duree));
			c.setEtatCourant(c.etatStable);
			c.getFenetre().afficheMessage("Choisissez une action...");
		} catch (Exception e) {
			c.setEtatCourant(c.etatStable);
			c.getFenetre().afficheMessage("Problème lors du déplacement.");
			try {
				c.getListeDeCdes().annule();
			} catch (Exception ex) {
			}
		} finally {
			c.getFenetre().afficheMessage("Choisissez une action.");
			c.getFenetre().ActiverBouton(Fenetre.AJOUTER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.SUPPRIMER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.RENSEIGNER_NB_LIVREURS, true);
			c.getFenetre().ActiverBouton(Fenetre.CHARGER_PLAN, true);
			c.getFenetre().ActiverBouton(Fenetre.CHARGER_DEMANDE_DE_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.DEPLACER_LIVRAISON, true);
			c.getFenetre().ActiverBouton(Fenetre.CALCULER_TOURNEES, true);
			c.getFenetre().setDesequilibre(c.getEnsembleDeTournees().isDesequilibre());
		}
	}
}
