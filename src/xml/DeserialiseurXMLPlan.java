package xml;

import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import modele.Noeud;
import modele.Plan;
import modele.Troncon;

/**
 * 
 * @author Arthur BELLEMIN et Loïc CASTELLON, architecture initiale inspirée de l'application PlaCo
 * 
 *
 */
public class DeserialiseurXMLPlan extends DeserialiseurXML {
	/**
	 * hash map de id - noeud
	 */
	private static HashMap<Long,Noeud> collecNoeud = new HashMap<Long,Noeud>();
	
	/**
	 * liste de tronçons
	 */
	private static ArrayList<Troncon> collecTroncon = new ArrayList<Troncon>();
	
	/**
	 * latitude minimale
	 */
	private static double minLat;
	
	/**
	 * longitude minimale
	 */
	private static double minLong;
	
	/**
	 * latitude maximale
	 */
	private static double maxLat;
	
	/**
	 * longitude maximale
	 */
	private static double maxLong;
	
	/**
	 *  Permet de construire un plan
	 * @param plan
	 * @throws ExceptionXML
	 */
	public static void chargerPlan(Plan plan) throws ExceptionXML {
		File xml = ouvrirFichier();
		try {
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
	        Document document = docBuilder.parse(xml);
	        Element racine = document.getDocumentElement();	        
	        if (racine.getNodeName().equals("reseau")) {
	        	initMinMax();
	            construireAPartirDeDOMXML(racine);
	            plan.setCollectionNoeuds(collecNoeud);
	            plan.setCollectionTroncons(collecTroncon);
	            plan.setLatmax(maxLat);
	            plan.setLatmin(minLat);
	            plan.setLongmax(maxLong);
	            plan.setLongmin(minLong);
	         }
	         else
	         	throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement du plan");
		}
 	}
	
	/**
	 * Permet de construire un plan avec un nom de fichier défini
	 *  Méthode uniquement utilisée pour de tests
	 * @param plan
	 * @param filename
	 * @throws ExceptionXML
	 */
	public static void chargerPlan(Plan plan, String filename) throws ExceptionXML {
		File xml = new File(filename);
		try {			
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
	        Document document = docBuilder.parse(xml);
	        Element racine = document.getDocumentElement();
	        if (racine.getNodeName().equals("reseau")) {
	        	initMinMax();
	            construireAPartirDeDOMXML(racine);
	            plan.setCollectionNoeuds(collecNoeud);
	            plan.setCollectionTroncons(collecTroncon);
	            plan.setLatmax(maxLat);
	            plan.setLatmin(minLat);
	            plan.setLongmax(maxLong);
	            plan.setLongmin(minLong);
	         }
	         else
	         	throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement du plan");
		}
 	}
	
	/**
	 * Permet de construire un plan
	 * @param noeudDOMRacine
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	private static void construireAPartirDeDOMXML(Element noeudDOMRacine) throws Exception, NumberFormatException {
		
	   	NodeList listeNoeud = noeudDOMRacine.getElementsByTagName("noeud");
	   	for (int i = 0; i < listeNoeud.getLength(); i++) {
	   		Element el = (Element) (listeNoeud.item(i));
	   		Noeud noeudTempo = creerNoeud(el);
	   		collecNoeud.put(noeudTempo.getId(), noeudTempo);
	   	}
	   	
	   	NodeList listeNoeudTroncon = noeudDOMRacine.getElementsByTagName("troncon");
	   	for (int i = 0; i < listeNoeudTroncon.getLength(); i++) {
	   		Element el = (Element) (listeNoeudTroncon.item(i));
	   		Troncon tronconTempo = creerTroncon(el);
	   		collecTroncon.add(tronconTempo);
	   	}
	}
	
	/**
	 * Permet de construire un noeud
	 * @param elt
	 * @return
	 * @throws Exception
	 */
	private static Noeud creerNoeud(Element elt) throws Exception {
		Noeud noeudTempo = null;
		
		double longitude = Double.parseDouble(elt.getAttribute("longitude"));
		if(longitude<=0) 
			throw new Exception("Longitude négative");
		
   		double latitude = Double.parseDouble(elt.getAttribute("latitude"));
   		if(latitude<=0) 
   			throw new Exception("Latitude négative");
   		
   		long id = Long.parseLong(elt.getAttribute("id"));
   		if(id==0) 
   			throw new Exception("Id incorrect");
   		
   		noeudTempo = new Noeud(id,latitude,longitude,null);
   		
   		if(latitude > maxLat)
   			maxLat = latitude;   		
   		if(latitude < minLat)
   			minLat = latitude;
   		if(longitude > maxLong)
   			maxLong = longitude;   		
   		if(longitude < minLong)   			
   			minLong = longitude;
   		
   		return noeudTempo;
	}
	
	/**
	 * Permet de construire un tronçon
	 * @param elt
	 * @return
	 * @throws Exception
	 */
	private static Troncon creerTroncon(Element elt) throws Exception {
		Troncon tronconTempo = null;
		
		double longueur = Double.parseDouble(elt.getAttribute("longueur"));
		if (longueur<0)
			throw new Exception("Longueur n�gative");
   		String nomRue = elt.getAttribute("nomRue");
   		long idNoeudOrigine = Long.parseLong(elt.getAttribute("origine"));
   		if(idNoeudOrigine==0) 
   			throw new Exception("idNoeudOrigine incorrect");
   		
   		long idNoeudDestination = Long.parseLong(elt.getAttribute("destination"));
   		if(idNoeudDestination==0) 
   			throw new Exception("idNoeudDestination incorrect");
   		

   		Noeud NoeudOrigine = getNoeudDansCollectionByID(idNoeudOrigine);
   		Noeud NoeudDestination = getNoeudDansCollectionByID(idNoeudDestination);
   		tronconTempo = new Troncon(longueur,nomRue,NoeudOrigine,NoeudDestination);
   		NoeudOrigine.addTronconPartant(tronconTempo);
   		return tronconTempo;
	}
	
	/**
	 * 
	 * @param myId l'identifiant d'un noeud
	 * @return le noeud correspondant à cet identifiant
	 */
	private static Noeud getNoeudDansCollectionByID(Long myId) {
		Noeud noeudResultat = collecNoeud.get(myId);
		return noeudResultat;
	}
	
	/**
	 * Initialise les paramètres maxLong, minLong, maxLat et minLat
	 */
	private static void initMinMax() {
		maxLong = 0.0;
		minLong = 9999999.0;
		maxLat = 0.0;
		minLat = 99999999.0;
	}
}
