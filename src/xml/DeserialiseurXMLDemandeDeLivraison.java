package xml;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import modele.DemandeDeLivraison;
import modele.Entrepot;
import modele.Noeud;
import modele.Plan;
import modele.PointDeLivraison;
/**
 * 
 * @author Arthur BELLEMIN et Loïc CASTELLON, architecture initiale inspirée de l'application PlaCo
 *
 */
public class DeserialiseurXMLDemandeDeLivraison extends DeserialiseurXML {
	/**
	 * liste des points de livraison
	 */
	private static ArrayList<PointDeLivraison> collecPointDeLivraison;

	/**
	 * entrepot
	 */
	private static Entrepot entrepot = null;

	/**
	 * format de date
	 */
	private static SimpleDateFormat monFormat = new SimpleDateFormat("hh:mm:ss");

	/**
	 * Permet de construire une demande de livraison
	 * @param dedel demande de Livraison
	 * @param plan plan
	 * @throws Exception
	 */
	public static void chargerDemandeDeLivraison(DemandeDeLivraison dedel, Plan plan) throws Exception {
		collecPointDeLivraison = new ArrayList<PointDeLivraison>();
		try {
			File xml = ouvrirFichier();
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
			Document document = docBuilder.parse(xml);
			Element racine = document.getDocumentElement();
			if (racine.getNodeName().equals("demandeDeLivraisons")) {
				construireAPartirDeDOMXML(racine, plan);		
				dedel.setEntrepot(entrepot);
				dedel.setPointsDeLivraison(collecPointDeLivraison);
			}
			else
				throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement de la demande de livraison");
		}
	}
	
	/**
	 * Permet de construire une demande de livraison avec un nom de fichier défini
	 * Méthode uniquement utilisée pour de tests
	 * @param dedel
	 * @param plan
	 * @param filename
	 * @throws Exception
	 */
	public static void chargerDemandeDeLivraison(DemandeDeLivraison dedel, Plan plan, String filename) throws Exception {
		collecPointDeLivraison = new ArrayList<PointDeLivraison>();
		try {
			File xml = new File(filename);
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
			Document document = docBuilder.parse(xml);
			Element racine = document.getDocumentElement();
			if (racine.getNodeName().equals("demandeDeLivraisons")) {
				construireAPartirDeDOMXML(racine, plan);		
				dedel.setEntrepot(entrepot);
				dedel.setPointsDeLivraison(collecPointDeLivraison);
			}
			else
				throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement de la demande de livraison");
		}
	}
	
	/**
	 * Permet de construire une demande de livraison
	 * @param noeudDOMRacine
	 * @param plan
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	private static void construireAPartirDeDOMXML(Element noeudDOMRacine, Plan plan) throws Exception, NumberFormatException{

		NodeList listeNoeud = noeudDOMRacine.getElementsByTagName("entrepot");
		for (int i = 0; i < listeNoeud.getLength(); i++) {
			Element el = (Element) (listeNoeud.item(i));
			Entrepot entrepotTempo = creerEntrepot(el, plan);
			entrepot = entrepotTempo;
		}

		NodeList listeNoeudTroncon = noeudDOMRacine.getElementsByTagName("livraison");
		for (int i = 0; i < listeNoeudTroncon.getLength(); i++) {
			Element el = (Element) (listeNoeudTroncon.item(i));
			PointDeLivraison pointDeLivraisonTempo = creerPointDeLivraison(el, plan);
			collecPointDeLivraison.add(pointDeLivraisonTempo);
		}
	}
	
	/**
	 * Permet de construire un entrepot
	 * @param elt
	 * @param plan
	 * @return
	 * @throws Exception
	 */
	private static Entrepot creerEntrepot(Element elt, Plan plan) throws Exception {
		Entrepot EntrepotTempo = null;
		String heureDepart = elt.getAttribute("heureDepart");
		Date date = monFormat.parse(heureDepart);
		long IdNoeud = Long.parseLong(elt.getAttribute("adresse"));
		Noeud noeud = plan.rechercheNoeudParId(IdNoeud);
		EntrepotTempo = new Entrepot(noeud,date);

		return EntrepotTempo;

	}
	
	/**
	 * Permet de construire un point de livraison
	 * @param elt
	 * @param plan
	 * @return
	 * @throws Exception
	 */
	private static PointDeLivraison creerPointDeLivraison(Element elt, Plan plan) throws Exception {
		PointDeLivraison pointDeLivraisonTempo = null;
		long IdNoeud = Long.parseLong(elt.getAttribute("adresse"));
		int duree = Integer.parseInt(elt.getAttribute("duree"));
		Noeud noeud = plan.rechercheNoeudParId(IdNoeud);
		pointDeLivraisonTempo = new PointDeLivraison(noeud,duree);
		return pointDeLivraisonTempo;
	}
}
