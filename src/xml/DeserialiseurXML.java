package xml;
import java.io.File;

/**
 * Classe abstraite pour les désérialiseurs XML
 * @author architecture de l'application PlaCo
 *
 */
public abstract class DeserialiseurXML {
	
	protected static File ouvrirFichier() throws ExceptionXML {
		File xml = OuvreurDeFichierXML.getInstance().ouvre(true);
		return xml;
		
	}
	
}
