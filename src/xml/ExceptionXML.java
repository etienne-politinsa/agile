package xml;

/**
 * Classe permettant de gérer des exceptions 
 * @author architecture de l'application PlaCo
 *
 */
@SuppressWarnings("serial")
public class ExceptionXML extends Exception {

	public ExceptionXML(String message) {
		super(message);
	}
}
