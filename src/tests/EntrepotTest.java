package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.Test;

import modele.Entrepot;
import modele.Noeud;
import modele.Troncon;

class EntrepotTest {

	@Test
	void test() {
		Entrepot entrepot1 = new Entrepot(new Long(12), 4592.5, 34.7, new ArrayList<Troncon>(), new Date());
		assertNotNull(entrepot1.getTronconsPartants());
		assertNotNull(entrepot1.getHeureDepart());
		assertEquals(entrepot1.getLatitude(), 4592.5);
		assertEquals(entrepot1.getLongitude(), 34.7);

		Noeud noeud = new Noeud(new Long(15),348.48,0.48,null);
		Entrepot entrepot2 = new Entrepot(noeud, new Date());
		assertNotNull(entrepot2.getTronconsPartants());
		assertNotNull(entrepot2.getHeureDepart());
		assertEquals(entrepot2.getLatitude(), 348.48);
		assertEquals(entrepot2.getLongitude(), 0.48);
		
		entrepot2.setEstSelectionne(false);
		assertFalse(entrepot2.isEstSelectionne());
		
		assertNotNull(entrepot1.toString());
		assertNotNull(entrepot2.affiche());
	}

}
