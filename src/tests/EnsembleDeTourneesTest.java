package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.*;
import xml.DeserialiseurXMLDemandeDeLivraison;
import xml.DeserialiseurXMLPlan;
import xml.ExceptionXML;


class EnsembleDeTourneesTest {
	
	private final String CHEMIN_PLAN_1 = AdresseFichiersTest.CHEMIN + "testEnsembleDeTournees-plan.xml";
	private final String CHEMIN_DEMANDE_DE_LIVRAISON_1 = AdresseFichiersTest.CHEMIN + "testEnsembleDeTournees-dl.xml";

	@Test
	@SuppressWarnings("static-access")
	void test1() {
		Plan plan = new Plan(10,10);
		try {
			new DeserialiseurXMLPlan().chargerPlan(plan,CHEMIN_PLAN_1);
		} catch (ExceptionXML e) {
			e.printStackTrace();
			fail("Erreur lors du chargement du plan.");
		}
		DemandeDeLivraison demandeDeLivraison = new DemandeDeLivraison();
		demandeDeLivraison.setNombreLivreurs(2);
		try {
			new DeserialiseurXMLDemandeDeLivraison().chargerDemandeDeLivraison(demandeDeLivraison, plan, CHEMIN_DEMANDE_DE_LIVRAISON_1);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors du chargement de la demande de livraison.");
		}
		
		EnsembleDeTournees edt = new EnsembleDeTournees(plan, demandeDeLivraison);
		assertNotNull(edt);
		assertNotNull(edt.getListeTournee());
		edt.setListeTournee(null);
		assertNull(edt.getListeTournee());
		
		edt.calculer();
		edt.miseAJourMeilleureSolution();
		edt.stopCalcul(true);
		
		assertNotNull(edt.affiche());
		assertNotNull(edt.toString());
		
		assertEquals(2, edt.getListeTournee().size());
		
		assertEquals(1, edt.getIndex(edt.getListeTournee().get(1)));
		assertEquals(-1, edt.getIndex(new Tournee(0, 30, null)));
		
		edt.setEstSelectionne(0, true);
		assertTrue(edt.getListeTournee().get(0).isEstSelectionne());
		
		assertFalse(edt.isDesequilibre());
		edt.getListeTournee().get(0).setListePointTrajet(new ArrayList<Pair<PointDeLivraison, Trajet>>());
		assertTrue(edt.isDesequilibre());
		edt.setListeTournee(new ArrayList<Tournee>());
		assertFalse(edt.isDesequilibre());
	}
}