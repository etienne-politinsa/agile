package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Troncon;

class NoeudTest {

	@Test
	void constructeurs() {
		Noeud noeud1 = new Noeud(new Long(12),12.0,14.2,null);
		assertNotNull(noeud1.getTronconsPartants());
		
		Noeud noeud2 = new Noeud(new Long(16),12.67,674.2,new ArrayList<Troncon>());
		assertNotNull(noeud2);
	}

	@Test
	void settersGettersToString() {
		Noeud noeud = new Noeud();

		noeud.setId(new Long(29));
		assertEquals(noeud.getId(),new Long(29));
		
		noeud.setLatitude(45.4673);
		assertEquals(noeud.getLatitude(),45.4673);
		
		noeud.setLongitude(4.39);
		assertEquals(noeud.getLongitude(),4.39);
		
		noeud.setTronconsPartants(null);
		assertNull(noeud.getTronconsPartants());
		
		assertNotNull(noeud.toString());
	}

	@Test
	void methodes() {
		Noeud noeud = new Noeud();
		noeud.setLatitude(4);
		noeud.setLongitude(3);
		double resultat = noeud.calculDistanceEuclidienne(0, 0);
		assertEquals(resultat, 5);
		
		noeud.addTronconPartant(new Troncon(resultat, null, noeud, noeud));
		assertEquals(noeud.getTronconsPartants().size(), 1);
	}
}
