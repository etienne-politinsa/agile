package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Troncon;

class TronconTest {

	@Test
	void constructeurSettersGettersToString() {
		Troncon troncon1 = new Troncon(14.3, "Nom du tronçon", new Noeud(), new Noeud());
		assertNotNull(troncon1.getNoeudDestination());
		assertNotNull(troncon1.getNoeudOrigine());
		assertEquals(troncon1.getLongueur(), 14.3);
		assertEquals(troncon1.getNom(), "Nom du tronçon");
		
		Troncon troncon2 = new Troncon(0, null, new Noeud(), new Noeud());
		troncon2.setLongueur(13589.487);
		troncon2.setNom("name");
		troncon2.setNoeudOrigine(null);
		troncon2.setNoeudDestination(null);
		assertNull(troncon2.getNoeudDestination());
		assertNull(troncon2.getNoeudOrigine());
		assertEquals(troncon2.getLongueur(),13589.487);
		assertEquals(troncon2.getNom(), "name");
		
		assertNotNull(troncon1.toString());
		assertNotNull(troncon1.affiche());
	}

	@Test
	void methodes() {
		Noeud noeudOrigine = new Noeud(new Long(368), -50.55, 178.45, null);
		Noeud noeudDestination = new Noeud(new Long(368), +50.55, 178.45, null);
		Troncon troncon = new Troncon(0, "un nom", noeudOrigine, noeudDestination);
		double resultat = troncon.calculDistanceEuclidienne(0, 0);
		assertEquals(resultat, 178.45);
	}
}