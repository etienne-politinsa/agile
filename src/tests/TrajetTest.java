package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Trajet;
import modele.Troncon;

class TrajetTest {

	@Test
	void test() {
		ArrayList<Troncon> parcoursTroncons = new ArrayList<Troncon>();
		Noeud noeudDepart = new Noeud(new Long(1), 100, 100, null);
		Noeud noeudIntermediaire = new Noeud(new Long(2), 100, 300, null);
		Noeud noeudArrivee = new Noeud(new Long(3), 200, 300, null);
		Troncon troncon1 = new Troncon(200, "troncon 1", noeudDepart, noeudIntermediaire);
		Troncon troncon2 = new Troncon(100, "troncon 2", noeudIntermediaire, noeudArrivee);
		parcoursTroncons.add(troncon1);
		parcoursTroncons.add(troncon2);
		
		Trajet trajet = new Trajet(parcoursTroncons);
		assertNotNull(trajet.getNoeudArrivee());
		assertNotNull(trajet.getNoeudDepart());
		assertNotNull(trajet.getParcoursTroncons());
		assertEquals(300, trajet.getLongueur());
		assertEquals(new Long(72000), trajet.getTempsParcours());
		
		assertNotNull(trajet.toString());
		assertNotNull(trajet.affiche());
		
		assertEquals(200, trajet.calculDistanceEuclidienne(0, 100));
	}
}