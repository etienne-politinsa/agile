package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.Test;

import modele.DemandeDeLivraison;
import modele.Pair;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Tournee;
import modele.Trajet;
import xml.DeserialiseurXMLDemandeDeLivraison;
import xml.DeserialiseurXMLPlan;
import xml.ExceptionXML;

class TourneeTest {
	
	private final String CHEMIN_PLAN = AdresseFichiersTest.CHEMIN + "testTournee-plan.xml";
	private final String CHEMIN_DEMANDE_DE_LIVRAISON = AdresseFichiersTest.CHEMIN + "testTournee-dl.xml";

	@Test
	@SuppressWarnings("static-access")
	void constructeurSetterGetterToString() {
		Plan plan = new Plan(10,10);
		try {
			new DeserialiseurXMLPlan().chargerPlan(plan,CHEMIN_PLAN);
		} catch (ExceptionXML e) {
			System.err.println(e.getMessage());
			fail("Erreur lors du chargement du plan.");
		}
		DemandeDeLivraison demandeDeLivraison = new DemandeDeLivraison();
		try {
			new DeserialiseurXMLDemandeDeLivraison().chargerDemandeDeLivraison(demandeDeLivraison, plan, CHEMIN_DEMANDE_DE_LIVRAISON);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			fail("Erreur lors du chargement de la demande de livraison.");
		}
		
		Tournee tournee1 = new Tournee (12.3, 2, new ArrayList<Pair<PointDeLivraison,Trajet>>());
		assertNotNull(tournee1.getListePointTrajet());
		assertEquals(tournee1.getLivreur(), 2);
		assertEquals(tournee1.getLongueur(), 12.3);
		
		Tournee tournee2 = new Tournee(0, demandeDeLivraison.getPointsDeLivraison(), plan, demandeDeLivraison.getEntrepot());
		assertNotNull(tournee2.toString());
		assertNotNull(tournee2.affiche(demandeDeLivraison));
		assertEquals(1, tournee2.getIndexOfPointDeLivraison(demandeDeLivraison.getPointsDeLivraison().get(1)));
		assertEquals(-1, tournee2.getIndexOfPointDeLivraison(new PointDeLivraison(new Long(10), 0, 0, null, 0)));
		
		Tournee tournee3 = new Tournee(0, new ArrayList<PointDeLivraison>(), plan, demandeDeLivraison.getEntrepot());
		tournee3.setListePointTrajet(null);
		assertNull(tournee3.getListePointTrajet());
		tournee3.setLongueur(45.9);
		assertEquals(45.9, tournee3.getLongueur());
		tournee3.setLivreur(567);
		assertEquals(567, tournee3.getLivreur());
		tournee3.setEstSelectionne(true);
		assertTrue(tournee3.isEstSelectionne());
		assertNotNull(tournee3);
	}
	
	@Test
	@SuppressWarnings({ "static-access", "deprecation" })
	void autres() {
		Plan plan = new Plan(10,10);
		try {
			new DeserialiseurXMLPlan().chargerPlan(plan,CHEMIN_PLAN);
		} catch (ExceptionXML e) {
			e.printStackTrace();
			fail("Erreur lors du chargement du plan.");
		}
		DemandeDeLivraison demandeDeLivraison = new DemandeDeLivraison();
		try {
			new DeserialiseurXMLDemandeDeLivraison().chargerDemandeDeLivraison(demandeDeLivraison, plan, CHEMIN_DEMANDE_DE_LIVRAISON);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors du chargement de la demande de livraison.");
		}
		
		Tournee tournee = new Tournee(0, demandeDeLivraison.getPointsDeLivraison(), plan, demandeDeLivraison.getEntrepot());
		double res = tournee.calculDistanceEuclidienne(1, 1.5);
		assertEquals(0, res);
		
		PointDeLivraison nouveau = null;
		try {
			nouveau = tournee.ajouterPointDeLivraison(plan.rechercheNoeudParId(new Long(7)), 48, null);
			assertNull(nouveau);
			nouveau = tournee.ajouterPointDeLivraison(plan.rechercheNoeudParId(new Long(7)), 48, plan);
			assertEquals(new Date(70,0,1,8,14,0), nouveau.getHeureFin());
			tournee.avancer(nouveau, plan, demandeDeLivraison.getEntrepot());
			assertEquals(new Date(70,0,1,8,13,12), nouveau.getHeureFin());
			tournee.reculer(nouveau, plan, demandeDeLivraison.getEntrepot());
			assertEquals(new Date(70,0,1,8,14,0), nouveau.getHeureFin());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors de ajouterPointDeLivraison ou avancer");
		}
		
		try {
			tournee.avancer(new PointDeLivraison(null, res, res, null, 0), plan, demandeDeLivraison.getEntrepot());
			fail("On ne devrait pas pouvoir avancer un point inconnu.");
		} catch (Exception e) {
		}
		
		try {
			tournee.avancer(demandeDeLivraison.getPointsDeLivraison().get(0), plan, demandeDeLivraison.getEntrepot());
			fail("On ne devrait pas pouvoir avancer le premier point.");
		} catch (Exception e) {
		}
		
		try {
			tournee.avancer(new PointDeLivraison(null, res, res, null, 0), plan, demandeDeLivraison.getEntrepot());
			fail("On ne devrait pas pouvoir reculer un point inconnu.");
		} catch (Exception e) {
		}
		
		try {
			tournee.reculer(nouveau, plan, demandeDeLivraison.getEntrepot());
			fail("On ne devrait pas pouvoir reculer le dernier point.");
		} catch (Exception e) {
		}

		try {
			tournee.avancer(nouveau, plan, demandeDeLivraison.getEntrepot());
		} catch (Exception e) {
			e.printStackTrace();
		}
		tournee.supprimerPointDeLivraison(nouveau, plan);
		assertEquals(new Date(70,0,1,8,12,0), tournee.getListePointTrajet().get(tournee.getListePointTrajet().size()-2).getFirst().getHeureFin());
		
		try {
			tournee.supprimerPointDeLivraison(new PointDeLivraison(null, res, res, null, 0), plan);
			fail("On ne devrait pas pouvoir supprimer un point inconnu.");
		} catch (Exception e) {
		}
	}
}