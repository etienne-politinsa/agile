package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import modele.DemandeDeLivraison;
import modele.Entrepot;
import modele.Graphe;
import modele.Noeud;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Troncon;
import tsp.TSP;
import tsp.TSP1;
import xml.DeserialiseurXMLDemandeDeLivraison;
import xml.DeserialiseurXMLPlan;
import xml.ExceptionXML;

class TSPTest {

	private static Graphe graphe;
	private static final String CHEMIN_PLAN = AdresseFichiersTest.CHEMIN + "moyenPlan.xml";
	private static final String CHEMIN_DEMANDE_DE_LIVRAISON = AdresseFichiersTest.CHEMIN + "dl-moyen-12.xml";

	@Test
	void test(){


		Plan plan = new Plan(50,50);
		try {
			new DeserialiseurXMLPlan().chargerPlan(plan,CHEMIN_PLAN);
		} catch (ExceptionXML e) {
			System.err.println(e.getMessage());
			fail("Erreur lors du chargement du plan.");
		}
		DemandeDeLivraison demandeDeLivraison = new DemandeDeLivraison();
		try {
			new DeserialiseurXMLDemandeDeLivraison().chargerDemandeDeLivraison(demandeDeLivraison, plan, CHEMIN_DEMANDE_DE_LIVRAISON);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			fail("Erreur lors du chargement de la demande de livraison.");
		}


		graphe = new Graphe(plan,demandeDeLivraison);

		TSP tsp = new TSP1();

		tsp.chercheSolution(graphe);
		tsp.getMeilleureSolution(1);
		Integer[] test = tsp.meilleureSolution();
		Integer[] test2 ={0,7,6,3,10,11,1,5,8,9,4,2,12,13}; 
		for(int i = 0;i<test.length;i++) {
			assertEquals(test[i],test2[i],"La meilleure solution doit être retournée");
		}
	}

}
