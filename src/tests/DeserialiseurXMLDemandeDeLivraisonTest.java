package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import modele.DemandeDeLivraison;
import modele.Plan;
import xml.DeserialiseurXMLDemandeDeLivraison;
import xml.DeserialiseurXMLPlan;
import xml.ExceptionXML;

class DeserialiseurXMLDemandeDeLivraisonTest {
	
	public static final String CHEMIN_FICHIER = AdresseFichiersTest.CHEMIN;
	public static final String FILENAME_PLAN = CHEMIN_FICHIER +"testXMLplan.xml";
	public static Plan plan = new Plan(5, 10);
	
	@BeforeAll
	static void chargerPlan() throws ExceptionXML {
		
		DeserialiseurXMLPlan.chargerPlan(plan, FILENAME_PLAN);
	}

	@Test
	void testChargerDemandeDeLivraison() throws Exception {
		String filename;
		
		/*
		 * Fichier JPG (complètement incompatible)
		 */
		filename = CHEMIN_FICHIER+"picture.jpg";
		DemandeDeLivraison dedel = new DemandeDeLivraison();
		
		try {
			DeserialiseurXMLDemandeDeLivraison.chargerDemandeDeLivraison(dedel, plan, filename);
		} catch (ExceptionXML e) {
			if(!e.getMessage().equals("Erreur de chargement de la demande de livraison"))
				fail("Erreur dans le désérialiseur demande de livraison (jpg)");
		}
		
		/*
		 * Fichier XML non conforme
		 */
		filename = CHEMIN_FICHIER+"testXMLdedelNonConforme.xml";
		try {
			DeserialiseurXMLDemandeDeLivraison.chargerDemandeDeLivraison(dedel, plan, filename);
			fail("Erreur dans le désérialiseur demande de livraison (doc non conforme)");
		} catch (ExceptionXML e) {
		}
		
		/*
		 * Fichier vide
		 */
		filename = CHEMIN_FICHIER+"testXMLdedelVide.xml";
		try {
			DeserialiseurXMLDemandeDeLivraison.chargerDemandeDeLivraison(dedel, plan, filename);
			fail("Erreur dans le désérialiseur demande de livraison (doc vide)");
		} catch (ExceptionXML e) {
		}
		
		/*
		 * Fichier compatible: pas d'erreurs + plan cohérent ?
		 */
		filename = CHEMIN_FICHIER+"testXMLdedel.xml";
		try {
			DeserialiseurXMLDemandeDeLivraison.chargerDemandeDeLivraison(dedel, plan, filename);
		} catch (ExceptionXML e) {
			fail("Erreur dans le désérialiseur demande de livraison (doc conforme)");
		}
		assertNotNull(dedel.getEntrepot());
		assertNotNull(dedel.getNombreLivreurs());
		assertNotNull(dedel.getPointsDeLivraison());
		
	}

}
