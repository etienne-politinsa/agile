package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.Test;

import modele.DemandeDeLivraison;
import modele.Noeud;
import modele.PointDeLivraison;
import modele.Troncon;

class PointDeLivraisonTest {

	@Test
	void test() {
		PointDeLivraison pdl1 = new PointDeLivraison(new Long(12), 12.0, 14.2, new ArrayList<Troncon>(), 16);
		assertEquals(pdl1.getDuree(),16);
		
		Noeud noeud = new Noeud(new Long(34928),3488.08,388.48,null);
		PointDeLivraison pdl2 = new	PointDeLivraison(noeud, 0);
		assertEquals(pdl2.getDuree(), 0);
		
		pdl1.setDuree(14);
		assertEquals(pdl1.getDuree(),14);
		
		pdl1.setEstSelectionne(true);
		assertTrue(pdl1.isEstSelectionne());
		
		pdl2.setHeurePassage(new Date(new Long(0)));
		pdl2.setDuree(60);
		assertEquals(pdl2.getHeurePassage(),new Date(new Long(0)));
		assertEquals(pdl2.getHeureFin(), new Date(new Long(60*1000)));
		
		assertNotNull(pdl1.toString());
		pdl1.setHeurePassage(null);
		assertNotNull(pdl1.affiche(new DemandeDeLivraison()));
		pdl1.setHeurePassage(new Date());
		assertNotNull(pdl1.affiche(new DemandeDeLivraison()));
	}

}
