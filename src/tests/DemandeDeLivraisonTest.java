package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.DemandeDeLivraison;
import modele.Entrepot;
import modele.Noeud;
import modele.PointDeLivraison;

class DemandeDeLivraisonTest {

	@Test
	void test() {
		
		//Tests sur constructeur vide
		DemandeDeLivraison dedel = new DemandeDeLivraison();
		assertEquals(dedel.getNombreLivreurs(),1);
		assertNotNull(dedel.getPointsDeLivraison());
		assertNull(dedel.getEntrepot());
		
		//Tests sur constructeur sans nombre de livreurs
		Noeud noeud = new Noeud();
		Entrepot unEntrepot = new Entrepot(noeud, null);
		ArrayList<PointDeLivraison> desPointsDeLivraison = new ArrayList<PointDeLivraison>();
		DemandeDeLivraison dedel2 = new DemandeDeLivraison( unEntrepot, desPointsDeLivraison);
		assertEquals(dedel2.getNombreLivreurs(),1);
		assertNotNull(dedel2.getPointsDeLivraison());
		assertNotNull(dedel2.getEntrepot());
		
		//Tests sur constructeur avec nombre de livreurs
		DemandeDeLivraison dedel3 = new DemandeDeLivraison( unEntrepot, desPointsDeLivraison, 12);
		assertEquals(dedel3.getNombreLivreurs(),12);
		assertNotNull(dedel3.getPointsDeLivraison());
		assertNotNull(dedel3.getEntrepot());
		
		
		
		
	}

}
