package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import modele.Plan;
import xml.DeserialiseurXMLPlan;
import xml.ExceptionXML;



class DeserialiseurXMLPlanTest {
	
	public static final String CHEMIN_FICHIER = AdresseFichiersTest.CHEMIN;

	/**
	 * Test method for {@link xml.DeserialiseurXMLPlan#chargerPlan(modele.Plan)}.
	 */
	@Test
	void testChargerPlan() {
		Plan plan = new Plan(5, 10);
		String filename;

		/*
		 * Fichier JPG (complètement incompatible)
		 */
		filename = CHEMIN_FICHIER+"picture.jpg";
		try {
			DeserialiseurXMLPlan.chargerPlan(plan, filename);
		} catch (ExceptionXML e) {
			if(!e.getMessage().equals("Erreur de chargement du plan"))
				fail("Erreur dans le désérialiseur plan (jpg)");
		}

		/*
		 * Fichier XML non conforme
		 */
		filename = CHEMIN_FICHIER+"testXMLplanNonConforme.xml";
		try {
			DeserialiseurXMLPlan.chargerPlan(plan, filename);
			fail("Erreur dans le désérialiseur plan (doc non conforme)");
		} catch (ExceptionXML e) {
		}

		/*
		 * Fichier compatible vide
		 */
		filename = CHEMIN_FICHIER+"testXMLplanVide.xml";
		try {
			DeserialiseurXMLPlan.chargerPlan(plan, filename);
		} catch (ExceptionXML e) {
			fail("Erreur dans le désérialiseur plan (doc conforme vide)");
		}
		

		/*
		 * Fichier compatible: pas d'erreurs + plan cohérent ?
		 */
		filename = CHEMIN_FICHIER+"testXMLplan.xml";
		try {
			DeserialiseurXMLPlan.chargerPlan(plan, filename);
		} catch (ExceptionXML e) {
			fail("Erreur dans le désérialiseur plan (doc conforme)");
		}
		assertNotNull(plan.getCollectionNoeuds());
		assertNotNull(plan.getCollectionTroncons());
		assertEquals(plan.getCollectionNoeuds().size(), 3);
		assertEquals(plan.getHauteurplan(),10);
		assertEquals(plan.getLatmin(),45.1);
	}
}
