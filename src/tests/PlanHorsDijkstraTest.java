package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Plan;
import modele.Troncon;
import xml.DeserialiseurXMLPlan;
import xml.ExceptionXML;

class PlanHorsDijkstraTest {
	
	private final String CHEMIN_PLAN = AdresseFichiersTest.CHEMIN + "testPlanHorsDijkstra.xml";
	
	@Test
	void constructeurs() {
		assertNotNull(new Plan());
		
		Plan plan = new Plan(798, 2837);
		assertEquals(plan.getLargeurplan(),798);
		assertEquals(plan.getHauteurplan(),2837);
	}

	@Test
	void getterSetterToString() {
		Plan plan = new Plan();
		
		plan.setLargeurplan(4672);
		assertEquals(plan.getLargeurplan(), 4672);
		
		plan.setHauteurplan(711);
		assertEquals(plan.getHauteurplan(), 711);
		
		plan.setLatmax(37.98729);
		assertEquals(plan.getLatmax(), 37.98729);
		
		plan.setLatmin(1045.4);
		assertEquals(plan.getLatmin(), 1045.4);
		
		plan.setLongmax(3874.7);
		assertEquals(plan.getLongmax(), 3874.7);
		
		plan.setLongmin(0);
		assertEquals(plan.getLongmin(), 0);
		
		plan.setCollectionNoeuds(new HashMap<Long,Noeud>());
		assertNotNull(plan.getCollectionNoeuds());
		
		plan.setCollectionTroncons(new ArrayList<Troncon>());
		assertNotNull(plan.getCollectionTroncons());
		
		assertNotNull(plan.toString());
	}
	
	@SuppressWarnings("static-access")
	@Test
	void methodes() {
		Plan plan = new Plan();
		try {
			new DeserialiseurXMLPlan().chargerPlan(plan,CHEMIN_PLAN);
		} catch (ExceptionXML e) {
			e.printStackTrace();
			fail("Erreur lors du chargement du plan.");
		}
		
		try {
			Noeud noeud1 = plan.rechercheNoeudParId(new Long(6));
			assertEquals(noeud1.getId(), new Long(6));
			Noeud noeud2 = plan.rechercheNoeudParId(new Long(0));
			assertNull(noeud2);
			Noeud noeud3 = plan.rechercheNoeudParId(null);
			assertNull(noeud3);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors de rechercheNoeudParId");
		}
		
		try {
			Noeud noeud4 = plan.getNoeudLePlusProche(1,1);
			assertEquals(noeud4.getId(), new Long(8));
			Noeud noeud5 = plan.getNoeudLePlusProche(50,70);
			assertEquals(noeud5.getId(), new Long(4));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors de rechercheNoeudParId");
		}
		
		try {
			Troncon troncon1 = plan.getTronconLePlusProche(1.4, 1.1);
			assertEquals(troncon1.getNoeudOrigine().getId(), new Long(8));
			assertEquals(troncon1.getNoeudDestination().getId(), new Long(1));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Erreur lors de rechercheNoeudParId");
		}
	}
}
