package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

import modele.DemandeDeLivraison;
import modele.Entrepot;
import modele.Graphe;
import modele.Noeud;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Trajet;
import modele.Troncon;

class GrapheTest {
	/*
	 * Test du graphe sur l'exemple suivant :
	 * 
	 *                                         0 Lille L
	 *            E = Entrepot                     ^
	 *            L = Livraison                    |
	 *            					       	     225.3
	 *            					       		   |
	 *                                             v              
	 * 3 Nantes <------------385.9------------> 1 Paris <-490.2-> 2 Strasbourg L
	 *    ^                                        ^                   |
	 *    |                                        |                   |
	 *  346.9                                    491.2               492.8
	 *    |                                        |                   |
	 *    v                                        v                   |
	 * 6 Bordeaux -375.3-> 4 Clermont E -165.2-> 5 Lyon L <------------'
	 *    ^                                        ^
	 *    |                                        |
	 *  245.7                                    314.2
	 *    |                                        |
	 *    v                                        v
	 * 7 Toulouse L <--------403.2-----------> 8 Marseille <-198.8-> 9 Nice L          
	 */
	
	private static Plan plan;
	private static DemandeDeLivraison demandeDeLivraison;
	//private static String[] villes = {"Lille","Paris","Strasbourg","Nantes","Clermont","Lyon","Bordeaux","Toulouse","Marseille","Nice"};
	private static Graphe graphe;

	@SuppressWarnings("deprecation")
	@Test
	void test() {
		//Cr�ation du plan
		Noeud lille = new Noeud(new Long(0), 50.633333, 3.066667, null);
		Noeud paris = new Noeud(new Long(1), 48.866667, 2.333333, null);
		Noeud strasbourg = new Noeud(new Long(2), 48.5734053, 7.7521113, null);
		Noeud nantes = new Noeud(new Long(3), 47.218371, -1.553621, null);
		Noeud clermont = new Noeud(new Long(4), 47.081012, 2.398782, null);
		Noeud lyon = new Noeud(new Long(5), 45.764043, 4.835659, null);
		Noeud bordeaux = new Noeud(new Long(6), 44.837789, -0.57918, null);
		Noeud toulouse = new Noeud(new Long(7), 43.600000, 1.433333, null);
		Noeud marseille = new Noeud(new Long(8), 43.300000, 5.400000, null);
		Noeud nice = new Noeud(new Long(9), 43.700000, 7.250000, null);
		
		Troncon lilleParis = new Troncon(225.3, "Lille -> Paris", lille, paris);
		lille.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(lilleParis)));
		
		Troncon parisLille = new Troncon(225.3, "Paris -> Lille", paris, lille);
		Troncon parisStrasbourg = new Troncon(490.2, "Paris -> Strasbourg", paris, strasbourg);
		Troncon parisLyon= new Troncon(491.2, "Paris -> Lyon", paris, lyon);
		Troncon parisNantes= new Troncon(385.9, "Paris -> Nantes", paris, nantes);
		paris.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(parisLille, parisStrasbourg, parisLyon, parisNantes)));
		
		Troncon strasbourgParis= new Troncon(490.2, "Strasbourg -> Paris", strasbourg, paris);
		Troncon strasbourgLyon= new Troncon(492.8, "Strasbourg -> Lyon", strasbourg, lyon);
		strasbourg.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(strasbourgParis, strasbourgLyon)));
		
		Troncon nantesParis= new Troncon(385.9, "Nantes -> Paris", nantes, paris);
		Troncon nantesBordeaux= new Troncon(346.9, "Nantes -> Bordeaux", nantes, bordeaux);
		nantes.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(nantesParis, nantesBordeaux)));
		
		Troncon clermontLyon= new Troncon(165.2, "Clermont -> Lyon", clermont, lyon);
		clermont.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(clermontLyon)));
		
		Troncon lyonParis= new Troncon(491.2, "Lyon -> Paris", lyon, paris);
		Troncon lyonMarseille= new Troncon(314.2, "Lyon -> Marseille", lyon, marseille);
		lyon.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(lyonParis, lyonMarseille)));
		
		Troncon bordeauxClermont= new Troncon(375.3, "Bordeaux -> Clermont", bordeaux, clermont);
		Troncon bordeauxNantes= new Troncon(346.9, "Bordeaux -> Nantes", bordeaux, nantes);
		Troncon bordeauxToulouse= new Troncon(245.7, "Bordeaux -> Toulouse", bordeaux, toulouse);
		bordeaux.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(bordeauxClermont, bordeauxNantes, bordeauxToulouse)));
		
		Troncon toulouseBordeaux= new Troncon(245.7, "Toulouse -> Bordeaux", toulouse, bordeaux);
		Troncon toulouseMarseille= new Troncon(403.2, "Toulouse -> Marseille", toulouse, marseille);
		toulouse.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(toulouseBordeaux, toulouseMarseille)));
		
		
		Troncon marseilleToulouse= new Troncon(403.2, "Marseille -> Toulouse", marseille, toulouse);
		Troncon marseilleLyon= new Troncon(314.2, "Marseille -> Lyon", marseille, lyon);
		Troncon marseilleNice= new Troncon(198.8, "Marseille -> Nice", marseille, nice);
		marseille.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(marseilleToulouse, marseilleLyon, marseilleNice)));
		
		Troncon niceMarseille= new Troncon(198.8, "Nice -> Marseille", nice, marseille);
		nice.setTronconsPartants(new ArrayList<Troncon>(Arrays.asList(niceMarseille)));
		
		plan = new Plan(0, 0);
		
		HashMap<Long,Noeud> noeudsPlan = new HashMap<Long,Noeud>();
		noeudsPlan.put(new Long(0), lille);
		noeudsPlan.put(new Long(1), paris);
		noeudsPlan.put(new Long(2), strasbourg);
		noeudsPlan.put(new Long(3), nantes);
		noeudsPlan.put(new Long(4), clermont);
		noeudsPlan.put(new Long(5), lyon);
		noeudsPlan.put(new Long(6), bordeaux);
		noeudsPlan.put(new Long(7), toulouse);
		noeudsPlan.put(new Long(8), marseille);
		noeudsPlan.put(new Long(9), nice);
		plan.setCollectionNoeuds(noeudsPlan);
		
		ArrayList<Troncon> tronconsPlan = new ArrayList<Troncon>();
		tronconsPlan.add(lilleParis);
		tronconsPlan.add(parisLille);
		tronconsPlan.add(parisStrasbourg);
		tronconsPlan.add(parisLyon);
		tronconsPlan.add(parisNantes);
		tronconsPlan.add(strasbourgParis);
		tronconsPlan.add(strasbourgLyon);
		tronconsPlan.add(nantesParis);
		tronconsPlan.add(nantesBordeaux);
		tronconsPlan.add(clermontLyon);
		tronconsPlan.add(lyonParis);
		tronconsPlan.add(lyonMarseille);
		tronconsPlan.add(bordeauxClermont);
		tronconsPlan.add(bordeauxNantes);
		tronconsPlan.add(bordeauxToulouse);
		tronconsPlan.add(toulouseBordeaux);
		tronconsPlan.add(toulouseMarseille);
		tronconsPlan.add(marseilleToulouse);
		tronconsPlan.add(marseilleLyon);
		tronconsPlan.add(marseilleNice);
		tronconsPlan.add(niceMarseille);
		plan.setCollectionTroncons(tronconsPlan);
		
		//Cr�ation de la demande de livraison
		demandeDeLivraison = new DemandeDeLivraison();
		Entrepot entrepotClermont = new Entrepot(clermont, new Date(2018, 10, 29, 8, 0, 0));
		ArrayList<PointDeLivraison> pointsDeLivraison = new ArrayList<PointDeLivraison>();
		PointDeLivraison livraisonLille = new PointDeLivraison(lille,0);
		pointsDeLivraison.add(livraisonLille);
		PointDeLivraison livraisonStrasbourg = new PointDeLivraison(strasbourg,0);
		pointsDeLivraison.add(livraisonStrasbourg);
		PointDeLivraison livraisonLyon = new PointDeLivraison(lyon,0);
		pointsDeLivraison.add(livraisonLyon);
		PointDeLivraison livraisonNice = new PointDeLivraison(nice,0);
		pointsDeLivraison.add(livraisonNice);
		PointDeLivraison livraisonToulouse = new PointDeLivraison(toulouse,0);
		pointsDeLivraison.add(livraisonToulouse);
		demandeDeLivraison.setEntrepot(entrepotClermont);
		demandeDeLivraison.setPointsDeLivraison(pointsDeLivraison);


		graphe = new Graphe(plan,demandeDeLivraison);
		
		Trajet t1 = graphe.getTrajet(clermont,clermont);
		assertNull(t1,"Il ne doit pas y avoir de trajet de clermont � clermont");
		
		Trajet t2 = graphe.getTrajet(bordeaux,clermont);
		assertNull(t2,"Il ne doit pas y avoir de trajet au d�part de bordeaux");
		
		Trajet t3 = graphe.getTrajet(livraisonLille,toulouse);
		Trajet t4 = graphe.getTrajet(lille,livraisonToulouse);
		assertEquals(t3,t4,"Il ne doit pas y avoir de diff�rence entre le trajet des livraison et des noeuds");
		
		Trajet t5 = graphe.getTrajet(entrepotClermont,livraisonLyon);
		assertEquals(t5.getLongueur(),165.2,"Le trajet entre Clermont et Lyon a bien la bonne valeur");
	}

}
