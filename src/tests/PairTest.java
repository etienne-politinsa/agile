package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import modele.Pair;

class PairTest {
	
	@Test
	void test() {
		Pair<Integer,String> pair = new Pair<Integer,String>(34,"Jefg");
		assertNotNull(pair);
		
		pair.setFirst(75);
		int first = pair.getFirst();
		assertEquals(first,75);
		
		pair.setSecond("asmp");
		String second = pair.getSecond();
		assertEquals(second,"asmp");
	}

}
