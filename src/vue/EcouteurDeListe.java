package vue;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controleur.Controleur;

/**
 * @author Arthur BELLEMIN
 */
public class EcouteurDeListe implements ListSelectionListener{
	/**
	 * controleur associé à l'écouteur
	 */
	private Controleur controleur;
	/**
	 * liste écoutée par le présent écouteur de liste
	 */
	private ListeElement liste;
	/**
	 * Crée un écouteur de liste
	 * @param liste la liste écoutée par le présent écouteur de liste
	 * @param controleur le controleur associé à l'écouteur
	 */
	public EcouteurDeListe(ListeElement liste, Controleur controleur){
		this.liste = liste;
		this.controleur = controleur;
	}
	/**
	 * override la méthode valueChanged et permet de dispatcher les diverses actions 
	 *  de changement de liste afin de signaler au controleur 
	 *  quel évènement a été déclenché dans la liste 
	 * @param e un évenement de changement de la valeur selectionee dans la liste
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(liste.getSelectedValue() != null) {
			
			String selection = liste.getSelectedValue().toString();
			liste.getVueGraphique().setIsAllSelected(false);
			liste.getVueTextuelle().setAllSelected(false);
			
			switch(selection) {
			case "--Tout voir--":
				liste.getVueGraphique().setIsAllSelected(true);
				liste.getVueTextuelle().setAllSelected(true);
				controleur.controleAffichageEnsembleVueTextuelle(true,true,true,true);
				break;
			case "--Entrepot--":
				controleur.controleAffichageEnsembleVueTextuelle(true,false,false,true);
				break;
			case "--Livraisons--":
				controleur.controleAffichageEnsembleVueTextuelle(false,true,false,true);
				break;
			case "--Tournees--":
				controleur.controleAffichageEnsembleVueTextuelle(false,false,true,true);
				break;	
			default :	
						
				String[] infosSplitees = selection.split(" ");
				
				if(infosSplitees.length >= 1){
					String numero = infosSplitees[1];
					String[] numeroSplite = numero.split("°");
					if(numeroSplite.length >= 1) {
						int index = Integer.parseInt(numeroSplite[1]);
						
						switch(infosSplitees[0]) {
						case "Livraison":
							controleur.controleAffichageEnsembleVueTextuelle(false,true,false,false);
							controleur.selectionEntiteParticuliere(index-1, "livraison"); 							
							break;
						case "Tournee":	
							controleur.controleAffichageEnsembleVueTextuelle(false,false,true,false);
							controleur.selectionEntiteParticuliere(index-1, "tournee");							
							break;
						}
					}				
				}				
			}
		}

	}	
}
