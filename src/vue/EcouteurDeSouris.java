package vue;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;
import controleur.Controleur;


/**
 * @author Arthur BELLEMIN et Thibault REMY, architecture initiale inspirée de l'application PlaCo
 *
 */
public class EcouteurDeSouris extends MouseAdapter {
	/**
	 * controleur associé à l'écouteur
	 */
	private Controleur controleur;
	/**
	 * vue graphique associée à la fenetre écoutée
	 */
	private VueGraphique vueGraphique;
	/**
	 * fenetre écoutée
	 */
	private Fenetre fenetre;
	/**
	 * Crée un écouteur de souris
	 * @param controleur le controleur associé à l'écouteur
	 * @param vueGraphique la vue graphique associée à la fenetre écoutée
	 * @param fenetre la fenetre écoutée
	 */
	public EcouteurDeSouris(Controleur controleur, VueGraphique vueGraphique, Fenetre fenetre){
		this.controleur = controleur;
		this.vueGraphique = vueGraphique;
		this.fenetre = fenetre;
	}
	/**
	 * @param evt l'évènement souris 
	 * Override de mouseClicked, dispatche les évènements souros sur les divers boutons 	 * 
	 * de la fenetre et effectue les appels nécessaires vers le contrôleur
	 */
	@Override
	public void mouseClicked(MouseEvent evt) {		
		switch (evt.getButton()) {
		
		//bouton gauche de la souris
		case MouseEvent.BUTTON1:
			int x = coordonneesX(evt);
			int y = coordonneesY(evt);
			double longi = vueGraphique.convertirXEnLongitude(x);
			double lati = vueGraphique.convertirYEnLatitude(y);
			if(longi!=0.0 && lati!=0.0){			
				if(!evt.isControlDown()) {					
					controleur.clicGauche(longi, lati);
				}else{
					controleur.controleClicGauche(longi, lati);
					vueGraphique.setIsAllSelected(false);
				}					
			}

			break;

		//bouton droit de la souris	
		case MouseEvent.BUTTON3: 
			int x1 = coordonneesX(evt);
			int y1 = coordonneesY(evt);
			double longi1 = vueGraphique.convertirXEnLongitude(x1);
			double lati1 = vueGraphique.convertirYEnLatitude(y1);
			if(longi1 != 0.0 && lati1!=0.0) {
				controleur.clicDroit(longi1, lati1);
				vueGraphique.setIsAllSelected(false);				
			}	
			break;

		default:


		}
	}

	public void mouseMoved(MouseEvent evt) {		
	}
	/**
	 * @param evt l'évènement souris 
	 * @return l'abcisse x du clic souris sur la fenetre
	 * méthode inspirée de PlaCo
	 * permet de renvoyer l'abscisse d'un clic souris dans la fenetre à part de l'évènement appelant
	 */
	private int coordonneesX(MouseEvent evt){
		MouseEvent e = SwingUtilities.convertMouseEvent(fenetre, evt, vueGraphique);
		int x = Math.round((float)e.getX());
		return x;
	}

	/**
	 * @param evt l'évènement souris 
	 * @return l'ordonnée y du clic souris sur la fenetre
	 * méthode inspirée de PlaCo
	 * permet de renvoyer l'ordonnée d'un clic souris dans la fenetre à part de l'évènement appelant
	 */
	private int coordonneesY(MouseEvent evt){
		MouseEvent e = SwingUtilities.convertMouseEvent(fenetre, evt, vueGraphique);
		int y = Math.round((float)e.getY());
		return y;
	}	

}