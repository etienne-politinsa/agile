package vue;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import modele.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import javax.swing.JPanel;


/**
 * @author Arthur BELLEMIN et Thibault REMY architecture initiale inspirée de l'application PlaCo
 */
public class VueGraphique extends JPanel implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * echelle de la vue graphique, coefficient multiplicateur des tailles d'elements
	 */
	private int echelle;
	/**
	 * hauteur de la vue graphique
	 */
	private int hauteurVue;
	/**
	 * largeur de la vue graphique
	 */
	private int largeurVue;
	/**
	 * plan à afficher par la vue graphique
	 */
	private Plan plan;
	/**
	 * demande de livraison à afficher par la vue graphique
	 */
	private DemandeDeLivraison demandeDeLivraison;
	/**
	 * ensembleDeTournees à afficher par la vue graphique
	 */
	private EnsembleDeTournees ensembleDeTournees;
	/**
	 * objet grapichs de la vue graphique
	 */
	private Graphics g;
	/**
	 * liste de couleurs standards pour les tournees
	 */
	private final Color[] LISTE_COULEUR_DEFAUT={Color.GREEN,
			Color.PINK,
			Color.MAGENTA,
			Color.ORANGE,
			Color.GRAY,
			Color.RED,
			Color.BLUE,
			Color.CYAN,
			Color.DARK_GRAY,
			Color.LIGHT_GRAY,
			new Color(91, 60, 17),
			new Color(240, 195, 0),
			new Color(132, 46, 27),
			new Color(131, 166, 151),
			new Color(153, 122, 144),
			new Color(253, 108, 158),
			new Color(255, 0, 255),
			new Color(9, 106, 9),
			new Color(254, 195, 172),
			new Color(225, 206, 154)};
	/**
	 * liste de couleurs quand il faut plus de couleurs que de couleurs standards disponibles
	 *  pour les tournees
	 */
	private ArrayList<Color> listesDeCouleur;
	/**
	 * troncon selectionne
	 */
	private Troncon tronconSelectionne;
	/**
	 * tournee selectionnee
	 */
	private Tournee tourneeSelectionee;
	/**
	 * noeud temporaire utilisé lors de l'ajout d'un noeud
	 */
	private Noeud noeudTempoAjout;
	/**
	 * booleen permettant de savoir si les objets ont ete tous selectionnés ou non 
	 */
	private boolean isAllSelected;	

	/**
	 * Cree la vue graphique permettant de dessiner le plan plan, la demande de livraison DemandeDeLivraison
	 * et l'ensemble de tournees ensembleDeTournees avec l'echelle e dans la fenetre f
	 * @param plan
	 * @param e l'echelle
	 * @param f la fenetre
	 * @param demandeDeLivraison
	 */
	public VueGraphique(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees ensembleDeTournees, Fenetre f) {
		super();
		plan.addObserver(this); // this observe plan
		demandeDeLivraison.addObserver(this); //this observe demandeDeLivraison
		ensembleDeTournees.addObserver(this);
		this.echelle = 10;
		hauteurVue = plan.getHauteurplan()*echelle;
		largeurVue = plan.getLargeurplan()*echelle;
		setLayout(null);
		setBackground(Color.white);
		setSize(largeurVue, hauteurVue);
		f.getContentPane().add(this);
		this.isAllSelected = true;
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		this.ensembleDeTournees = ensembleDeTournees;
		listesDeCouleur = new ArrayList<Color>();
	}

	/**
	 * Retourne la valeur du booleen isAllSelected
	 * @return isAllSelected 
	 */
	public boolean isIsAllSelected() {
		return isAllSelected;
	}
	/**
	 * Affecte la valeur du booleen isAllSelected à la valeur du parametre
	 * @param isAllSelected 
	 */
	public void setIsAllSelected(boolean isAllSelected) {
		this.isAllSelected = isAllSelected;
	}
	/**
	 * Retourne la valeur du noeud temporaire NoeudTempoAjout
	 * @return noeudTempoAjout 
	 */
	public Noeud getNoeudTempoAjout() {
		return noeudTempoAjout;
	}
	/**
	 * Affecte la valeur du noeud temporaire NoeudTempoAjout à la valeur du parametre	
	 * @param noeudTempoAjout  
	 */
	public void setNoeudTempoAjout(Noeud noeudTempoAjout) {
		this.noeudTempoAjout = noeudTempoAjout;
	}
	/**
	 * Force l'affichage du noeud temporaire
	 */
	public void afficheNoeudTempo(){
		affiche(noeudTempoAjout);
	}

	/**
	 * Methode appelee a chaque fois que VueGraphique doit etre redessinee
	 * N'affiche que les elements non nuls (troncons, tournees, points de livraison et entrepot)
	 * Prend en compte la notion de sélection d'un élément
	 * @param g
	 */
	@Override
	public void paintComponent(Graphics g) {		
		super.paintComponent(g);
		this.g = g;		

		ArrayList<Troncon> troncons = plan.getCollectionTroncons();
		if(troncons != null) {					
			for(int i=0; i<troncons.size(); i++) {
				affiche(troncons.get(i),false, false);					
			}			
		}

		ArrayList<Tournee> tournees = ensembleDeTournees.getListeTournee();
		if(tournees != null) {			
			if(tournees.size()>LISTE_COULEUR_DEFAUT.length) {
				tirerCouleursAleatoires(listesDeCouleur, tournees.size()-LISTE_COULEUR_DEFAUT.length);
			}						
			for(int i=0; i<tournees.size(); i++) {
				if(i < LISTE_COULEUR_DEFAUT.length) {
					g.setColor(LISTE_COULEUR_DEFAUT[i]);
				}else{
					g.setColor(listesDeCouleur.get(i-LISTE_COULEUR_DEFAUT.length));
				}				
				affiche(tournees.get(i));
			}
			//afficheFlecheDesTournees(tournees);
			g.setColor(Color.black);
		}

		ArrayList<PointDeLivraison> pointsDeLivraison = demandeDeLivraison.getPointsDeLivraison();
		if(pointsDeLivraison != null) {			
			for(int i=0; i<pointsDeLivraison.size(); i++) {
				affiche(pointsDeLivraison.get(i));				
			}			
		}

		Entrepot entrepot = demandeDeLivraison.getEntrepot();
		if(entrepot != null) {
			affiche(entrepot);
		}


		if(tronconSelectionne != null)
			afficherTronconSelectionne();
		if(tourneeSelectionee != null)
			afficherTourneeSelectionnee();
		if(noeudTempoAjout != null)
			afficheNoeudTempo();		
	}

	/**
	 * Affecte la valeur de l'échelle de la vue graphique à e
	 * Met à jour les dimensions en prenant en compte cette nouvelle échelle
	 * @param e
	 */
	public void setEchelle(int e) {
		largeurVue = (largeurVue/echelle)*e;
		hauteurVue = (hauteurVue/echelle)*e;
		setSize(largeurVue, hauteurVue);
		echelle = e;
	}
	/**
	 * Retourne la valeur de l'échelle de la vue graphique
	 * @return echelle
	 */
	public int getEchelle() {
		return echelle;
	}
	/**
	 * Retourne la valeur de la hauteur de la vue graphique
	 * @return hauteurVue
	 */
	public int getHauteur() {
		return hauteurVue;
	}
	/**
	 * Retourne la largeur de la hauteur de la vue graphique
	 * @return largeurVue
	 */
	public int getLargeur() {
		return largeurVue;
	}
	/**
	 * Retourne le nombre de points de livraison de la demande de livraison
	 * @return le nb de points de livraison
	 */
	public int getNbPointsDeLivraison() {
		return demandeDeLivraison.getPointsDeLivraison().size();
	}

	/**
	 * Affecte la valeur du troncon selectionne à t
	 * @param t
	 */
	public void setTronconSelectionne(Troncon t)
	{
		tronconSelectionne = t;
	}
	/**
	 * Affecte la valeur de la tournee selectionnee à to
	 * @param to
	 */
	public void setTourneeSelectionne(Tournee to)
	{
		tourneeSelectionee = to;
	}

	/**
	 * Methode appelee par les objets observes par this a chaque fois qu'ils ont ete modifies
	 * Afin de repaint la vue textuelle
	 */
	@Override
	public void update(Observable o, Object arg) {		
		repaint();		
		revalidate();
	}
	/**
	 * Permet de tirer des couleurs aléatoires et les affecter dans un tableau de taille taille
	 * @param tabCouleur
	 * @param taille
	 */
	public static void tirerCouleursAleatoires(ArrayList<Color> tabCouleur , int taille)
	{
		tabCouleur.clear();
		for(int m=0 ; m <taille; m++)
		{
			int composanteRouge = new Random().nextInt(255);
			int composanteVerte = new Random().nextInt(255);
			int composanteBleue = new Random().nextInt(255);
			tabCouleur.add(new Color(composanteRouge, composanteVerte,composanteBleue ));
		}

	}	
	/**
	 * Retourne le plan de la vue graphique
	 * @return plan
	 */
	public Plan getPlan() {
		return plan;
	}
	/**
	 * Affecte la valeur du plan de la vue graphique à plan et l'observe
	 * @param plan
	 */
	public void setPlan(Plan plan) {
		this.plan = plan;
		plan.addObserver(this);
	}
	/**
	 * Retourne la demande de livraison de la vue graphique
	 * @return demandeDeLivraison
	 */
	public DemandeDeLivraison getDemandeDeLivraison() {
		return demandeDeLivraison;
	}
	/**
	 * Affecte la valeur de la demande de livraison de la vue graphique à demandeDeLivraison
	 * et l'observe
	 * @param demandeDeLivraison
	 */
	public void setDemandeDeLivraison(DemandeDeLivraison demandeDeLivraison) {
		this.demandeDeLivraison = demandeDeLivraison;
		demandeDeLivraison.addObserver(this);
	}
	/**
	 * Retourne l'ensemble des tournees de la vue graphique
	 * @return ensembleDeTournees
	 */
	public EnsembleDeTournees getEnsembleDeTournees() {
		return ensembleDeTournees;
	}
	/**
	 * Affecte la valeur d'ensemble de tournees de la vue graphique à ensembleDeTournees
	 * et l'observe
	 * @param ensembleDeTournees
	 */
	public void setEnsembleDeTournees(EnsembleDeTournees ensembleDeTournees) {
		this.ensembleDeTournees = ensembleDeTournees;
		ensembleDeTournees.addObserver(this);
	}
	/**
	 * Force l'affichage en surbrillance du troncon selectionné
	 * @deprecated
	 */
	public void afficherTronconSelectionne(){		
		g.setColor(Color.YELLOW);
		affiche(tronconSelectionne,true, false);
		g.setColor(Color.BLACK);
	}
	/**
	 * Force l'affichage en surbrillance de la tournée selectionnée
	 * @deprecated
	 */
	public void afficherTourneeSelectionnee(){
		int posTournee = tourneeSelectionee.getLivreur();
		if(posTournee <= LISTE_COULEUR_DEFAUT.length){
			g.setColor(LISTE_COULEUR_DEFAUT[posTournee]);
		}else{
			g.setColor(Color.YELLOW);
		}		
		affiche(tourneeSelectionee);
		g.setColor(Color.BLACK);
	}
	/**
	 * Affichage du noeud n dans le graphics g
	 * @param n
	 */
	public void affiche(Noeud n) {		
		int ynoeud = convertirLatitude(n.getLatitude());
		int xnoeud = convertirLongitude(n.getLongitude());	
		g.setColor(Color.cyan);
		g.fillOval(xnoeud-5,ynoeud-5,10,10);

	}
	/**
	 * Affichage du noeud t dans le graphics g
	 * L'affichage sera différent si il est dans une tournee : coloration homogène à la couleur de la tournée
	 * L'affichage sera différent si il est dans une tournee : surbrillance
	 * @param t
	 * @param isTournee
	 * @param isInTourneeSelec
	 */
	public void affiche(Troncon t, boolean isTournee, boolean isInTourneeSelec) {		
		int yori = convertirLatitude(t.getNoeudOrigine().getLatitude());
		int xori = convertirLongitude(t.getNoeudOrigine().getLongitude());
		int ydest = convertirLatitude(t.getNoeudDestination().getLatitude());
		int xdest = convertirLongitude(t.getNoeudDestination().getLongitude());
		
		if(!isTournee) {
			g.drawLine(xori, yori, xdest , ydest);
		}else{
			Graphics2D g2 = (Graphics2D) g;
			if(!isInTourneeSelec)			
				g2.setStroke(new BasicStroke(3));
			else
				g2.setStroke(new BasicStroke(5));

			g2.draw(new Line2D.Float(xori, yori, xdest , ydest));
			//dessineFleche(g2, xori, xdest, yori, ydest);		

		}		
	}
	/**
	 * Affichage de la tournee tour dans le graphics g
	 * @param tour
	 */
	public void affiche(Tournee tour) {
		//changer le design si il est selectionne
		ArrayList<Pair<PointDeLivraison,Trajet>> listesPointTrajet = tour.getListePointTrajet();
		for(int j=0 ; j<listesPointTrajet.size();j++) {
			Trajet trajetTempo = listesPointTrajet.get(j).getSecond();
			if((tour.isEstSelectionne() && !isAllSelected)) {
				affiche(trajetTempo, true);
			}else{
				affiche(trajetTempo, false);
			}	 
		}		
	}
	/**
	 * Affichage du trajet traj dans le graphics g
	 * Si le trajet est dans une tourneeSelectionne parametre eponyme à true : surbrillance
	 * @param traj
	 * @param estDansTourneeSelectionee
	 */
	public void affiche(Trajet traj, boolean estDansTourneeSelectionee) {		
		ArrayList<Troncon> parcoursTroncons = traj.getParcoursTroncons();
		for(int k=0 ; k <parcoursTroncons.size(); k++) {
			if(estDansTourneeSelectionee)
				affiche(parcoursTroncons.get(k), true, true);
			else
				affiche(parcoursTroncons.get(k), true, false);
		}		
	}

	/**
	 * Affichage du point de livraison l dans le graphics g
	 * @param l
	 */
	public void affiche(PointDeLivraison l) {

		int ynoeud = convertirLatitude(l.getLatitude());
		int xnoeud = convertirLongitude(l.getLongitude());
		if(l.isEstSelectionne() && !isAllSelected) {
			g.setColor(Color.red);
			g.fillOval(xnoeud-10,ynoeud-10,20,20);

		}else{
			g.setColor(Color.red);
			g.fillOval(xnoeud-5,ynoeud-5,10,10);
		}		
	}
	/**
	 * Affichage de l'entrepôt p dans le graphics g
	 * @param p
	 */
	public void affiche(Entrepot p){		
		int ynoeud = convertirLatitude(p.getLatitude());
		int xnoeud = convertirLongitude(p.getLongitude());	
		g.setColor(Color.blue);
		g.fillOval(xnoeud-8,ynoeud-8,16,16);		
	}	
	/**
	 * Convertit une latitude en abscisse de la fenetre
	 * Prend en compte le retournement vertical des coordonnées
	 * @param lat une latitiude
	 * @return l'abscisse associée dans la fenetre
	 */
	public int convertirLatitude(double lat)
	{
		double rapportLatEchelle = plan.getLatmax()-plan.getLatmin();
		double rapportAuMinLat = lat-plan.getLatmin();
		int resultat = (int)((rapportAuMinLat/rapportLatEchelle)* plan.getLargeurplan()*echelle);		
		return (plan.getLargeurplan()*echelle-resultat);
	}
	/**
	 * Convertit une longitude en ordonnée de la fenetre
	 * @param longi une longitude
	 * @return l'ordonnée associée dans la fenetre
	 */
	public int convertirLongitude(double longi)
	{
		double rapportLongEchelle = plan.getLongmax()-plan.getLongmin();
		double rapportAuMinLong = longi - plan.getLongmin();
		int resultat = (int)((rapportAuMinLong/rapportLongEchelle)*plan.getHauteurplan()*echelle);		

		return resultat;
	}
	/**
	 * Convertit une ordonnée de la fenetre en longitude
	 * @param x une ordonnée
	 * @return longitude
	 */
	public double convertirXEnLongitude(int x)
	{
		double rapportLongEchelle =  plan.getLongmax()-plan.getLongmin();
		double res = (x * (rapportLongEchelle)/(plan.getHauteurplan()*echelle)) +  plan.getLongmin();		
		return res;		
	}
	/**
	 * Convertit une abscisse de la fenetre en latitude
	 * @param y une abscisse
	 * @return latitude
	 */
	public double convertirYEnLatitude(int y)
	{
		double rapportLatEchelle = plan.getLatmax()-plan.getLatmin();
		double ymax = plan.getLargeurplan()*echelle ;
		double res = ((ymax -y) *(rapportLatEchelle)/(plan.getLargeurplan()*echelle)) + plan.getLatmin() ;		
		return res;
	}
	/**
	 * AFfiche des fleches dans une liste de tournees
	 * @param tournees	
	 */
	public void afficheFlecheDesTournees(ArrayList<Tournee> tournees){
		for(int i=0; i<tournees.size(); i++)
		{
			if(i <= LISTE_COULEUR_DEFAUT.length) {
				g.setColor(LISTE_COULEUR_DEFAUT[i]);
			}else{
				g.setColor(listesDeCouleur.get(i-LISTE_COULEUR_DEFAUT.length));
			}
			Graphics2D g2 = (Graphics2D) g;
			ArrayList<Pair<PointDeLivraison,Trajet>> trajets = tournees.get(i).getListePointTrajet();
			for(int j=0 ; j <trajets.size(); j++) {
				ArrayList<Troncon> troncons = trajets.get(j).getSecond().getParcoursTroncons();
				for(int k=0; k<troncons.size(); k++) {
					int yori = convertirLatitude(troncons.get(k).getNoeudOrigine().getLatitude());
					int xori = convertirLongitude(troncons.get(k).getNoeudOrigine().getLongitude());
					int ydest = convertirLatitude(troncons.get(k).getNoeudDestination().getLatitude());
					int xdest = convertirLongitude(troncons.get(k).getNoeudDestination().getLongitude());
					dessineFleche(g2, xori, xdest, yori, ydest);					
				}
			}			
		}
	}

	/**
	 * Affiche une fleche dans un graphique g2 d'un point d'origine (xori,yori) à un point d'arrivée (xdest,ydest)
	 * @param g2	
	 * @param xori
	 * @param xdest
	 * @param yori
	 * @param ydest
	 */
	public void dessineFleche(Graphics2D g2,int xori, int xdest,int yori, int ydest){
		g2.setStroke(new BasicStroke(2));

		int xmoitie = (xori+xdest)/2;
		int ymoitie = (yori+ydest)/2;

		int tailleFleche = 15;	

		double angleAbscisse;
		double angleFleche = Math.PI / 12;

		int deltaX = (xdest-xori);
		int deltaY = (ydest-yori);

		//Cas du Troncon Vertical
		if (deltaX == 0.0d) {
			angleAbscisse = Math.PI / 2;
		}else{
			angleAbscisse = Math.atan(((double)deltaY /(double)deltaX));			
		}

		double x1 = (double) xmoitie + ( tailleFleche * Math.cos(angleAbscisse - angleFleche));
		double y1 = (double) ymoitie + ( tailleFleche * Math.sin(angleAbscisse - angleFleche));
		double x2 = (double) xmoitie + ( tailleFleche * Math.cos(angleAbscisse + angleFleche));
		double y2 = (double) ymoitie + ( tailleFleche * Math.sin(angleAbscisse + angleFleche));		

		g2.draw(new Line2D.Double(xmoitie, ymoitie, x1, y1));
		g2.draw(new Line2D.Double(xmoitie, ymoitie, x2, y2));
		
	}	
}
