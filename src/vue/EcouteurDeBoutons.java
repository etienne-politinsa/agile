package vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import controleur.Controleur;

/**
 * @author Arthur BELLEMIN et Thibault REMY, architecture initiale inspirée de l'application PlaCo
 *
 */
public class EcouteurDeBoutons implements ActionListener {
	/**
	 * controleur associé à l'écouteur
	 */
	private Controleur controleur;
	/**
	 * fenetre écoutée par le présent ecouteur de bouton
	 */
	private Fenetre fenetre;

	/**
	 * Crée un écouteur de boutons
	 * @param controleur le controleur associé à la fenetre qui crée l'écouteur
	 * @param fen la fenetre écoutée par le présent ecouteur de bouton	 
	 */
	public EcouteurDeBoutons(Controleur controleur,Fenetre fen) {
		this.controleur = controleur;
		this.fenetre = fen;
	}

	/**
	 * Override de actionPerformed, dispatche les clics sur les divers boutons de la fenetre et effectue les bons
	 * appels vers le contrôleur
	 */
	@Override
	public void actionPerformed(ActionEvent e) { 		
		switch (e.getActionCommand()) {			
		case Fenetre.CHARGER_PLAN: 
			controleur.chargerPlan(); 
			break;
		case Fenetre.CHARGER_DEMANDE_DE_LIVRAISON: 
			controleur.chargerLivraisons(); 
			break;
		case Fenetre.RENSEIGNER_NB_LIVREURS:
			try {
				fenetre.lancerInterfaceConfigNbLivreurs();
				controleur.renseignerNbLivreurs(); 				
				break;
			} catch (Exception exception) {
				break;
			}
		case Fenetre.CALCULER_TOURNEES:				
			controleur.chargerTournees();				
			break;
		case Fenetre.AJOUTER_LIVRAISON:
			try{
				fenetre.lancerInterfaceConfigDuree();
			}catch (Exception exception){
				break;
			}
			controleur.ajouter();						
			break;
		case Fenetre.SUPPRIMER_LIVRAISON:
			controleur.supprimer();
			break;
		case Fenetre.DEPLACER_LIVRAISON:
			controleur.deplacer();
			break;
		case Fenetre.INTERROMPRE_LIVRAISON:
			controleur.stopCalcul();
			break;
		}
	}
}
