package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Noeud;
import modele.Plan;
import modele.Tournee;
import modele.Troncon;
import controleur.Controleur;


/**
 * @author Arthur BELLEMIN et Thibault REMY, architecture initiale inspirée de l'application PlaCo
 */
public class Fenetre extends JFrame implements ComponentListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Intitules des boutons de la fenetre	
	/**
	 * texte associé au bouton de chargement du plan
	 */
	public final static String CHARGER_PLAN = "Charger un plan";
	/**
	 * texte associé au bouton de chargement de la demande de livraison
	 */
	public final static String CHARGER_DEMANDE_DE_LIVRAISON = "Charger une demande de livraison";
	/**
	 * texte associé au bouton pour renseigner le nombre de livreurs
	 */
	public final static String RENSEIGNER_NB_LIVREURS = "Renseigner le nombre de livreurs";
	/**
	 * texte associé au bouton pour calculer les tournees
	 */
	public final static String CALCULER_TOURNEES = "Calculer les tournees";
	/**
	 * texte associé au bouton pour ajouter les livraisons
	 */
	public final static String AJOUTER_LIVRAISON = "Ajouter une livraison";
	/**
	 * texte associé au bouton pour la suppression des livraisons
	 */
	public final static String SUPPRIMER_LIVRAISON = "Supprimer une livraison";
	/**
	 * texte associé au bouton pour le deplacement des livraisons
	 */
	public final static String DEPLACER_LIVRAISON = "Deplacer une livraison";
	/**
	 * texte associé au bouton pour l'interruption des livraisons
	 */
	public final static String INTERROMPRE_LIVRAISON = "Interrompre le calcul";
	/**
	 * liste des boutons contenus dans la fenetre
	 */
	private ArrayList<JButton> boutons;
	/**
	 * cadre contenant des messages pour l'utilisateur
	 */
	private JLabel cadreMessages;
	/**
	 * cadre contenant le nombre de livreurs
	 */
	private JLabel cadreLivreur;
	/**
	 * cadre contenant le nombre de la rue courante
	 */
	private JLabel cadreNomRue;
	/**
	 * vue/espace de la fenetre  permettant d'afficher graphiquement les objets
	 * du systeme (plan, demande de livraison, tournées ...)
	 */
	private VueGraphique vueGraphique;
	/**
	 * vue/espace de la fenetre  permettant d'afficher textuellement les objets
	 * du systeme (plan, demande de livraison, tournées ...)
	 */
	private VueTextuelle vueTextuelle;
	/**
	 * barre de defilement de la vue textuelle
	 */
	private JScrollPane scrollTexte;
	/**
	 * barre de defilement de la liste
	 */
	private JScrollPane scrollListe;
	/**
	 * liste cliquable pouvant contenir différents objets du systeme (livraisons, tournees, ...)
	 */
	private ListeElement vueListeTournee;
	/**
	 * l'écouteur de boutons de la fenetre
	 */
	private EcouteurDeBoutons ecouteurDeBoutons;
	/**
	 * l'écouteur de souris de la fenetre
	 */
	private EcouteurDeSouris ecouteurDeSouris;
	/**
	 * l'écouteur de clavier de la fenetre
	 */
	private EcouteurDeClavier ecouterDeClavier;
	/**
	 * liste des intitulés des boutons de la fenetre
	 */
	private final String[] intitulesBoutons = new String[]{CHARGER_PLAN, CHARGER_DEMANDE_DE_LIVRAISON, 
			RENSEIGNER_NB_LIVREURS,CALCULER_TOURNEES, INTERROMPRE_LIVRAISON, AJOUTER_LIVRAISON,SUPPRIMER_LIVRAISON, DEPLACER_LIVRAISON};
	/**
	 * hauteur constante d'un bouton standard
	 */
	private final int hauteurBouton = 40;
	/**
	 * largeur constante d'un bouton standard
	 */
	private final int largeurBouton = 200;
	/**
	 * hauteur constante du cadre de messages
	 */
	private final int hauteurCadreMessages = 100;
	/**
	 * hauteur constante du cadre d'affichage du nombre de livreurs
	 */
	private final int hauteurCadreLivreurs = 50;
	/**
	 * hauteur constante du cadre d'affichage du nom de la rue
	 */
	private final int hauteurCadreNomRue = 50;
	/**
	 * largeur de la liste de selection
	 */
	private final int largeurVueTournee = 100;
	/**
	 * largeur de la vue textuelle
	 */
	private final int largeurVueTextuelle = 420;
	/**
	 * largeur temporaire utilisee pour le redimensionnement de fenetre
	 */
	private int largeurTempo = 0;
	/**
	 * nombre de livreurs, valeur par défaut à 1
	 */
	private int valueLivreur = 1;
	/**
	 * durée entrée par l'utilisateur dans le cas d'un ajout de point de livraison
	 */
	private int dureeAddPdl = 0;


	/**
	 * Cree une fenetre avec des boutons, une zone graphique pour dessiner le plan plan avec la demande de livraison demandeDeLivraison, 
	 * un cadre pour afficher des messages, une zone textuelle decrivant les formes de p,
	 * et des ecouteurs de boutons, de clavier et de souris qui envoient des messages au controleur c. Elle peut aussi afficher
	 * les tournées présentent dans l'ensemble de tournées edt
	 * @param plan le plan
	 * @param demandeDeLivraison la demande de livraison
	 * @param edt l'ensemble de tournées
	 * @param controleur le controleur
	 */
	public Fenetre(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees edt, Controleur controleur){
		setLayout(null);
		creeBoutons(controleur);

		cadreMessages = new JLabel();
		cadreMessages.setBorder(BorderFactory.createTitledBorder("Messages..."));

		cadreLivreur = new JLabel();
		cadreLivreur.setBorder(BorderFactory.createTitledBorder("Nombre Livreurs :"));	
		cadreLivreur.setText(Integer.toString(valueLivreur));		

		cadreNomRue = new JLabel();
		cadreNomRue.setBorder(BorderFactory.createTitledBorder("Rue selectionnée:"));
		cadreNomRue.setText("Affichage Rue");

		getContentPane().add(cadreMessages);
		getContentPane().add(cadreLivreur);
		getContentPane().add(cadreNomRue);
		getContentPane().addComponentListener(this);

		vueGraphique = new VueGraphique(plan, demandeDeLivraison, edt, this);		
		vueTextuelle = new VueTextuelle(plan, demandeDeLivraison, edt, this);
		vueListeTournee = new ListeElement(edt, demandeDeLivraison, this, vueGraphique, vueTextuelle, controleur);
		getContentPane().add(vueListeTournee);
		//this.setExtendedState(MAXIMIZED_BOTH);

		ecouteurDeSouris = new EcouteurDeSouris(controleur,vueGraphique,this);
		addMouseListener(ecouteurDeSouris);
		addMouseMotionListener(ecouteurDeSouris);

		ecouterDeClavier = new EcouteurDeClavier(controleur);
		this.setFocusable(true);
		addKeyListener(ecouterDeClavier);
		vueListeTournee.addKeyListener(ecouterDeClavier);

		scrollTexte = new JScrollPane(vueTextuelle,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scrollTexte);

		scrollListe = new JScrollPane(vueListeTournee,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scrollListe);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		this.setTitle("Livraisons by Hexa'llez les Bleus");


		setTailleFenetre();
		setVisible(true);
	}

	/**
	 * Cree les boutons correspondant aux intitules contenus dans intitulesBoutons
	 * cree un ecouteur de boutons qui ecoute ces boutons
	 * inspiré d'une méthode similaire de Placo
	 * @param controleur
	 */
	private void creeBoutons(Controleur controleur){
		ecouteurDeBoutons = new EcouteurDeBoutons(controleur, this);
		boutons = new ArrayList<JButton>();
		for (String intituleBouton : intitulesBoutons) {
			JButton bouton = new JButton(intituleBouton);
			boutons.add(bouton);
			bouton.setFont (new Font ("TimesRoman", Font.BOLD | Font.ITALIC, 12));
			bouton.setSize(largeurBouton,hauteurBouton);
			bouton.setLocation(0,(boutons.size()-1)*hauteurBouton);
			bouton.setFocusable(false);
			bouton.setFocusPainted(false);
			bouton.setBackground(new Color(29, 68, 87));
			bouton.setForeground(Color.WHITE);			
			bouton.addActionListener(ecouteurDeBoutons);
			getContentPane().add(bouton);	
		}
	}

	/**
	 * Definit la taille du cadre et de ses composants en fonction de la taille de la vue
	 * inspiré de l'architecture d'une méthode similaire de PlaCo
	 * @param largeurVue
	 * @param hauteurVue
	 */
	private void setTailleFenetre() {
		int hauteurBoutons = hauteurBouton*intitulesBoutons.length;
		int hauteurFenetre = Math.max(vueGraphique.getHauteur(),hauteurBoutons)+hauteurCadreMessages;
		int largeurFenetre = vueGraphique.getLargeur()+largeurBouton+largeurVueTextuelle+60+largeurVueTournee;
		largeurTempo = largeurFenetre;
		setSize(largeurFenetre, hauteurFenetre);

		cadreMessages.setSize(largeurFenetre,60);
		cadreMessages.setLocation(0,hauteurFenetre-hauteurCadreMessages);

		cadreLivreur.setSize(largeurBouton,hauteurCadreLivreurs);
		cadreLivreur.setLocation(0,hauteurFenetre-hauteurCadreMessages-hauteurCadreLivreurs);

		cadreNomRue.setSize(largeurBouton,hauteurCadreNomRue);
		cadreNomRue.setLocation(0,hauteurFenetre-hauteurCadreMessages-hauteurCadreLivreurs-hauteurCadreNomRue);

		vueGraphique.setLocation(largeurBouton, 0);

		scrollListe.setLocation(20+vueGraphique.getLargeur()+largeurBouton,0);
		scrollListe.setSize(largeurVueTournee,hauteurFenetre-hauteurCadreMessages);

		scrollTexte.setSize(largeurVueTextuelle,hauteurFenetre-hauteurCadreMessages);
		scrollTexte.setLocation(20+vueGraphique.getLargeur()+largeurBouton+largeurVueTournee,0);
	}

	/**
	 * Affiche message dans la fenetre de dialogue avec l'utilisateur
	 * Méthode tirée de PlaCo
	 * @param message
	 */
	public void afficheMessage(String message) {
		cadreMessages.setText(message);
	}	

	/**
	 * Active les boutons si b = true, les desactive sinon	 * 
	 * Méthode inspirée de PlaCo
	 * @param b
	 */
	public void autoriseBoutons(Boolean b) {
		for (JButton bouton : boutons)
			bouton.setEnabled(b);
	}
	/** 
	 * @return l'échelle de la vue graphique
	 */
	public int getEchelle(){
		return vueGraphique.getEchelle();
	}

	/**
	 * Active le boutons dont l'intitulé est str si b = true, le desactive sinon	 * 
	 * Méthode inspirée de PlaCo
	 * @param str
	 * @param active
	 */
	public void ActiverBouton(String str, boolean active) {
		JButton toDisable = getButtonByString(str);
		if (toDisable != null)
			toDisable.setEnabled(active);
	}
	/**
	 * Renvoie le bouton décrit par l'intitulé str 
	 * Méthode inspirée de PlaCo
	 * @param str
	 * @param active
	 */
	private JButton getButtonByString(String str) {
		for (JButton b: boutons) {
			if (b.getText().equals(str))
				return b;
		}
		return null;
	}

	/**
	 * Set la valeur de la tournee selectionnee de la vue à la valeur to
	 * @param to
	 * @param active
	 */
	public void setTourneeSelectionee(Tournee to)
	{
		vueGraphique.setTourneeSelectionne(to);
	}

	/**	
	 * Affecte l'échelle de la vueGraphique à echelle et redimensionne la fenetre
	 * Méthode inspirée de PlaCo
	 * @param echelle	 
	 */
	public void setEchelle(int echelle){
		vueGraphique.setEchelle(echelle);
		setTailleFenetre();
	}
	/**	
	 * Affecte le nom de la rue nomRue dans le cadre du nom de rue	 
	 * @param nomRue	 
	 */
	public void setNomDuRueDansCadre(String nomRue)
	{
		cadreNomRue.setText(nomRue);
	}
	/**	
	 * Force l'affichage d'un noeud dans la vue graphique 
	 * @param noeud	 
	 */
	public void afficherNoeud(Noeud noeud)
	{
		vueGraphique.setNoeudTempoAjout(noeud);		
		vueGraphique.repaint();
	}
	/**	
	 * Place à une valeur nulle le noeud temporaire de la vue graphique	 
	 */
	public void cleanNoeudTemporaire()
	{
		vueGraphique.setNoeudTempoAjout(null);
	}
	/**	
	 * Affecte le plan des differentes vues (graphique, textuelle, liste) à la valeur p
	 * @param p	 
	 */
	public void setPlan(Plan p)
	{
		vueGraphique.setPlan(p);
		vueGraphique.setTourneeSelectionne(null);
		vueGraphique.setTronconSelectionne(null);
		vueTextuelle.setPlan(p);
		vueListeTournee.majListe();
	}
	/**	
	 * Affecte l'ensemble de tournées des differentes vues (graphique, textuelle, liste) à la valeur edt
	 * @param edt 
	 */
	public void setEnsembleDeTournees(EnsembleDeTournees edt)
	{
		vueGraphique.setEnsembleDeTournees(edt); 
		vueTextuelle.setEnsembleDeTournees(edt);
		vueListeTournee.setEnsembleDeTournees(edt);
		vueListeTournee.majListe();
	}
	/**	
	 * Affecte la demande de livraison des differentes vues (graphique, textuelle, liste) à la valeur ddl
	 * @param ddl 
	 */
	public void setDemandeDeLivraison(DemandeDeLivraison ddl)
	{
		vueGraphique.setDemandeDeLivraison(ddl);
		vueTextuelle.setDemandeDeLivraison(ddl);
		vueListeTournee.setDemandeDeLivraison(ddl);
		vueListeTournee.majListe();
	}
	/**	
	 * Lance l'interface de configuration du nombre de livreurs permettant 
	 * à l'utilisateur de selectionner un nombre proposé compris entre 
	 * 1 et le nombre de point de livraison
	 * @throws Exception de fermeture de pop-up
	 */
	public void lancerInterfaceConfigNbLivreurs() throws Exception {
		Object[] possibilities  = {"1", "2", "3", "4","5","6","7","8","9","10"};
		try{
			int nbPointDeLivraison = vueGraphique.getNbPointsDeLivraison();
			possibilities = new Object[nbPointDeLivraison];		
			for(int i=0; i<nbPointDeLivraison; i++) {
				possibilities[i] = Integer.toString(i+1);
			}
		}catch(Exception e){

		}		

		String s = (String)JOptionPane.showInputDialog(
				this,
				"Veuillez renseigner le nombre de livreurs:\n"
				,
				"Configuration du nombre de livreurs",
				JOptionPane.PLAIN_MESSAGE,
				null,
				possibilities,
				"1");

		valueLivreur = Integer.parseInt(s);

	}

	/**	
	 * Affecte et met à jour la valeur du nombre de livreur dans la fenetre et dans le cadre dédié
	 * @param nbLivreur 
	 */
	public void setNbLivreursDsFenetre(int nbLivreur){
		valueLivreur = nbLivreur;
		cadreLivreur.setText(Integer.toString(valueLivreur));
	}

	/**	
	 * Retourne la valeur du nombre de livreurs défini dans la fenetre
	 * @return valueLivreur  
	 */
	public int getValueNbLivreur(){
		return valueLivreur;
	}

	/**	
	 * Lance l'interface de configuration de la durée de livraison lors d'un ajout de
	 * livraison, permet à l'utilisateur d'entrer une valeur correspondant à la durée en minutes
	 * @throws Exception de fermeture de pop-up
	 */
	public void lancerInterfaceConfigDuree() throws Exception{
		String retourInterface = JOptionPane.showInputDialog(this
				, "Rentrez la duree de livraison en secondes"
				,0
				);
		dureeAddPdl = Integer.parseInt(retourInterface);
	}
	/**	
	 * Retourne la liste contenue dans la fenetre
	 * @return vueListeTournee
	 */
	public ListeElement getVueListeTournee() {
		return vueListeTournee;
	}
	/**	
	 * Affecte la liste contenue dans la fenetre à vueListeTournee
	 * @param vueListeTournee
	 */
	public void setVueListeTournee(ListeElement vueListeTournee) {
		this.vueListeTournee = vueListeTournee;
	}
	/**	
	 * Retourne la durée entrée de la livraison dernièrement ajoutée
	 * @return dureeAddPdl
	 */
	public int getDureeAddPdl(){
		return dureeAddPdl;
	}
	/**	
	 * Controle la notion de selection/affichage d'objets dans la vue textuelle
	 * @param entrepot si true affichera l'entrepot sinon non
	 * @param livraisons si true affichera les livraisons sinon non
	 * @param tournees	si true affichera les tournees sinon non
	 */
	public void controleAffichageEnsembleVueTextuelle(boolean entrepot, boolean livraisons, boolean tournees)
	{
		vueTextuelle.setAfficheEntrepot(entrepot);
		vueTextuelle.setAfficheLivraisons(livraisons);
		vueTextuelle.setAfficheTournees(tournees);
		vueTextuelle.affiche();
	}
	/**	
	 * Affecte la valeur de desequilibre à la valeur du paramètre desequilibre
	 * Met à jour la vue textuelle en prenant en compte cette information
	 * @param desequilibre	 
	 */
	public void setDesequilibre(boolean desequilibre){
		vueTextuelle.gererDesequilibre(desequilibre);
		vueTextuelle.affiche();
	}
	/**	
	 * Affecte la valeur de la tournee selectionee à tourneeProche
	 * Indique à la vue graphique que la selection sur les objets n'est pas exhaustive
	 * @param tourneeProche	 
	 */
	public void selectionnerTournee(Tournee tourneeProche)
	{
		vueGraphique.setTourneeSelectionne(tourneeProche);
		vueGraphique.setIsAllSelected(false);
	}
	
	/**	
	 * Affecte la valeur du troncon selectioné à troncon
	 * Force le réaffichage de la vue graphique pour le nouveau troncon
	 * @param tourneeProche	 
	 */
	public void selectionnerTroncon(Troncon troncon)
	{
		vueGraphique.setTronconSelectionne(troncon) ;
		vueGraphique.repaint();
		this.setNomDuRueDansCadre(troncon.getNom());
	}

	/**	
	 * Surcharge la méthode de redimensionnement de la fenetre
	 * Permet de redimensionner les divers composants de la fenetre dans une logique responsive
	 * @param e Evenement	 
	 */
	@Override
	public void componentResized(ComponentEvent e) {

		int newWidth = this.getWidth();
		
		if(largeurTempo != newWidth) {
			double coeffResponsiveWidth = (newWidth/1140.0);			
			int newEchelle = (int) (coeffResponsiveWidth *10);
			setEchelle(newEchelle);

		}

	}

	@Override
	public void componentMoved(ComponentEvent e) {	
	}

	@Override
	public void componentShown(ComponentEvent e) {		
	}

	@Override
	public void componentHidden(ComponentEvent e) {		
	}	

}
