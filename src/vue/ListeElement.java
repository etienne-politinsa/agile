package vue;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import controleur.Controleur;
import modele.*;

/**
 * @author Arthur BELLEMIN
 */
public class ListeElement extends JList<String> implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * la demande de livraison de la liste
	 */
	private DemandeDeLivraison dedel;
	/**
	 * l'ensemble de tournees de la liste
	 */
	private EnsembleDeTournees edt;	
	/**
	 * les elements de la liste
	 */
	private DefaultListModel<String> elementsListe;
	/**
	 * la vue graphique associée à la liste dans une fenetre commune
	 */
	private VueGraphique vueGraphique;
	/**
	 * la vue textuelle associée à la liste dans une fenetre commune
	 */
	private VueTextuelle vueTextuelle;
	/**
	 * constante pour affichage dans la liste pour tout voir
	 */	
	public final String TOUT_VOIR = "--Tout voir--";
	/**
	 * constante pour affichage dans la liste pour voir l'entrepot
	 */	
	public final String VOIR_ENTREPOT = "--Entrepot--";
	/**
	 * constante pour affichage dans la liste pour voir les livraisons
	 */	
	public final String TOUT_VOIR_LIVRAISON = "--Livraisons--";
	/**
	 * constante pour affichage dans la liste pour voir les tournees
	 */	
	public final String TOUT_VOIR_TOURNEES = "--Tournees--";

	/**
	 * Crée une liste d'element qui sera integrée à la fenetre fen au côté de la vue graphique vg et de la vue textuelle vt
	 * permettant de selectionner
	 * des tournees de l'ensemble de tournees edt ou des points livraisons ou entrepot
	 * de la demande de livraison dedel, elle sera commandée par le controleur controleur
	 * @param edt
	 * @param dedel
	 * @param fenetre
	 * @param vg
	 * @param vt
	 * @param controleur
	 */	
	public ListeElement(EnsembleDeTournees edt, DemandeDeLivraison dedel, Fenetre fen, VueGraphique vg, VueTextuelle vt, Controleur controleur)
	{
		super();
		this.edt = edt;
		this.dedel = dedel;	
		this.vueGraphique = vg;
		this.vueTextuelle = vt;	

		dedel.addObserver(this);
		edt.addObserver(this);		

		elementsListe = new DefaultListModel<String>();
		this.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		this.setSelectionForeground(new Color(55,60,103));
		this.setLayoutOrientation(JList.VERTICAL);
		this.setVisibleRowCount(-1);	

		//Correction de bug
		this.setFocusable(true);	

		EcouteurDeListe ecouteurDeListe = new EcouteurDeListe(this, controleur);
		this.addListSelectionListener(ecouteurDeListe);		
	}

	@Override
	/**
	 * Surcharge de la methode update appelée quand une information de changement
	 * sur la demande de livraison ou l'ensemble de tournées observés est notifiée
	 * Elle met à jour la liste
	 * @param o
	 * @parma arg
	 */	
	public void update(Observable o, Object arg) {			
		majListe();
	}

	/**
	 * Méthode de mise à jour de la liste, remettant à zéro les elements et ajoutant (si les objets existent)
	 * les tournees de l'ensemble de tournées edt ainsi que l'entrepot et les livraisons 
	 * de la demande de livraison dedel. Certaines constantes sont également ajoutées
	 * comme l'option permettant de tout voir ou un type d'objet de manière exhaustive
	 * (toutes les tournées, toutes les livraisons, ...)	 
	 */
	public void majListe(){
		elementsListe.clear();

		if(dedel != null && dedel.getEntrepot()!=null) {
			elementsListe.addElement(TOUT_VOIR);
			if(dedel.getEntrepot()!=null) {
				elementsListe.addElement(VOIR_ENTREPOT);
			}
			elementsListe.addElement(TOUT_VOIR_LIVRAISON);
			ArrayList<PointDeLivraison> pdls = dedel.getPointsDeLivraison();
			if(pdls!=null) {
				for(int i=0; i<pdls.size(); i++) {
					elementsListe.addElement("Livraison n°" + Integer.toString(i+1));
				}
			}
			if(edt != null) {
				elementsListe.addElement(TOUT_VOIR_TOURNEES);
				ArrayList<Tournee> tournees = edt.getListeTournee();
				for(int i=0; i<tournees.size(); i++) {
					elementsListe.addElement("Tournee n°" + Integer.toString(i+1));
				}

			}
		}		
		this.setModel(elementsListe);	
	}
	/**
	 * Retourne la vue graphique associée à la liste
	 * @return vueGraphique	 
	 */
	public VueGraphique getVueGraphique() {
		return vueGraphique;
	}	
	/**
	 * Retourne la vue textuelle associée à la liste
	 * @return vueTextuelle 
	 */
	public VueTextuelle getVueTextuelle() {
		return vueTextuelle;
	}
	/**
	 * Affecte la demande de livraison à la valeur dedel
	 * Observe cette nouvelle demande de livraison
	 * @param dedel 
	 */
	public void setDemandeDeLivraison(DemandeDeLivraison dedel){		
		this.dedel = dedel;			
		dedel.addObserver(this);		
	}
	/**
	 * Affecte l'ensemble de tournees à la valeur edt
	 * Observe ce nouvel ensemble de tournee
	 * @param edt 
	 */
	public void setEnsembleDeTournees(EnsembleDeTournees edt)
	{
		this.edt = edt;		
		edt.addObserver(this);	
	}

}


