package vue;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import controleur.Controleur;
/**
 * @author Arthur BELLEMIN et Thibault REMY, architecture initiale inspirée de l'application PlaCo
 */
public class EcouteurDeClavier extends KeyAdapter {
	/**
	 * controleur associé à l'écouteur
	 */
	private Controleur controleur;

	/**
	 * Crée un écouteur de clavier
	 * @param controleur associé à l'écouteur
	 */
	public EcouteurDeClavier(Controleur controleur){
		this.controleur = controleur;		
	}

	/**
	 * override la méthode keyPressed et permet de dispatcher les diverses actions clavier
	 * afin de signaler au controleur quel évènement clavier a été déclenché dans la vue 
	 * @param e l'évenement clavier e
	 */
	@Override
	public void keyPressed(KeyEvent e) {		
		int caractereSaisi = e.getKeyCode();	
		
		switch(caractereSaisi){

		//flecheDuHaut
		case 38:				
			controleur.flecheHaut();
			break;

		//flecheDuBas	
		case 40:				
			controleur.flecheBas();
			break;

		//retourChariot	
		case 10:
			controleur.finModif();
			break;	

		//ctrl z	
		case KeyEvent.VK_Z :
			if(e.getModifiers() == KeyEvent.CTRL_MASK) {				
				controleur.undo();
			}
			break;

		//ctrl y
		case KeyEvent.VK_Y:
			if(e.getModifiers() == KeyEvent.CTRL_MASK) {				
				controleur.redo();
			}
			break;			

		default:

		}
	}

}
