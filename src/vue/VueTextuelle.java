package vue;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Entrepot;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Tournee;

/**
 * @author Arthur BELLEMIN et Thibault REMY, architecture initiale inspirée de l'application PlaCo
 *
 */
public class VueTextuelle extends JLabel implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * texte à afficher dans la vue textuelle
	 */
	private String texte;
	/**
	 * message de desequilibre
	 */
	private String desequilibre;
	/**
	 * plan de la vue textuelle
	 */
	private Plan plan;
	/**
	 * demande de livraison de la vue textuelle
	 */
	private DemandeDeLivraison demandeDeLivraison;
	/**
	 * ensemble de tournée de la vue textuelle
	 */
	private EnsembleDeTournees ensembleDeTournees;	
	/**
	 * booleen de controle de l'affichage de l'entrepot
	 * si true affichera l'entrepot sinon non
	 */
	private boolean afficheEntrepot;
	/**
	 * booleen de controle de l'affichage des livraisons
	 * si true affichera les livraisons sinon non
	 */
	private boolean afficheLivraisons;
	/**
	 * booleen de controle de l'affichage des tournées
	 * si true affichera les tournées sinon non
	 */
	private boolean afficheTournees;
	/**
	 * booleen de controle : permet de savoir si tout est selectionné
	 */
	private boolean isAllSelected;
	/**
	 * listes des couleurs par défaut pour l'affichage des tournées
	 */
	private final Color[] LISTE_COULEUR_DEFAUT={Color.GREEN, Color.PINK, Color.MAGENTA, Color.ORANGE, Color.GRAY, Color.RED, Color.BLUE, Color.CYAN, Color.DARK_GRAY, Color.LIGHT_GRAY};

	/**
	 * Cree une vue textuelle de plan, demande de livraison et d'ensemble de tournées
	 *  dans fenetre
	 * @param plan
	 * @param demandeDeLivraison
	 * @param ensembleDeTournees
	 * @param fenetre
	 */
	public VueTextuelle(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees ensembleDeTournees, Fenetre fenetre){
		super();

		Border customColorBorder = BorderFactory.createLineBorder(new Color(55,186,214)); 
		TitledBorder borderDetails = BorderFactory.createTitledBorder(customColorBorder,"Détails");
		borderDetails.setTitleJustification(TitledBorder.CENTER);		
		setBorder(borderDetails);		
		this.setVerticalTextPosition(TOP);
		this.setVerticalAlignment(TOP);
		fenetre.getContentPane().add(this);	

		afficheEntrepot = true;
		afficheLivraisons = true;
		afficheTournees = true;

		this.desequilibre = "";

		plan.addObserver(this); // this observe plan
		demandeDeLivraison.addObserver(this); //this observes dedel		
		ensembleDeTournees.addObserver(this);
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		this.ensembleDeTournees = ensembleDeTournees;
		this.isAllSelected = true;
	}
	/**
	 * Retourne le plan de la vue textuelle
	 * @return plan
	 */
	public Plan getPlan() {
		return plan;
	}
	/**
	 * Affecte la valeur du plan de la vue textuelle à plan et l'observe
	 * @param plan
	 */
	public void setPlan(Plan plan) {
		this.plan = plan;
		plan.addObserver(this);
	}
	/**
	 * Retourne la demande de livraison de la vue textuelle
	 * @return demandeDeLivraison
	 */
	public DemandeDeLivraison getDemandeDeLivraison() {
		return demandeDeLivraison;
	}
	/**
	 * Affecte la valeur de la demande de livraison de la vue textuelle à demandeDeLivraison
	 * et l'observe
	 * @param demandeDeLivraison
	 */
	public void setDemandeDeLivraison(DemandeDeLivraison demandeDeLivraison) {
		this.demandeDeLivraison = demandeDeLivraison;
		demandeDeLivraison.addObserver(this);
	}
	/**
	 * Retourne l'ensemble des tournees de la vue textuelle
	 * @return ensembleDeTournees
	 */
	public EnsembleDeTournees getEnsembleDeTournees() {
		return ensembleDeTournees;
	}
	/**
	 * Affecte la valeur d'ensemble de tournees de la vue textuelle à ensembleDeTournees
	 * et l'observe
	 * @param ensembleDeTournees
	 */
	public void setEnsembleDeTournees(EnsembleDeTournees ensembleDeTournees) {
		this.ensembleDeTournees = ensembleDeTournees;
		ensembleDeTournees.addObserver(this);
	}
	/**
	 * Autorise ou non l'affichage du message de desequilibre dans la vue textuelle
	 * Si le parametre vaut true l'autorise sinon non
	 * @param isEnDesequilibre
	 */
	public void gererDesequilibre(boolean isEnDesequilibre)
	{
		if(isEnDesequilibre)
			this.desequilibre = "<h3> Attention il y a des tournées en déséquilibre </h3>";
		else
			this.desequilibre = "";
	}

	/**
	 * Methode appelee par les objets observes par this a chaque fois qu'ils ont ete modifies
	 */
	@Override
	public void update(Observable o, Object arg) {	
		affiche();	
	}
	/**
	 * Methode appelee a chaque fois que la vue textuelle doit etre affichée
	 * N'affiche que les détails des elements non nuls (troncons, tournees, points de livraison et entrepot)
	 * Prend en compte la notion de sélection d'un élément	 
	 */
	public void affiche(){

		texte = "<html>";

		texte += desequilibre ;
		Entrepot entrepot = demandeDeLivraison.getEntrepot();
		if(entrepot != null && afficheEntrepot) {
			texte += "<div style=\"color: blue;\">";
			texte += "<h2> Entrepot </h2>";
			texte += entrepot.affiche();
			texte += "</div>";
		}

		ArrayList<PointDeLivraison> pointsDeLivraison = demandeDeLivraison.getPointsDeLivraison();
		ArrayList<PointDeLivraison> pointsDeLivraisonTri = new ArrayList<>(pointsDeLivraison);
		if(pointsDeLivraison != null && pointsDeLivraison.size()>0 && afficheLivraisons) {	
			DemandeDeLivraison.trierPointsDeLivraisonParHeureDePassage(pointsDeLivraisonTri);		
			texte += "<div style=\"color: red;\"> <h2> Points De Livraisons </h2> <ul>";
			for(int i=0; i<pointsDeLivraisonTri.size(); i++) {
				if(pointsDeLivraisonTri.get(i).isEstSelectionne())
					texte += "<li>" +  pointsDeLivraisonTri.get(i).affiche(demandeDeLivraison) + "</li>";				
			}
			texte += "</ul> </div>";
		}

		ArrayList<Tournee> tournees = ensembleDeTournees.getListeTournee();
		if(tournees != null && tournees.size()>0 && afficheTournees) {			
			texte += "<div> <h2> Tournees Calculees </h2> <ul>";
			for(int i=0; i<tournees.size(); i++) {
				String color = "black";
				if(i<LISTE_COULEUR_DEFAUT.length)
					color = convertirCouleurEnString(LISTE_COULEUR_DEFAUT[i]);

				if(tournees.get(i).isEstSelectionne())
					texte += "<li style=\"color:"+ color +";\">" + tournees.get(i).affiche(demandeDeLivraison) + "</li>" ;
			}			
		}

		texte = texte+"</html>";
		setText(texte);		
	}

	/**
	 * Retourne la valeur du booleen isAllSelected
	 * @return isAllSelected 
	 */
	public boolean isAllSelected() {
		return isAllSelected;
	}
	/**
	 * Affecte la valeur du booleen isAllSelected à la valeur du parametre
	 * @param isAllSelected 
	 */
	public void setAllSelected(boolean isAllSelected) {
		this.isAllSelected = isAllSelected;
	}
	/**
	 * Retourne la valeur du booleen afficheEntrepot
	 * @return afficheEntrepot 
	 */
	public boolean isAfficheEntrepot() {
		return afficheEntrepot;
	}
	/**
	 * Affecte la valeur du booleen afficheEntrepot à la valeur du parametre
	 * @param afficheEntrepot 
	 */
	public void setAfficheEntrepot(boolean afficheEntrepot) {
		this.afficheEntrepot = afficheEntrepot;
	}
	/**
	 * Retourne la valeur du booleen afficheLivraisons
	 * @return afficheLivraisons 
	 */
	public boolean isAfficheLivraisons() {
		return afficheLivraisons;
	}
	/**
	 * Affecte la valeur du booleen afficheLivraisons à la valeur du parametre
	 * @param afficheLivraisons 
	 */
	public void setAfficheLivraisons(boolean afficheLivraisons) {
		this.afficheLivraisons = afficheLivraisons;
	}
	/**
	 * Retourne la valeur du booleen afficheTournees
	 * @return afficheTournees 
	 */
	public boolean isAfficheTournees() {
		return afficheTournees;
	}
	/**
	 * Affecte la valeur du booleen afficheTournees à la valeur du parametre
	 * @param afficheTournees 
	 */
	public void setAfficheTournees(boolean afficheTournees) {
		this.afficheTournees = afficheTournees;
	}
	/**
	 * Convertit la couleur c passée en paramètre en une string associée décrivant 
	 * la couleur
	 * @param c 
	 * @return la string couleur
	 */
	public String convertirCouleurEnString(Color c)
	{
		String couleur = "";
		if (Color.GREEN.equals(c)) {
			couleur = "#32CD32";
		}
		if (Color.PINK.equals(c)) {
			couleur = "#f9a6e9";
		} 
		if (Color.MAGENTA.equals(c)) {
			couleur = "#FF00FF";
		} 
		if (Color.ORANGE.equals(c)) {
			couleur = "orange";
		} 
		if (Color.GRAY.equals(c)) {
			couleur = "gray";
		} 
		if (Color.RED.equals(c)) {
			couleur = "red";
		}
		if (Color.BLUE.equals(c)) {
			couleur = "blue";
		}
		if (Color.CYAN.equals(c)) {
			couleur = "#00FFFF";
		}
		if (Color.DARK_GRAY.equals(c)) {
			couleur = "#5a5958";
		}
		if (Color.LIGHT_GRAY.equals(c)) {
			couleur = "#aeaaa8";
		} 		
		return couleur;
	}

}
