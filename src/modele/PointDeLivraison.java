package modele;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class PointDeLivraison extends Noeud {

	/**
	 * temps en secondes nécessaire sur place pour réaliser la livraison
	 */
	private int duree;

	/**
	 * date à laquelle le livreur arrive sur le point de livraison
	 */
	private Date heurePassage;

	/**
	 * boolean permettant à la vue de savoir si le PointDeLivraison est
	 * sélectionné
	 */
	private boolean estSelectionne;

	/**
	 * 
	 * @return estSelectionne l'état courant du PointDeLivraison
	 */
	public boolean isEstSelectionne() {
		return estSelectionne;
	}

	/**
	 * 
	 * @param estSelectionne l'état courant du PointDeLivraison
	 */
	public void setEstSelectionne(boolean estSelectionne) {
		this.estSelectionne = estSelectionne;
		setChanged();
		notifyObservers();
	}

	/**
	 * Constructeur utilisé uniquement pour des tests
	 * 
	 * @param unId         l'id
	 * @param uneLatitude  la latitude du point de livraison
	 * @param uneLongitude la longitude du point de livraison
	 * @param troncons     la liste des tronçons qui partents de ce point de
	 *                     livraison
	 * @param uneDuree     le temps en secondes nécessaire sur place pour
	 *                     réaliser la livraison
	 */
	public PointDeLivraison(Long unId, double uneLatitude, double uneLongitude,
							ArrayList<Troncon> troncons,
			int uneDuree) {
		super(unId, uneLatitude, uneLongitude, troncons);
		this.setDuree(uneDuree);
		this.heurePassage = null;
		this.estSelectionne = true;
	}

	/**
	 * @param noeud le noeud auquel à lieu la demande de livraison
	 * @param duree le temps en secondes nécessaire sur place pour réaliser la
	 *              livraison
	 */
	public PointDeLivraison(Noeud noeud, int duree) {
		super(noeud.getId(), noeud.getLatitude(), noeud.getLongitude(),
			  noeud.getTronconsPartants());
		this.duree = duree;
		this.estSelectionne = true;
	}

	/**
	 * @return temps en secondes nécessaire sur place pour réaliser la
	 * 		   livraison
	 */
	public int getDuree() {
		return duree;
	}

	/**
	 * @param duree temps en secondes nécessaire sur place pour réaliser la
	 *              livraison
	 */
	public void setDuree(int duree) {
		this.duree = duree;
	}

	/**
	 * @return date à laquelle le livreur arrive sur le point de livraison
	 */
	public Date getHeurePassage() {
		return heurePassage;
	}

	/**
	 * @param heurePassage date à laquelle le livreur arrive sur le point de
	 *                     livraison
	 */
	public void setHeurePassage(Date heurePassage) {
		this.heurePassage = heurePassage;
	}

	/**
	 * @return date à laquelle le livreur repart sur le point de livraison
	 */
	public Date getHeureFin() {
		return new Date(heurePassage.getTime() + duree * 1000);

	}

	/**
	 * @return description concise d'un tronçon pour un affichage utilisateur
	 */
	@SuppressWarnings("deprecation")
	public String affiche(DemandeDeLivraison dedel) {
		String heure = "heure de passage à définir";
		if (heurePassage != null)
			heure = "passage à " + heurePassage.getHours() + "h" + ":"
					+ heurePassage.getMinutes() + "min";
		return "    Point de Livraison n°"
			   + (dedel.getIndexPointDeLivraison(this) + 1) + " [" + heure
			   + ", " + duree + " sec" + "]";
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	public String toString() {
		String res = "PointDeLivraison{";
		res += "id = " + this.getId();
		res += ", estSelectionne = " + estSelectionne;
		res += "latitude = " + this.getLatitude();
		res += ", longitude = " + this.getLongitude();
		res += ", duree = " + this.getDuree();
		res += ", heureDePassage = " + this.getHeurePassage();
		res += "}";

		return res;
	}
}