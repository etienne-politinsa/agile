package modele;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.Collections;
/**
 * @author Florian et Loic
 *
 */
import java.util.Observable;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class DemandeDeLivraison extends Observable {
	/**
	 * vitesse du livreur en km/h (15km/h)
	 */
	public static final double VITESSE_LIVREUR_M_PAR_S = 15.*1000./3600.; 

	/**
	 * entrepot
	 */
	private Entrepot entrepot;

	/**
	 * liste des points de livraisons
	 */
	private ArrayList<PointDeLivraison> pointsDeLivraison;

	/**
	 * nombre de livreurs
	 */
	private int nombreLivreurs;

	/**
	 * 
	 * @param unEntrepot l'entrepot
	 * @param desPointsDeLivraison liste des points de livraisons
	 * @param unNombreDeLivreurs
	 */
	public DemandeDeLivraison(Entrepot unEntrepot,
			ArrayList<PointDeLivraison> desPointsDeLivraison, int unNombreDeLivreurs) {
		this.entrepot = unEntrepot;
		this.pointsDeLivraison = desPointsDeLivraison;
		this.nombreLivreurs = unNombreDeLivreurs;
	}

	/**
	 * Constructeur avec le nombre de livreur passé à 1 par défaut
	 * @param unEntrepot l'entrepot
	 * @param desPointsDeLivraison liste des points de livraisons
	 */
	public DemandeDeLivraison(Entrepot unEntrepot,
			ArrayList<PointDeLivraison> desPointsDeLivraison) {
		this.entrepot = unEntrepot;
		this.pointsDeLivraison = desPointsDeLivraison;
		this.nombreLivreurs = 1;
	}

	/**
	 * Constructeur par défaut
	 */
	public DemandeDeLivraison() {
		this.nombreLivreurs = 1;
		this.entrepot = null;
		this.pointsDeLivraison = new ArrayList<PointDeLivraison>();		
	}

	/**
	 * 
	 * @return l'entrepot
	 */
	public Entrepot getEntrepot() {
		return entrepot;
	}

	/**
	 * 
	 * @param entrepot l'entrepot
	 */
	public void setEntrepot(Entrepot entrepot) {
		this.entrepot = entrepot;
		setChanged();
		notifyObservers();			
	}

	/**
	 * 
	 * @return la liste des points de livraisons
	 */
	public ArrayList<PointDeLivraison> getPointsDeLivraison() {
		return pointsDeLivraison;
	}

	/**
	 * 
	 * @param pointsDeLivraison la liste des points de livraisons
	 */
	public void setPointsDeLivraison(ArrayList<PointDeLivraison> pointsDeLivraison) {
		this.pointsDeLivraison = pointsDeLivraison;
		setChanged();
		notifyObservers();		
	}

	/**
	 * 
	 * @param index index de la Livraison à sélectionner
	 * @param sel true s'il faut sélectionner le point de livraison, false sinon
	 */
	public void setEstSelectionne(int index, boolean sel) {
		pointsDeLivraison.get(index).setEstSelectionne(sel);
		setChanged();
		notifyObservers();
	}

	/**
	 * 
	 * @param sel true s'il faut sélectionner l'entrepot, false sinon
	 */
	public void setEntrepotSelectionne(boolean sel) {
		entrepot.setEstSelectionne(sel);
		setChanged();
		notifyObservers();
	}

	/**
	 * 
	 * @return le nombre de livreurs
	 */
	public int getNombreLivreurs() {
		return nombreLivreurs;
	}

	/**
	 * 
	 * @param nombreLivreurs le nombre de livreurs
	 */
	public void setNombreLivreurs(int nombreLivreurs) {
		this.nombreLivreurs = nombreLivreurs;
		setChanged();
		notifyObservers();	
	}

	/**
	 * 
	 * @param pdl le point de livraison à supprimer
	 */
	public void remove(PointDeLivraison pdl) {
		if(pointsDeLivraison.remove(pdl))
			return;
		for(PointDeLivraison p : pointsDeLivraison) {
			if(p.getId().equals(pdl.getId())) {
				pointsDeLivraison.remove(p);
				return;
			}
		}
	}
	
	/**
	 * 
	 * @param pdl point de livraison 
	 * @return la position du point de livraison dans la demande de livraison
	 */
	public int getIndexPointDeLivraison(PointDeLivraison pdl) {
		for(int i=0;i<pointsDeLivraison.size();i++ ) {
			if(pointsDeLivraison.get(i).getId().equals(pdl.getId()))
				return i;
		}
		return -1;
	}

	/**
	 * 
	 * @param longi 
	 * @param lati
	 * @return le point de livraison le plus proche du point (longi,lati)
	 */
	public PointDeLivraison getPointDeLivraisonLePlusProche(double longi, double lati) {
		PointDeLivraison pdl = null;
		double distanceMin = Double.MAX_VALUE;
		for	(PointDeLivraison tempo : pointsDeLivraison) {
			double distanceTempo = tempo.calculDistanceEuclidienne(longi, lati);
			if(distanceTempo<distanceMin) {
				pdl = tempo;
				distanceMin = distanceTempo;
			}
		}
		return pdl;
	}

	/**
	 * 
	 * @param noeud
	 * @return p le point de livraison correspondant au noeud
	 */
	public PointDeLivraison getPointDeLivraisonParNoeud(Noeud noeud) {
		for(PointDeLivraison p :pointsDeLivraison) {
			if(p.getId().equals(noeud.getId() ))
				return p;
		}
		return null;
	}
	
	/**
	 * Trie la liste des points de livraison par heure de passage
	 * @param liste de point de livraison
	 */
	public static void trierPointsDeLivraisonParHeureDePassage(ArrayList<PointDeLivraison> liste) {
		Collections.sort(liste, (pdl1,pdl2)->comparateurHeurePassage(pdl1,pdl2));
	}


	/**
	 * @param pdl1
	 * @param pdl2
	 * @return the value 0 si les deux points de livraison ont la même heure de livraison,
	 * une valeur inférieure à 0 si pdl1 a une heure de passage avant pdl2,
	 * une valeur suprérieure à 0 sinon
	 */
	public static int comparateurHeurePassage(PointDeLivraison pdl1, PointDeLivraison pdl2) {
		if(pdl1.getHeurePassage() == null || pdl2.getHeurePassage() == null)
			return 1;
		return pdl1.getHeurePassage().compareTo(pdl2.getHeurePassage());
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	public String toString() {
		String res= "DemandeDeLivraison{\n";
		res += "  nombreLivreurs = " + this.nombreLivreurs + ",\n";
		res += "  entrepot = " + this.entrepot+",\n";
		res += "  pointsDeLivraison = [";
		for(PointDeLivraison p : this.pointsDeLivraison)
			res+= "\n    "+p;
		res += "\n  ]\n";
		res += "}";
		return res;
	}

}
