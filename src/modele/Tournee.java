package modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Observable;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class Tournee extends Observable {
	/**
	 * la longueur du trajet en mètres
	 */
	private double longueur;

	/**
	 * liste des points de livraison ordonée par ordre de passage et de leur
	 * trajet pour s'y rendre. Le dernier point de livraison est donc null car
	 * cela correspond au retour à l'entrepot
	 */
	private ArrayList<Pair<PointDeLivraison, Trajet>> listePointTrajet;

	/**
	 * le numéro du livreur qui effectue cette tournée
	 */
	private int livreur;

	/**
	 * 
	 * @param livreur
	 */
	public void setLivreur(int livreur) {
		this.livreur = livreur;
	}

	/**
	 * 
	 * @param longueur
	 */
	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	/**
	 * 
	 * @param listePointTrajet
	 */
	public void setListePointTrajet(ArrayList<Pair<PointDeLivraison,
									Trajet>> listePointTrajet) {
		this.listePointTrajet = listePointTrajet;
	}

	/**
	 * boolean permettant à la vue de savoir si la tournée est sélectionnée
	 */
	private boolean estSelectionne;

	/**
	 * @param longueur         la longueur du trajet en mètres
	 * @param livreur          le numéro du livreur qui effectue cette tournée
	 * @param listePointTrajet liste des points de livraison ordonée par ordre
	 *                         de passage et de leur trajet pour s'y rendre. Le
	 *                         dernier point de livraison est donc null car
	 *                         cela correspond au retour à l'entrepot
	 */
	public Tournee(double longueur, int livreur, ArrayList<Pair<PointDeLivraison,
				   Trajet>> listePointTrajet) {
		this.longueur = longueur;
		this.livreur = livreur;
		this.listePointTrajet = listePointTrajet;
		this.estSelectionne = true;
	}

	/**
	 * Constructeur utile pour les tests
	 * 
	 * @param livreur    le numéro du livreur qui effectue cette tournée
	 * @param listePoint liste des points de livraison ordonée par ordre de
	 * 					 passage
	 * @param p          le plan support de la demande de livraison
	 * @param e          l'entrepot de la demande de livraison
	 */
	public Tournee(int livreur, ArrayList<PointDeLivraison> listePoints, Plan p,
				   Entrepot e) {
		this.livreur = livreur;
		this.estSelectionne = true;
		listePointTrajet = new ArrayList<Pair<PointDeLivraison, Trajet>>();
		for (int i = 0; i < listePoints.size(); i++) {
			if (i == 0) {
				Trajet t = (p.calculerPlusCourtChemin(e,
								new ArrayList<Noeud>(Arrays.asList(listePoints
										.get(i)))))
						   .get(0);
				listePointTrajet.add(new Pair<PointDeLivraison,
									 Trajet>(listePoints.get(i), t));
			} else {
				Trajet t = (p.calculerPlusCourtChemin(listePoints.get(i - 1),
								new ArrayList<Noeud>(Arrays.asList(listePoints
										.get(i)))))
						   .get(0);
				listePointTrajet.add(new Pair<PointDeLivraison,
									 Trajet>(listePoints.get(i), t));
			}
		}
		if (listePoints.size() > 0) {
			Trajet t = (p.calculerPlusCourtChemin(listePoints
							.get(listePoints.size() - 1),
								new ArrayList<Noeud>(Arrays.asList(e))))
					   .get(0);
			listePointTrajet.add(new Pair<PointDeLivraison, Trajet>(null, t));
		}
		calculerHorairesDePassageAPartirDe(0, e);
	}

	/**
	 * avancer un point de livraison dans l'ordre de passage de la tournée
	 * 
	 * @param pdl  le point de livraison à avancer
	 * @param plan le plan support de la demande de livraison
	 * @param e    l'entrepot de la demande de livraison
	 */
	public void avancer(PointDeLivraison pdl, Plan plan, Entrepot e)
			throws Exception {
		int index = this.getIndexOfPointDeLivraison(pdl);
		if (index < 1) {
			throw new Exception("Il est impossible d'avancer"
					+ "le premier point de livraison de la tournée");
		}
		this.echangePointDeLivraisonAvecPrecedent(index, plan, e);
	}

	/**
	 * reculer un point de livraison dans l'ordre de passage de la tournée
	 * 
	 * @param pdl  le point de livraison à reculer
	 * @param plan le plan support de la demande de livraison
	 * @param e    l'entrepot de la demande de livraison
	 */
	public void reculer(PointDeLivraison pdl, Plan plan, Entrepot e)
			throws Exception {
		int index = this.getIndexOfPointDeLivraison(pdl);
		if (index > this.listePointTrajet.size() - 3) {
			throw new Exception("Il est impossible de reculer le dernier"
					+ "point de livraison de la tournée");
		}
		this.echangePointDeLivraisonAvecPrecedent(index + 1, plan, e);
	}

	/**
	 * echange le point de livraison avec le précédent dans l'ordre de
	 * passage de la tournée
	 * 
	 * @param index l'index du point de livraison dans la tournee
	 * @param plan  le plan support de la demande de livraison
	 * @param e     l'entrepot de la demande de livraison
	 */
	private void echangePointDeLivraisonAvecPrecedent(int index, Plan plan,
												 Entrepot e) throws Exception {
		// récupération des paires intéressantes
		Pair<PointDeLivraison, Trajet> debut = listePointTrajet.get(index - 1);
		Pair<PointDeLivraison, Trajet> milieu = listePointTrajet.get(index);
		Pair<PointDeLivraison, Trajet> fin = listePointTrajet.get(index + 1);

		// recalcul des trajets
		Trajet trajetDebut = (plan.calculerPlusCourtChemin(debut.getSecond()
										.getNoeudDepart(),
									new ArrayList<Noeud>(Arrays.asList(milieu
										.getSecond().getNoeudArrivee()))))
							  .get(0);
		Trajet trajetMilieu = (plan.calculerPlusCourtChemin(milieu.getSecond()
										.getNoeudArrivee(),
									new ArrayList<Noeud>(Arrays.asList(milieu
									   .getSecond().getNoeudDepart()))))
							  .get(0);
		Trajet trajetFin = (plan.calculerPlusCourtChemin(milieu.getSecond()
										.getNoeudDepart(),
									new ArrayList<Noeud>(Arrays.asList(fin
										.getSecond().getNoeudArrivee()))))
							.get(0);

		// échange des point de livraison et mise à jour des trajets
		PointDeLivraison pdl = milieu.getFirst();
		milieu.setFirst(debut.getFirst());
		debut.setFirst(pdl);
		debut.setSecond(trajetDebut);
		milieu.setSecond(trajetMilieu);
		fin.setSecond(trajetFin);

		// réaffectation des horaires de passage
		calculerHorairesDePassageAPartirDe(index - 1, e);
	}
	
	/**
	 * Calcule et met à jour les horaires de passage des points de livraisons
	 * @param debut l'index du premier point de livraison à recalculer
	 * @param e 	l'entrepot duquel part la tournée
	 */
	private void calculerHorairesDePassageAPartirDe(int debut, Entrepot e) {
		Long temps;
		if (debut > 0)
			temps = listePointTrajet.get(debut - 1).getFirst().getHeureFin()
					.getTime();
		else
			temps = e.getHeureDepart().getTime();

		for (int i = debut; i < this.listePointTrajet.size() - 1; i++) {
			temps += this.listePointTrajet.get(i).getSecond()
					 .getTempsParcours();
			this.listePointTrajet.get(i).getFirst()
				.setHeurePassage(new Date(temps));
			temps = this.listePointTrajet.get(i).getFirst().getHeureFin()
					.getTime();
		}
	}

	/**
	 * @param longi la longitude angulaire depuis laquelle on calcule la
	 * 				distance
	 * @param lati  la latitude angulaire depuis laquelle on calcule la
	 * 				distance
	 * @return la distance angulaire à vol d'oiseau entre les coordonnées
	 * 		   données en paramètre et le milieu du tronçon de la tounee
	 * 		   le plus proche des coordonnées données en paramètre
	 */
	public double calculDistanceEuclidienne(double longi, double lati) {
		double distanceMin = Double.MAX_VALUE;
		for (Pair<PointDeLivraison, Trajet> p : listePointTrajet) {
			double distanceTempo = p.getSecond()
								   .calculDistanceEuclidienne(longi, lati);
			if (distanceTempo < distanceMin) {
				distanceMin = distanceTempo;
			}
		}

		return distanceMin;
	}

	/**
	 * @param noeud le noeud auquel il faut ajouter une demande de livraison
	 * @param duree la duree en secondes nécessaire su place pour la livraison
	 * @param plan  le plan support de la demande de livraison
	 * @return ajout d'un point de livraison en dernière position dans la
	 * 		   tournée
	 */
	public PointDeLivraison ajouterPointDeLivraison(Noeud noeud, int duree,
													Plan plan) {
		// récupération d'informations
		Noeud nArrivee = listePointTrajet.get(listePointTrajet.size() - 1)
							.getSecond().getNoeudArrivee();
		Noeud nDepart = listePointTrajet.get(listePointTrajet.size() - 2)
							.getSecond().getNoeudArrivee();
		Long tempsPassage = listePointTrajet.get(listePointTrajet.size() - 2)
							.getFirst().getHeureFin().getTime();

		// calcul des trajets
		Trajet t1;
		Trajet t2;
		try {
			t1 = (plan.calculerPlusCourtChemin(nDepart,
											   new ArrayList<Noeud>(
													 Arrays.asList(noeud))))
			      .get(0);
			t2 = (plan.calculerPlusCourtChemin(noeud,
											   new ArrayList<Noeud>(
											         Arrays.asList(nArrivee))))
			      .get(0);
		} catch (Exception e) {
			return null;
		}

		// suppression du retour à l'entrepot
		longueur -= listePointTrajet.get(listePointTrajet.size() - 1)
					.getSecond().getLongueur();
		listePointTrajet.remove(listePointTrajet.size() - 1);

		// création du point de livraison
		PointDeLivraison pdl = new PointDeLivraison(noeud, duree);
		pdl.setHeurePassage(new Date(tempsPassage + t1.getTempsParcours()));
		Pair<PointDeLivraison, Trajet> pair1 =
				new Pair<PointDeLivraison,Trajet>(pdl, t1);
		listePointTrajet.add(pair1);
		longueur += t1.getLongueur();

		// ajout du retour à l'entrepot
		Pair<PointDeLivraison, Trajet> pair2 =
				new Pair<PointDeLivraison, Trajet>(null, t2);
		listePointTrajet.add(pair2);
		longueur += t2.getLongueur();
		return pdl;
	}

	/**
	 * @param pdl  le point de livraison à supprimer
	 * @param plan le plan support de la demande de livraison
	 */
	public void supprimerPointDeLivraison(PointDeLivraison pdl, Plan plan) {
		int i;
		int j = 0;
		Noeud nDepart = null;
		Noeud nArrivee;
		Long tempsParcours = new Long(0);

		// recherche et suppression du point de livraison
		for (i = 0; i < listePointTrajet.size(); i++) {
			Pair<PointDeLivraison, Trajet> p = listePointTrajet.get(i);
			if (p.getFirst() != null
				&& p.getFirst().getId().equals(pdl.getId())) {
				nDepart = p.getSecond().getNoeudDepart();
				longueur -= p.getSecond().getLongueur();
				tempsParcours = p.getFirst().getHeurePassage().getTime()
								- p.getSecond().getTempsParcours();
				j = i;
			}
		}
		if (nDepart == null)
			System.err.println("Erreur dans la suppression d'un point"
					+ "de livraison (classe Tournee : nDepart ==null");

		listePointTrajet.remove(listePointTrajet.get(j));

		// mise à jour de la tournée
		Pair<PointDeLivraison, Trajet> p = listePointTrajet.get(j);
		longueur -= p.getSecond().getLongueur();
		nArrivee = p.getSecond().getNoeudArrivee();
		Trajet t = (plan.calculerPlusCourtChemin(nDepart,
						new ArrayList<Noeud>(Arrays.asList(nArrivee))))
				    .get(0);
		p.setSecond(t);
		longueur += t.getLongueur();

		// mise à jour des horaires de passage
		while (p.getFirst() != null) {
			tempsParcours += p.getSecond().getTempsParcours();
			p.getFirst().setHeurePassage(new Date(tempsParcours));
			tempsParcours = p.getFirst().getHeureFin().getTime();
			p = listePointTrajet.get(++j);
		}
	}

	/**
	 * 
	 * @return estSelectionne l'état courant de la tournée
	 */
	public boolean isEstSelectionne() {
		return estSelectionne;
	}

	/**
	 * 
	 * @param estSelectionne l'état courant de la tournée
	 */
	public void setEstSelectionne(boolean estSelectionne) {
		this.estSelectionne = estSelectionne;
		setChanged();
		notifyObservers();
	}

	/**
	 * @return la longueur du trajet en mètres
	 */
	public double getLongueur() {
		return longueur;
	}

	/**
	 * @return liste des points de livraison ordonée par ordre de passage et de leur
	 *         trajet pour s'y rendre. Le dernier point de livraison est donc null
	 *         car cela correspond au retour à l'entrepot
	 */
	public ArrayList<Pair<PointDeLivraison, Trajet>> getListePointTrajet() {
		return listePointTrajet;
	}

	/**
	 * @return le numéro du livreur qui effectue cette tournée
	 */
	public int getLivreur() {
		return livreur;
	}

	/**
	 * @param pdl le point de livraison rechercher
	 * @return 	  l'index du point de livraison dans listePointTrajet s'il est présent,
	 *            -1 sinon
	 */
	public int getIndexOfPointDeLivraison(PointDeLivraison pdl) {
		for (int i = 0; i < listePointTrajet.size() - 1; i++) {
			Pair<PointDeLivraison, Trajet> pair = listePointTrajet.get(i);
			if (pair.getFirst().getId().equals(pdl.getId()))
				return i;
		}
		return -1;
	}

	/**
	 * @return description concise d'une tournee pour un affichage utilisateur
	 */
	public String affiche(DemandeDeLivraison dedel) {

		String texte = "  Tournee" + "" + " de longueur : " + (int) longueur
						+ " m, " + "effectuée par le livreur n°" + (livreur + 1) + "<ul>";
		for (Pair<PointDeLivraison, Trajet> p : listePointTrajet) {
			texte += "<li>";
			if (p.getFirst() != null)
				texte += (p.getFirst().affiche(dedel));

			texte += p.getSecond().affiche();
			texte += "</li>";
		}

		return texte + " </ul>  ]";
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	public String toString() {
		String texte = "Tournee{ longueur=" + longueur + " livreur="
					   + livreur + "\n";
		for (Pair<PointDeLivraison, Trajet> p : listePointTrajet)
			texte += "  " + p.getFirst() + " / " + p.getSecond() + "\r\n";
		texte += "}";
		return texte;
	}
}