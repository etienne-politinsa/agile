package modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.PriorityQueue;

/**
 * @author Edouard BARGE
 *
 */
public class Plan extends Observable {
	/**
	 * ensemble des noeuds du plan
	 */
	private HashMap<Long, Noeud> noeuds;

	/**
	 * ensemble des tronçons du plan
	 */
	private ArrayList<Troncon> troncons;

	/**
	 * largeur du plan
	 */
	int largeurplan;

	/**
	 * hauteur du plan
	 */
	int hauteurplan;

	/**
	 * latitude minimale du plan
	 */
	double latmin;

	/**
	 * latitude maximale du plan
	 */
	double latmax;

	/**
	 * longitude minimale du plan
	 */
	double longmin;

	/**
	 * longitude maximale du plan
	 */
	double longmax;

	public Plan() {
	}

	/**
	 * 
	 * @param largeurplan largeur du plan
	 * @param hauteurplan hauteur du plan
	 */
	public Plan(int largeurplan, int hauteurplan) {
		this.largeurplan = largeurplan;
		this.hauteurplan = hauteurplan;
	}

	/**
	 * 
	 * @return la largeur du plan
	 */
	public int getLargeurplan() {
		return largeurplan;
	}

	/**
	 * 
	 * @param largeurplan la largeur du plan
	 */
	public void setLargeurplan(int largeurplan) {
		this.largeurplan = largeurplan;
	}

	/**
	 * 
	 * @return la hauteur du plan
	 */
	public int getHauteurplan() {
		return hauteurplan;
	}

	/**
	 * 
	 * @param hauteurplan la hauteur du plan
	 */
	public void setHauteurplan(int hauteurplan) {
		this.hauteurplan = hauteurplan;
	}

	/**
	 * 
	 * @return la latitude minimale
	 */
	public double getLatmin() {
		return latmin;
	}

	/**
	 * 
	 * @param latmin la latitude minimale
	 */
	public void setLatmin(double latmin) {
		this.latmin = latmin;
	}

	/**
	 * 
	 * @return la latitude maximale
	 */
	public double getLatmax() {
		return latmax;
	}

	/**
	 * 
	 * @param latmax la latitude maximale
	 */
	public void setLatmax(double latmax) {
		this.latmax = latmax;
	}

	/**
	 * 
	 * @return la longitude minimale
	 */
	public double getLongmin() {
		return longmin;
	}

	/**
	 * 
	 * @param longmin la longitude minimale
	 */
	public void setLongmin(double longmin) {
		this.longmin = longmin;
	}

	/**
	 * 
	 * @return la longitude maximale
	 */
	public double getLongmax() {
		return longmax;
	}

	/**
	 * 
	 * @param longmax la longitude maximale
	 */
	public void setLongmax(double longmax) {
		this.longmax = longmax;
	}

	/**
	 * 
	 * @param unId l'id du Noeud recherché
	 * @return 	   le noeud correspondant à l'id
	 * @throws     Exception
	 */
	public Noeud rechercheNoeudParId(Long unId) throws Exception {
		return noeuds.get(unId);
	}

	/**
	 * 
	 * @param longi longitude
	 * @param lati  latitude
	 * @return 		le noeud le plus proche du point (longi,lati)
	 */
	public Noeud getNoeudLePlusProche(double longi, double lati) {
		Noeud n = null;
		double distanceMin = Double.MAX_VALUE;
		for (Map.Entry<Long, Noeud> mapentry : noeuds.entrySet()) {
			double distanceTempo = mapentry.getValue()
					.calculDistanceEuclidienne(longi, lati);
			if (distanceTempo < distanceMin) {
				n = mapentry.getValue();
				distanceMin = distanceTempo;
			}
		}
		return n;
	}

	/**
	 * 
	 * @param longi longitude
	 * @param lati  latitude
	 * @return      le troncon le plus proche du point (longi,lati)
	 */
	public Troncon getTronconLePlusProche(double longi, double lati) {
		Troncon t = null;
		double distanceMin = Double.MAX_VALUE;
		for (Troncon tempo : troncons) {
			double distanceTempo = tempo.calculDistanceEuclidienne(longi, lati);
			if (distanceTempo < distanceMin) {
				t = tempo;
				distanceMin = distanceTempo;
			}
		}
		return t;
	}

	/**
	 * 
	 * @param a une distance
	 * @param b une distance
	 * @return  -1 si a<b, 1 si a>b et 0 si a=b
	 */
	private int comparateurDeDistances(double a, double b) {
		if (a < b)
			return -1;
		else if (a > b)
			return 1;
		return 0;
	}

	/**
	 * 
	 * @param noeudDepart   un noeud de départ
	 * @param noeudsArrivee une liste de noeuds d'arrivée
	 * @return 				une liste de trajets allant du noeud de départ aux différents noeuds
	 *        				d'arrivées
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Trajet> calculerPlusCourtChemin(Noeud noeudDepart,
											ArrayList<Noeud> noeudsArrivee) {
		// Valeur de retour
		ArrayList<Trajet> ensembleDijsktraaNoeuds = new ArrayList<Trajet>();
		// Nombre de sommets
		int nombreSommets = noeuds.size();
		// Noeuds de 0 à n-1 suivant l'indice
		boolean sommetsNoirs[] = new boolean[nombreSommets]; 
		Arrays.fill(sommetsNoirs, false);
		// Tableau d remplit de distance d'infini
		double distance[] = new double[nombreSommets];
		Arrays.fill(distance, Double.MAX_VALUE);
		/*
		 * Tableau des predecesseurs remplit d'un int qui ne
		 * reference pas un sommet
		 */
		int predecesseur[] = new int[nombreSommets];
		Arrays.fill(predecesseur, nombreSommets + 1);
		HashMap<Integer, ArrayList<Troncon>> troncon =
			new HashMap<Integer, ArrayList<Troncon>>();
		// tableDeCorrespondanceIdSommet : on connait l'id on veut le sommet
		HashMap<Long, Integer> tableDeCorrespondanceIdSommet =
			new HashMap<Long, Integer>();
		 // tableDeCorrespondanceSommetId : on connait le sommet on veut l'id
		HashMap<Integer, Long> tableDeCorrespondanceSommetId =
			new HashMap<Integer, Long>();
		/*
		 * priority queue qui trie les valeurs en fonction
		 * des distances dans le tableau
		 */
		PriorityQueue<Integer> fileDePriorite =
			new PriorityQueue<Integer>(nombreSommets,
					(a, b) -> comparateurDeDistances(distance[a], distance[b]));
		
		int correspondance = 0;
		for (Map.Entry<Long, Noeud> entry : noeuds.entrySet()) {
			Long idTempo = entry.getKey(); // on construit les tableaux de correspondance
			Integer sommetTempo = new Integer(correspondance);
			tableDeCorrespondanceIdSommet.put(idTempo, sommetTempo);
			tableDeCorrespondanceSommetId.put(sommetTempo, idTempo);
			correspondance++;

		}
		Long idNoeudDepart = noeudDepart.getId();
		fileDePriorite.add(tableDeCorrespondanceIdSommet.get(idNoeudDepart));
		distance[tableDeCorrespondanceIdSommet.get(idNoeudDepart)] = 0;
		troncon.put(tableDeCorrespondanceIdSommet.get(idNoeudDepart),
													  new ArrayList<Troncon>());
		/*
		 * In fine, on obtient un table de hashage avec pour clé l'id
		 * du Noeud et pour valeur l'indice dans le graphe
		 */

		while (fileDePriorite.size() != 0) {
			// on extrait le sommet le plus proche grace � la file de prio
			Integer sommetCourant = fileDePriorite.poll();
			// on recupere la distance de ce noeud
			double distanceNoeud = distance[sommetCourant];
			
			if (!sommetsNoirs[sommetCourant]) { // si le noeud est noir on passe
				/*
				 * On définit le noeud courant comme étant noir = terminé
				 * à la fin de l'itération
				 */
				sommetsNoirs[sommetCourant] = true;

				try {

					for (Troncon t : rechercheNoeudParId(
							tableDeCorrespondanceSommetId.get(sommetCourant))
								.getTronconsPartants()) {
						/*
						 * On parcourt les troncons du noeud courant pour
						 * récupérer les noeuds destinations et les longueurs
						 */
						// On récupère destination
						Noeud voisin = t.getNoeudDestination();
						// On recupere son numéro de sommet
						Integer numeroSommetVoisin =
								tableDeCorrespondanceIdSommet
									.get(voisin.getId());
						/*
						 * On calcule la distance (longueur du troncon + distance
						 * du noeud courant (qui est également lenoeud origine)
						 */
						double distanceVoisin = distanceNoeud + t.getLongueur();
						/*
						 * Si la distance est plus petite on update et
						 * on push les valeurs
						 */
						if (distanceVoisin < distance[numeroSommetVoisin]) {
							distance[numeroSommetVoisin] = distanceVoisin;
							predecesseur[numeroSommetVoisin] = sommetCourant;
							fileDePriorite.add(numeroSommetVoisin);
							troncon.put(numeroSommetVoisin,
										(ArrayList<Troncon>) troncon
											.get(sommetCourant).clone());
							troncon.get(numeroSommetVoisin).add(t);

						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
		/*
		 * A la fin de la boucle, distance dispose des distances du sommet de départ à
		 * tous les autres sommets, et predecesseur permet de suivre le chemin depus le
		 * noeud origine vers le noeud souhaité
		 */

		for (int i = 0; i < noeudsArrivee.size(); i++) {
			/*
			 * on parcourt les noeudsArrivee, on recupere le sommet
			 * correspondant pour recuperer la liste de troncon du
			 * sommet, puis on crée le trajet
			 */
			Noeud noeudCourant = noeudsArrivee.get(i);
			Long idCourant = noeudCourant.getId();
			Integer sommetCorrespondant = tableDeCorrespondanceIdSommet
										  .get(idCourant);
			ArrayList<Troncon> listeTronconCourant = troncon
					.get(sommetCorrespondant);
			ensembleDijsktraaNoeuds.add(new Trajet(listeTronconCourant));
		}

		return ensembleDijsktraaNoeuds;
	}

	/**
	 * 
	 * @return la collection de noeuds
	 */
	public HashMap<Long, Noeud> getCollectionNoeuds() {
		return noeuds;
	}

	/**
	 * 
	 * @param noeuds la collection de noeuds
	 */
	public void setCollectionNoeuds(HashMap<Long, Noeud> noeuds) {
		this.noeuds = noeuds;
	}

	/**
	 * 
	 * @return la liste de tronçons
	 */
	public ArrayList<Troncon> getCollectionTroncons() {
		return troncons;
	}

	/**
	 * 
	 * @param troncons la liste de tronçons
	 */
	public void setCollectionTroncons(ArrayList<Troncon> troncons) {
		this.troncons = troncons;
		setChanged();
		notifyObservers();
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	@Override
	public String toString() {
		return "Plan [noeuds=" + noeuds + ", troncons=" + troncons + "]";
	}

}