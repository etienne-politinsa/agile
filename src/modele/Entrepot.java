package modele;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class Entrepot extends Noeud {
	/**
	 * heure de départ de la livraison 
	 */
	private Date heureDepart;

	/**
	 * boolean permettant à la vue de savoir si l'Entrepot est sélectionnée
	 */
	private boolean estSelectionne ;

	/**
	 * 
	 * @param id l'identifiant du noeud
	 * @param latitude latitude du noeud
	 * @param longitude longitude du noeud
	 * @param tronconsPartants liste de tronçons qui partent du noeud
	 * @param heureDepart heure de départ de la livraison
	 */
	public Entrepot(Long id, double latitude, double longitude, ArrayList<Troncon> tronconsPartants, Date heureDepart) {
		super(id, latitude, longitude, tronconsPartants);
		this.heureDepart = heureDepart;
		this.estSelectionne = true;
	}
	
	/**
	 * 
	 * @param noeud noeud correspondant à l'entrepot
	 * @param heureDepart heure de départ de la livraison
	 */
	public Entrepot(Noeud noeud, Date heureDepart) {
		super(noeud.getId(),noeud.getLatitude(),noeud.getLongitude(),noeud.getTronconsPartants());
		this.heureDepart = heureDepart;
		this.estSelectionne = true;
	}

	/**
	 * 
	 * @return estSelectionne l'état courant de l'Entrepot
	 */
	public boolean isEstSelectionne() {
		return estSelectionne;
	}
	
	/**
	 * 
	 * @param estSelectionne l'état courant de l'Entrepot
	 */
	public void setEstSelectionne(boolean estSelectionne) {
		this.estSelectionne = estSelectionne;
		setChanged();
		notifyObservers();
	}

	/**
	 * 
	 * @return heure de départ de la livraison 
	 */
	public Date getHeureDepart() {
		return heureDepart;
	}

	/**
	 * méthode d'affichage pour le débogage
	 */
	public String toString() {
		String res = "Entrepot{";
		res+= "id = "+ this.getId();
		res+= ", estSelectionne = " + estSelectionne;
		res+= ", latitude = " + this.getLatitude();
		res+= ", longitude = " + this.getLongitude();		
		res+= ", HeureDeDepart = " + heureDepart;
		res+= "}";
		return res;		
	}
	
	/**
	 * 
	 * @return description concise d'un entrepot pour un affichage utilisateur
	 */
	@SuppressWarnings("deprecation")
	public String affiche() {
		return "  Entrepot [départ à " + heureDepart.getHours() + "h" + ":"+heureDepart.getMinutes() + "min"+ "]";
	}

}
