package modele;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class Troncon {

	/**
	 * la longueur en mètre du tronçon
	 */
	private double longueur;

	/**
	 * le nom du tronçon
	 */
	private String nom;

	/**
	 * le noeud duquel part le tronçon
	 */
	private Noeud noeudOrigine;

	/**
	 * le noeud auquel arrive le tronçon
	 */
	private Noeud noeudDestination;

	/**
	 * @param uneLongueur    la longueur du tronçon en mètres
	 * @param unNom          le nom du tronçon
	 * @param uneOrigine     le noeud duquel part le tronçon
	 * @param uneDestination le noeud auquel arrive le tronçon
	 */
	public Troncon(double uneLongueur, String unNom, Noeud uneOrigine,
				   Noeud uneDestination) {

		this.longueur = uneLongueur;
		this.nom = unNom;
		this.noeudOrigine = uneOrigine;
		this.noeudDestination = uneDestination;

	}

	/**
	 * @return la longueur du tronçon en mètres
	 */
	public double getLongueur() {
		return longueur;
	}

	/**
	 * @param la longueur du tronçon en mètres
	 */
	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	/**
	 * @return le nom du tronçon
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom le nom du tronçon
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return le noeud duquel part le tronçon
	 */
	public Noeud getNoeudOrigine() {
		return noeudOrigine;
	}

	/**
	 * @param noeudOrigine le noeud duquel part le tronçon
	 */
	public void setNoeudOrigine(Noeud noeudOrigine) {
		this.noeudOrigine = noeudOrigine;
	}

	/**
	 * @return le noeud auquel arrive le tronçon
	 */
	public Noeud getNoeudDestination() {
		return noeudDestination;
	}

	/**
	 * @param noeudDestination le noeud auquel arrive le tronçon
	 */
	public void setNoeudDestination(Noeud noeudDestination) {
		this.noeudDestination = noeudDestination;
	}

	/**
	 * @param longi la longitude angulaire depuis laquelle on calcule
	 * 				la distance
	 * @param lati  la latitude angulaire depuis laquelle on calcule la distance
	 * @return la distance angulaire à vol d'oiseau entre les coordonnées
	 *         données en paramètre et le milieu du tronçon
	 */
	public double calculDistanceEuclidienne(double longi, double lati) {
		double latitude = (noeudOrigine.getLatitude()
						   + noeudDestination.getLatitude()) / 2.0;
		double longitude = (noeudOrigine.getLongitude()
							+ noeudDestination.getLongitude()) / 2.0;
		return Math.sqrt(Math.pow(lati - latitude, 2)
						 + Math.pow(longi - longitude, 2));
	}

	/**
	 * @return description concise d'un tronçon pour un affichage utilisateur
	 */
	public String affiche() {
		return nom + ", " + "sur " + longueur + "m";
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	public String toString() {
		String res = "Troncon{" + this.noeudOrigine.getId() + "->"
					  + this.noeudDestination.getId() + " " + " Nom = "
					  + this.nom + ", Longueur= " + this.longueur + "}";
		return res;
	}
}