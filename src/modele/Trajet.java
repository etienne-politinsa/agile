package modele;

import java.util.ArrayList;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class Trajet {
	/**
	 * le noeud duquel part le trajet
	 */
	final private Noeud noeudDepart;

	/**
	 * le noeud auquel arrive le trajet
	 */
	final private Noeud noeudArrivee;

	/**
	 * la longueur du trajet en mètres
	 */
	final private double longueur;

	/**
	 * la liste ordonnée des tronçons à emprunter pour aller de noeudDepart à
	 * noeudArivee
	 */
	final private ArrayList<Troncon> parcoursTroncons;

	/**
	 * @param parcoursTroncons la liste ordonnée des tronçons à emprunter pour
	 *                         aller de noeudDepart à noeudArivee
	 */
	public Trajet(ArrayList<Troncon> parcoursTroncons) {
		this.noeudDepart = parcoursTroncons.get(0).getNoeudOrigine();
		this.noeudArrivee = parcoursTroncons.get(parcoursTroncons.size() - 1)
							.getNoeudDestination();
		this.parcoursTroncons = parcoursTroncons;
		double longueurTemp = 0;
		for (Troncon t : parcoursTroncons) {
			longueurTemp += t.getLongueur();
		}
		this.longueur = longueurTemp;
	}

	/**
	 * @param longi la longitude angulaire depuis laquelle on calcule
	 * 				la distance
	 * @param lati  la latitude angulaire depuis laquelle on calcule
	 * 				la distance
	 * @return la distance angulaire à vol d'oiseau entre les coordonnées
	 *         données en paramètre et le milieu du tronçon du trajet le
	 *         plus proche des coordonnées données en paramètre
	 */
	public double calculDistanceEuclidienne(double longi, double lati) {
		double distanceMin = Double.MAX_VALUE;
		for (Troncon t : parcoursTroncons) {
			double distanceTempo = t.calculDistanceEuclidienne(longi, lati);
			if (distanceTempo < distanceMin) {
				distanceMin = distanceTempo;
			}
		}

		return distanceMin;
	}

	/**
	 * @return le noeud duquel part le trajet
	 */
	public Noeud getNoeudDepart() {
		return noeudDepart;
	}

	/**
	 * @return le noeud vers lequel arrive le trajet
	 */
	public Noeud getNoeudArrivee() {
		return noeudArrivee;
	}

	/**
	 * @return la liste ordonnée des tronçons à emprunter pour aller de
	 *         noeudDepart à noeudArivee
	 */
	public ArrayList<Troncon> getParcoursTroncons() {
		return parcoursTroncons;
	}

	/**
	 * @return la longueur du trajet en mètres
	 */
	public double getLongueur() {
		return longueur;
	}

	/**
	 * @return le temps de parcours du trajet en millisecondes
	 */
	public Long getTempsParcours() {
		return new Long(((long) (1000.
				*(this.longueur
						/ DemandeDeLivraison.VITESSE_LIVREUR_M_PAR_S))));
	}

	/**
	 * @return description concise d'un trajet pour un affichage utilisateur
	 */
	public String affiche() {
		String texte = " via un itinéraire de longueur " + longueur + " m\n";
		texte += "<ul>";
		for (Troncon t : parcoursTroncons) {
			texte += "<li>";
			texte += t.affiche() + "\n";
			texte += "</li>";
		}
		texte += "</ul>";

		return texte;
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	public String toString() {

		String renvoi = "Trajet{" + noeudDepart.getId() + "->"
						+ noeudArrivee.getId() + " : " + longueur + " par "
						+ parcoursTroncons.size() + " troncons[";
		for (Troncon t : parcoursTroncons)
			renvoi += t.toString();
		return renvoi;
	}
}