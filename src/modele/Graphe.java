package modele;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Florian MUTIN
 *
 */
public class Graphe {
	/**
	 * demandeDeLivraison
	 */
	private DemandeDeLivraison demandeDeLivraison;
	
	/**
	 * matrice qui contient les trajets entre les différents points de livraison
	 */
	private Trajet[][] matriceTrajet;
	
	/**
	 * plan
	 */
	private Plan plan;
	
	/**
	 * 
	 * @param plan
	 * @param demandeDeLivraison
	 */
	public Graphe(Plan plan, DemandeDeLivraison demandeDeLivraison){
		this.demandeDeLivraison = demandeDeLivraison;
		this.plan = plan;
		ArrayList<Noeud> noeuds = new ArrayList<Noeud>(this.demandeDeLivraison.getPointsDeLivraison());
		noeuds.add(0,this.demandeDeLivraison.getEntrepot());
		this.matriceTrajet = new Trajet[noeuds.size()][noeuds.size()];
		
		for(int i=0; i<noeuds.size(); i++){

			Noeud noeudCourant = noeuds.remove(i);
			Collection<Trajet> plusCourtsChemins = this.plan.calculerPlusCourtChemin(noeudCourant, noeuds);
			for(Trajet t : plusCourtsChemins){
				int indexDepart = i;
				Long idNoeudArrivee = t.getNoeudArrivee().getId();
				int indexArrivee = getIndexOf(idNoeudArrivee);
				matriceTrajet[indexDepart][indexArrivee] = t;
			}
			noeuds.add(i,noeudCourant);
		}
	}
	
	/**
	 * 
	 * @param id id d'un point de livraison
	 * @return l'index du noeud correspondant à l'id
	 */
	private int getIndexOf(Long id) {
		if(id==this.demandeDeLivraison.getEntrepot().getId())
			return 0;

		ArrayList<Noeud> noeuds = new ArrayList<Noeud>(this.demandeDeLivraison.getPointsDeLivraison());
		for(Noeud n : noeuds) {
			if(n.getId()==id)
				return getIndexOf(n);
		}

		return -1;
	}
	
	/**
	 * 
	 * @param noeud
	 * @return l'index du  noeud
	 */
	private int getIndexOf(Noeud noeud) {
		int index = this.demandeDeLivraison.getPointsDeLivraison().indexOf(noeud);
		if(index!=-1)
			return index+1;

		if(noeud.getId()==this.demandeDeLivraison.getEntrepot().getId())
			return 0;

		try {
			Noeud noeudDuPlan = this.plan.rechercheNoeudParId(noeud.getId());
			return getIndexOf(noeudDuPlan.getId());
		} catch(Exception e) {}

		return -1;
	}
	
	/**
	 * 
	 * @param i indice i
	 * @return le noeud correspondant à l'id
	 */
	public Noeud getNoeud(int i) {
		if(i==0) return this.demandeDeLivraison.getEntrepot();
		if(i>0 && i<this.demandeDeLivraison.getPointsDeLivraison().size()+1)
			return this.demandeDeLivraison.getPointsDeLivraison().get(i-1);
		
		return null;
	}
	
	/**
	 * 
	 * @param noeudDepart 
	 * @param noeudArrivee
	 * @return le trajet s'il existe entre le noeud de départ et d'arrivée, sinon null
	 */
	public Trajet getTrajet (Noeud noeudDepart, Noeud noeudArrivee) {
		int indexDepart = getIndexOf(noeudDepart);
		int indexArrivee = getIndexOf(noeudArrivee);
		if(indexDepart<0 || indexArrivee<0)
			return null;
		
		return matriceTrajet[indexDepart][indexArrivee];
	}
	
	/**
	 * 
	 * @param indexDepart
	 * @param indexArrivee
	 * @return le trajet entre un noeud d'index indexDepart et un noeud d'index indexArrivee
	 */
	public Trajet getTrajet(int indexDepart, int indexArrivee) {
		return matriceTrajet[indexDepart][indexArrivee];
	}
	
	/**
	 * 
	 * @return la matrice de trajets
	 */
	public Trajet[][] getMatriceTrajet () {
		return matriceTrajet;
	}
	
	/**
	 * 
	 * @return la demande de livraison
	 */
	public DemandeDeLivraison getDemandeDeLivraison() {
		return demandeDeLivraison;
	}
	
	/** 
	 * 
	 * @return le plan
	 */
	public Plan getPlan() {
		return plan;
	}
}