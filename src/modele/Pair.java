package modele;

/**
 * @author Etienne DELAHAYE
 *
 */
public class Pair<F, S> {
	/**
	 * un premier objet
	 */
	private F first;

	/**
	 * un second objet
	 */
	private S second;

	/**
	 * 
	 * @param second le second objet
	 */
	public void setSecond(S second) {
		this.second = second;
	}

	/**
	 * 
	 * @param first le first objet
	 */
	public void setFirst(F first) {
		this.first = first;
	}

	/**
	 * 
	 * @param o1 le premier objet
	 * @param o2 le second objet
	 */
	public Pair(F o1, S o2) {
		first = o1;
		second = o2;
	}

	/**
	 * 
	 * @return le premier objet
	 */
	public F getFirst() {
		return first;
	}

	/**
	 * 
	 * @return le second objet
	 */
	public S getSecond() {
		return second;
	}
}