package modele;
import java.util.ArrayList;
import java.util.Observable;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class Noeud extends Observable {
	/**
	 * l'identifiant du noeud
	 */
	private Long id;

	/**
	 * latitude du noeud
	 */
	private double latitude;

	/**
	 * longitude du noeud
	 */
	private double longitude;

	/**
	 * liste de tronçons qui partent du noeud
	 */
	private ArrayList<Troncon> tronconsPartants;

	public Noeud() {
		this.tronconsPartants = new ArrayList<Troncon>();
	}

	/**
	 * 
	 * @param id l'identifiant du noeud
	 * @param latitude latitude du noeud
	 * @param longitude longitude du noeud
	 * @param tronconsPartants liste de tronçons qui partent du noeud
	 */
	public Noeud(Long id, double latitude, double longitude, 
				ArrayList<Troncon> tronconsPartants) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		if (tronconsPartants!=null)
			this.tronconsPartants = tronconsPartants;
		else
			this.tronconsPartants = new ArrayList<Troncon>();
	}

	/**
	 * 
	 * @param longi une longitude
	 * @param lati une latitude
	 * @return la distance entre le point (longi,lati) et la positin du noeud
	 */
	public double calculDistanceEuclidienne(double longi, double lati) {
		return Math.sqrt(Math.pow(lati-latitude, 2)+Math.pow(longi-longitude, 2));
	}

	/**
	 * ajout t dans la liste de tronçons
	 * @param t un nouveau tronçon
	 */
	public void addTronconPartant(Troncon t) {
		this.tronconsPartants.add(t);
	}

	/**
	 * 
	 * @return l'identifiant du noeud
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id l'identifiant du noeud
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return latitude du noeud
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * 
	 * @param latitude latitude du noeud
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * 
	 * @return longitude du noeud
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * 
	 * @param longitude longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * 
	 * @return liste de tronçons qui partent du noeud
	 */
	public ArrayList<Troncon> getTronconsPartants() {
		return tronconsPartants;
	}

	/**
	 * 
	 * @param tronconsPartants liste de tronçons qui partent du noeud
	 */
	public void setTronconsPartants(ArrayList<Troncon> tronconsPartants) {
		this.tronconsPartants = tronconsPartants;
	}

	/**
	 * méthode d'affichage pour le débugage
	 */
	public String toString() {
		String res = "Noeud :";
		res+= ", id = "+ this.id;
		res+= ", latitude= " + this.latitude;
		res+= ", longitude= " + this.longitude;
		return res;
	}
}
