package modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Observable;

import tsp.TSP;
import tsp.TSP1;

/**
 * @author Florian MUTIN et Loïc CASTELLON
 *
 */
public class EnsembleDeTournees extends Observable{
	/**
	 * liste de tournées
	 */
	private ArrayList<Tournee> listeTournee;
	
	/**
	 * plan
	 */
	private Plan plan;
	
	/**
	 * demande de livraison
	 */
	private DemandeDeLivraison demandeDeLivraison;
	
	/**
	 * TSP
	 */
	private TSP tsp;
	
	/**
	 * @param plan
	 * @param demandeDeLivraison
	 */
	public EnsembleDeTournees(Plan plan, DemandeDeLivraison demandeDeLivraison){
		this.listeTournee = new ArrayList<Tournee>();
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		tsp = new TSP1();
		tsp.setEdt(this);
	}
	
	/**
	 * 
	 * @param tpsLimite temps à partir duquel le calcul des tournées doit s'arrêter
	 */
	public boolean calculer() {
		Graphe graphe = new Graphe(plan, demandeDeLivraison);
		tsp.chercheSolution(graphe);
		return true;
	}
	
	/**
	 * met à jour la meilleure solution 
	 */
	public void miseAJourMeilleureSolution() {
		listeTournee = tsp.getMeilleureSolution(this.demandeDeLivraison.getNombreLivreurs());
		setChanged();
		notifyObservers();
	}
	
	/**
	 * 
	 * @param longi 
	 * @param lati
	 * @return la tournée contenant le tronçon le plus proche du point (longi,lati)
	 */
	public Tournee getTourneeLaPlusProche(double longi, double lati) {
		double distanceMin = Double.MAX_VALUE;
		Tournee t = null;
		if(listeTournee.size()==0) return null;
		for	(Tournee tempo : listeTournee) {
			double distanceTempo = tempo.calculDistanceEuclidienne(longi, lati);
			if(distanceTempo<distanceMin) {
				t = tempo;
				distanceMin = distanceTempo;
			}
		}
		return t;
	}
	
	/**
	 * 
	 * @param pdl point de livraison
	 * @return la tournée qui contient le point de livraison
	 */ 
	public Tournee getTourneeParPointDeLivraison(PointDeLivraison pdl) {

		for (Tournee t: listeTournee) {
			for(Pair<PointDeLivraison, Trajet>  pair: t.getListePointTrajet()) {
				if (pair.getFirst()!=null && pair.getFirst().getId().equals(pdl.getId())) {
					return t;					
				}
			}
		}
		return null;
	}

	/**
	 * avancer un point de livraison dans l'ordre de passage de sa tournée
	 * @param pdl le point de livraison à avancer
	 */
	public void avancerPointDeLivraison(PointDeLivraison pdl) throws Exception {
		Tournee t = getTourneeParPointDeLivraison(pdl);
		t.avancer(pdl,plan,this.demandeDeLivraison.getEntrepot());
		setChanged();
		notifyObservers();
	}

	/**
	 * reculer un point de livraison dans l'ordre de passage de sa tournée
	 * @param pdl le point de livraison à reculer
	 */
	public void reculerPointDeLivraison(PointDeLivraison pdl) throws Exception {
		Tournee t = getTourneeParPointDeLivraison(pdl);
		t.reculer(pdl,plan,this.demandeDeLivraison.getEntrepot());
		setChanged();
		notifyObservers();
	}

	/**
	 * @param index l'index de la tournée à sélelectionner
	 * @param sel true pour sélectionner, false sinon
	 */
	public void setEstSelectionne(int index, boolean sel) {
		if(listeTournee.size()!=0)
			listeTournee.get(index).setEstSelectionne(sel);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * 
	 * @param t une tournée
	 * @return la position de la tournée dans l'ensemble de tournée
	 */
	public int getIndex(Tournee t) {
		for(int i=0;i<listeTournee.size();i++) {
			if(listeTournee.get(i).getLivreur()==t.getLivreur())
				return i;
		}
		return -1;
	}

	/**
	 * Ajoute le noeud en fin de tournée
	 * @param noeud noeud à ajouter
	 * @param dureeEnMin durée de point de livraison
	 * @param plan plan
	 * @param tournee tournée à modifier
	 * @throws Exception si l'ajout n'est pas possible
	 */
	public PointDeLivraison ajouterPointDeLivraison(Noeud noeud, int dureeEnMin, Plan plan, Tournee tournee) throws Exception {

		PointDeLivraison pdl = tournee.ajouterPointDeLivraison(noeud, dureeEnMin, plan);
		if(pdl==null)
			throw new Exception();
		else 
			demandeDeLivraison.getPointsDeLivraison().add(pdl);

		//notification des observateurs
		setChanged();
		notifyObservers();
		return pdl;
	}
	
	/**
	 * Ajoute le noeud dans une nouvelle tournée
	 * @param noeud noeud à ajouter
	 * @param dureeEnMin durée de point de livraison
	 * @param plan plan
	 * @param entrepot 
	 * @throws Exception
	 */
	public PointDeLivraison ajouterPointDeLivraisonNouvelleTournee(Noeud noeud, int dureeEnMin, Plan plan, Entrepot entrepot, Tournee t) throws Exception {
		double longueur = 0;
		int livreur = 1;
		ArrayList<Pair<PointDeLivraison,Trajet>> listePointTrajet = new ArrayList<Pair<PointDeLivraison,Trajet>>();

		//calcul des trajets 
		Trajet t1;
		Trajet t2;
		try {
			t1 = (plan.calculerPlusCourtChemin(entrepot,new ArrayList<Noeud>(Arrays.asList(noeud)))).get(0);
			t2 = (plan.calculerPlusCourtChemin(noeud,new ArrayList<Noeud>(Arrays.asList(entrepot)))).get(0);
		}
		catch (Exception e){
			throw new Exception();
		}
		PointDeLivraison pdl = new PointDeLivraison(noeud,dureeEnMin);
		Pair<PointDeLivraison,Trajet> p1 = new Pair<PointDeLivraison,Trajet>(pdl,t1);
		Pair<PointDeLivraison,Trajet> p2 = new Pair<PointDeLivraison,Trajet>(null,t2);
		longueur +=  p1.getSecond().getLongueur();
		longueur +=  p2.getSecond().getLongueur();
		pdl.setHeurePassage(new Date(t1.getTempsParcours()));
		listePointTrajet.add(p1);
		listePointTrajet.add(p2);
		Tournee tournee = new Tournee(longueur,livreur,listePointTrajet);
		t.setListePointTrajet(tournee.getListePointTrajet());
		t.setLivreur(tournee.getLivreur());
		t.setLongueur(tournee.getLongueur());
		
		listeTournee.add(t);
		demandeDeLivraison.getPointsDeLivraison().add(pdl);

		//notification des observateurs
		setChanged();
		notifyObservers();
		return pdl;
	}

	/**
	 * 
	 * @param pdl point de livraison à supprimer
	 * @param plan plan
	 * @param tournee tournée à modifier
	 */
	public void supprimerPointDeLivraison(PointDeLivraison pdl, Plan plan, Tournee tournee) throws Exception {
		if(tournee==null) 
			throw new Exception();
		
		if(tournee.getListePointTrajet().size()<3) {
			listeTournee.remove(tournee);
			tournee.getListePointTrajet().clear();
		}
		else {
			tournee.supprimerPointDeLivraison(pdl, plan);
		}
		demandeDeLivraison.remove(pdl);
		
		//notification des observateurs
		setChanged();
		notifyObservers();
	}
	
	/**
	 * 
	 * @return l'ensemble des tournées
	 */
	public ArrayList<Tournee> getListeTournee() {
		return listeTournee;
	}
	
	/**
	 * 
	 * @param listeTournee l'ensemble des tournées
	 */
	public void setListeTournee(ArrayList<Tournee> listeTournee) {
		this.listeTournee = listeTournee;
		setChanged();
		notifyObservers();		
	}
	
	/**
	 * 
	 * @param b true pour arrêter, false pour continuer
	 */
	public void stopCalcul(boolean b) {
		
		tsp.setObtenirMeilleureSolution(b);
	}
	
	/**
	 * 
	 * @return true si l'ensemble des tournées est déséquilibrée, false sinon
	 */
	public boolean isDesequilibre() {
		if(listeTournee.size()==0) 
			return false;
		int nbPdl = listeTournee.get(0).getListePointTrajet().size();
		for(Tournee t : listeTournee) {
			int tempo = t.getListePointTrajet().size();
			if(Math.abs(tempo-nbPdl)>1)
				return true;
			nbPdl = tempo;
		}
		return false;
	}

	/**
	 * 
	 * @return description concise d'un tronçon pour un affichage utilisateur
	 */
	public String affiche() {
		String texte = "EnsembleDeTournees[\n" +
				demandeDeLivraison.getEntrepot().affiche() + "\n";
		for(Tournee t : listeTournee)
			texte += t.affiche(demandeDeLivraison) + "\n";

		return texte ;
	}
}
