package agile;

import controleur.Controleur;
import modele.*;

public class Agile {

	/**
	 * largeur plan par défaut
	 */
	public static final int largeurPlan = 50;

	/**
	 * hauteur plan par défaut
	 */
	public static final int hauteurPlan = 50;

	/**
	 * Lancement de l'application
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Plan plan = new Plan(largeurPlan, hauteurPlan);		
			DemandeDeLivraison demandeDeLivraison = new DemandeDeLivraison();	
			EnsembleDeTournees edt = new EnsembleDeTournees(plan,demandeDeLivraison);
			new Controleur(plan, demandeDeLivraison, edt);
		} catch (Exception e) {}
	}
}
