package tsp;

import java.util.ArrayList;

import modele.EnsembleDeTournees;
import modele.Graphe;
import modele.Tournee;

public interface TSP {
	
	public void setEdt(EnsembleDeTournees edt);
		
	
	/**
	 * Cherche un circuit de duree minimale passant par chaque sommet du graphe
	 * @param graphe : le graphe qui contient les trajets entre chaques sommets
	 */
	public void chercheSolution(Graphe graphe);
	
	public Integer[] meilleureSolution();
	
	/**
	 * @param nbLivreurs : nombre de livreurs disponibles
	 * @return la tournée solution calculee par chercheSolution pour chaque livreur
	 */
	public ArrayList<Tournee> getMeilleureSolution(int nbLivreurs);
	

	public void setObtenirMeilleureSolution(boolean b);
}
