package tsp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;


import modele.Graphe;
import modele.Pair;
import modele.PointDeLivraison;
import modele.Tournee;
import modele.Trajet;
import modele.EnsembleDeTournees;

/**
 * @author Doudou
 *
 */
public abstract class TemplateTSP implements TSP {

	/**
	 * tableau contenant les indices de passage des livreurs dans l'ordre croissant
	 */
	private Integer[] meilleureSolution;
	/**
	 * longueur de la meilleure tournée
	 */
	private double coutMeilleureSolution = 0;
	/**
	 * booléen vrai si le temps limite est dépassé, faux sinon
	 */
	private Boolean tempsLimiteAtteint;
	/**
	 * nombre de livraison moyen par livreurs
	 */
	private int nbLivraisonsParLivreur;
	/**
	 * correspond a l'excédent de points de livraisons par rapport au nombre moyen
	 */
	private int excedentLivraisonsParLivreur;
	/**
	 * contient les trajets correspondant aux plus courts chemin entre les points de livraison
	 */
	Graphe graphe;
	/**
	 * vrai si le calcul doit être arrêté, faux sinon
	 */
	private boolean obtenirMeilleureSolution = false;
	/**
	 * contient les tournées pour chacun des livreurs
	 */
	EnsembleDeTournees edt;
	
	public void setEdt(EnsembleDeTournees edt) {
		this.edt = edt;
	}

	public void setObtenirMeilleureSolution(boolean unBoolean) {

		this.obtenirMeilleureSolution = unBoolean;

	}
	public Integer[] meilleureSolution() {
		return this.meilleureSolution;
	}
	public void chercheSolution(Graphe graphe){
		this.obtenirMeilleureSolution = false;
		this.graphe = graphe;
		ArrayList<PointDeLivraison> pointsDeLivraison = graphe.getDemandeDeLivraison().getPointsDeLivraison();
		int nbLivraisons = graphe.getDemandeDeLivraison().getNombreLivreurs();
		int nbSommets = pointsDeLivraison.size() + 1+ nbLivraisons; //+1 pour l'entrepot

		int[] duree = new int[nbSommets];
		duree[0] = 0;
		for(int i = 0 ; i<pointsDeLivraison.size() ; i++)
		{
			duree[i+1] = pointsDeLivraison.get(i).getDuree();
		}

		double[][] cout = new double[nbSommets][nbSommets];
		for(int i = 0 ; i<nbSommets ; i++)
		{
			for(int j = 0 ; j<nbSommets ; j++)
			{	
				Trajet t;
				if(i>=nbSommets-nbLivraisons && j<nbSommets-nbLivraisons ) {
					t = graphe.getTrajet(0, j);
				} 	else if (i<nbSommets-nbLivraisons && j>=nbSommets-nbLivraisons) {
					t = graphe.getTrajet(i, 0);

				}else if (i>=nbSommets-nbLivraisons && j>=nbSommets-nbLivraisons) {
					t = graphe.getTrajet(0, 0);

				}else {
					t = graphe.getTrajet(i, j);
				}
				if(t!=null) {
					cout[i][j] = t.getLongueur();
				}
			}
		}		

		chercheSolution(nbSommets, cout, duree);
	}

	/**
	 * @param tpsLimite : le temps maximal d'exécution
	 * @param nbSommets : le nombre de points de livraison + entrepot + entrepots fictifs
	 * @param cout : cout[i][j] contient le cout du chemin de i vers j
	 * @param duree : duree[j] contient la duree de la livraison au point j
	 */
	protected void chercheSolution(int nbSommets, double[][] cout, int[] duree){
		tempsLimiteAtteint = false;
		coutMeilleureSolution = Double.MAX_VALUE;
		meilleureSolution = new Integer[nbSommets];
		int nbRetourEntrepotRestant = graphe.getDemandeDeLivraison().getNombreLivreurs();
		nbLivraisonsParLivreur = (int)graphe.getDemandeDeLivraison().getPointsDeLivraison().size()/nbRetourEntrepotRestant;
		excedentLivraisonsParLivreur = graphe.getDemandeDeLivraison().getPointsDeLivraison().size()%nbRetourEntrepotRestant;
		ArrayList<Integer> nonVus = new ArrayList<Integer>();
		ArrayList<Integer> nonVusSansEntrepot = new ArrayList<Integer>();
		ArrayList<Integer> EntrepotsFictifsnonVus = new ArrayList<Integer>();
		for (int i=1; i<nbSommets; i++) nonVus.add(i);
		for (int i=1; i<nbSommets-nbRetourEntrepotRestant; i++) nonVusSansEntrepot.add(i);
		for(int i = nbSommets-nbRetourEntrepotRestant;i<nbSommets;i++)EntrepotsFictifsnonVus.add(i);
		ArrayList<Integer> vus = new ArrayList<Integer>(nbSommets);
		vus.add(0); // le premier sommet visite est 0
		branchAndBound(0, nonVus,nonVusSansEntrepot,EntrepotsFictifsnonVus, vus, 0, cout, duree,0);

	}


	/**
	 * @param livreur : le numéro du livreur de la tournée étudiée
	 * @param dernierEntrepotAtteint : l'indice du dernier entrepot assigné à un livreur précédent
	 * @return une paire contenant la tournée du livreur étudié, et l'indice du dernier entrepot atteint
	 */
	private Pair<Tournee,Integer> getMeilleureSolution(int livreur,int dernierEntrepotAtteint) {

		ArrayList<Pair<PointDeLivraison,Trajet>> listePointTrajet = new ArrayList<Pair<PointDeLivraison,Trajet>>();
		Long date = graphe.getDemandeDeLivraison().getEntrepot().getHeureDepart().getTime();
		ArrayList<PointDeLivraison> pointsDeLivraison = graphe.getDemandeDeLivraison().getPointsDeLivraison();
		int nbLivraisons = graphe.getDemandeDeLivraison().getNombreLivreurs();
		int nbSommets = pointsDeLivraison.size() + 1+ nbLivraisons; //+1 pour l'entrepot
		double longueur = 0;
		Trajet t = graphe.getTrajet(0, meilleureSolution[dernierEntrepotAtteint]);
		date+=t.getTempsParcours();
		PointDeLivraison p = (PointDeLivraison) graphe.getNoeud(meilleureSolution[dernierEntrepotAtteint]);
		p.setHeurePassage(new Date(date));
		date+=p.getDuree()*1000;
		listePointTrajet.add(new Pair<PointDeLivraison,Trajet>(p,t));
		longueur = longueur + t.getLongueur();
		while(meilleureSolution[dernierEntrepotAtteint+1]<nbSommets-nbLivraisons) {
			t = graphe.getTrajet(meilleureSolution[dernierEntrepotAtteint], meilleureSolution[dernierEntrepotAtteint+1]);
			date+=t.getTempsParcours();
			p = (PointDeLivraison) graphe.getNoeud(meilleureSolution[dernierEntrepotAtteint+1]);
			p.setHeurePassage(new Date(date));
			date+=p.getDuree()*1000;
			listePointTrajet.add(new Pair<PointDeLivraison,Trajet>(p,t));
			dernierEntrepotAtteint++;
			longueur = longueur + t.getLongueur();
		}

		t = graphe.getTrajet(meilleureSolution[dernierEntrepotAtteint],0);
		date+=t.getTempsParcours();
		dernierEntrepotAtteint = dernierEntrepotAtteint+2;
		listePointTrajet.add(new Pair<PointDeLivraison,Trajet>(null,t));
		longueur = longueur + t.getLongueur();
		return new Pair<Tournee,Integer>(new Tournee(longueur, livreur, listePointTrajet),new Integer(dernierEntrepotAtteint));
	}

	public ArrayList<Tournee> getMeilleureSolution(int nbLivreurs) {

		ArrayList<Tournee> tournees = new ArrayList<Tournee>();
		int j = 1;
		Pair<Tournee,Integer> MeilleureSolutionParLivreur;
		for(int i=0 ; i<nbLivreurs ;i++) {
			MeilleureSolutionParLivreur = getMeilleureSolution(i,j);
			tournees.add(MeilleureSolutionParLivreur.getFirst());
			j=MeilleureSolutionParLivreur.getSecond();

		}
		return tournees;
	}




	/**
	 * @param sommetCourant : le sommet en cours d'étude
	 * @param nonVus : la liste des sommets non vus 
	 * @param EntrepotFictifs : la liste des entrepots fictifs non vus
	 * @param cout 
	 * @param duree
	 * @return
	 */
	protected abstract double bound(Integer sommetCourant, ArrayList<Integer> nonVus,ArrayList<Integer> EntrepotFictifs, double[][] cout);

	/**
	 * @param sommetCrt
	 * @param nonVus
	 * @param cout
	 * @param duree
	 * @return
	 */
	protected abstract Iterator<Integer> iterator(Integer sommetCrt, ArrayList<Integer> nonVus, double[][] cout, int[] duree);


	/**
	 * @param a : un double
	 * @param b : un double
	 * @return : -1 si a>b, 1 si a>b, 0 sinon
	 */
	private int comparateurDeDistances(double a, double b) {

		if(a<b) {

			return -1;
		}else if(a>b) {

			return 1;	
		}
		return 0;

	}
	
	/**
	 * @param Vus : la liste des sommets vus
	 * @param borneEntrepot : la valeur minimale de l'indice d'un entrepot
	 * @param sommetCourant : le sommet en cours d'étude
	 * @return vrai si la le sommet étudié est supérieur au dernier sommet suivant un entrepot
	 */
	private boolean solutionCorrecte(ArrayList<Integer> Vus, int borneEntrepot,int sommetCourant) {
		if(Vus.size()==1) {return true;}
		int sommetApresEntrepot=0;
		for(int i = Vus.size()-2;i>=0;i--) {
			if(Vus.get(i)>borneEntrepot) {
				sommetApresEntrepot = Vus.get(i+1);
				break;
			}
		}
		if(sommetCourant > sommetApresEntrepot) {
			return true;
		}else {
			return false;
		}
		
	}

	/**
	 * @param sommetCrt
	 * @param nonVus 
	 * @param nonVusSansEntrepot : la liste des sommets non vus, ne contenant pas les entrepots non vus
	 * @param EntrepotsFictifsNonVus : la liste des entrepots fictifs non vus
	 * @param vus
	 * @param coutVus : le cout du trajet de la solution actuelle
	 * @param cout
	 * @param duree
	 * @param nbSommetsVusDepuisEntrepot : le nombre de sommets parcourus depuis le passage d'un entrepot fictif
	 */
	void branchAndBound(int sommetCrt, ArrayList<Integer> nonVus,ArrayList<Integer> nonVusSansEntrepot,ArrayList<Integer> EntrepotsFictifsNonVus, ArrayList<Integer> vus, double coutVus, double[][] cout, int[] duree, int nbSommetsVusDepuisEntrepot){
		if(obtenirMeilleureSolution==true) {return;}
		if (nonVus.size() == 0){ 
			
			coutVus += cout[sommetCrt][0];
			if (coutVus < coutMeilleureSolution){ 
				vus.toArray(meilleureSolution);
				coutMeilleureSolution = coutVus;
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							edt.miseAJourMeilleureSolution();
						} catch (Exception e) {}
					}
				}).start();
			}


		} else if (coutVus + bound(sommetCrt, nonVusSansEntrepot,EntrepotsFictifsNonVus, cout) < coutMeilleureSolution){
			Iterator<Integer> it ;
			Collections.sort(nonVus, (a,b) ->  comparateurDeDistances(cout[sommetCrt][a],cout[sommetCrt][b]));
			if(excedentLivraisonsParLivreur!=0) {
				if(nbSommetsVusDepuisEntrepot==nbLivraisonsParLivreur+1) {
					it = iterator(sommetCrt, EntrepotsFictifsNonVus, cout, duree);
					if(it.hasNext()) {
						Integer prochainSommet = it.next();
						excedentLivraisonsParLivreur--;
						vus.add(prochainSommet);
						nonVus.remove(prochainSommet);
						EntrepotsFictifsNonVus.remove(prochainSommet);
						branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree, 0);
						EntrepotsFictifsNonVus.add(prochainSommet);
						vus.remove(prochainSommet);
						nonVus.add(prochainSommet);
						excedentLivraisonsParLivreur++;
					}




				}else if(nbSommetsVusDepuisEntrepot==nbLivraisonsParLivreur){
					it = iterator(sommetCrt, nonVus, cout, duree);
					while (it.hasNext()){
						Integer prochainSommet = it.next();
						if(EntrepotsFictifsNonVus.indexOf(prochainSommet)!=-1) {
							EntrepotsFictifsNonVus.remove(prochainSommet);
							vus.add(prochainSommet);
							nonVus.remove(prochainSommet);
							branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree, 0);
							EntrepotsFictifsNonVus.add(prochainSommet);
						}else {
							nonVusSansEntrepot.remove(prochainSommet);
							vus.add(prochainSommet);
							nonVus.remove(prochainSommet);
							branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree,nbSommetsVusDepuisEntrepot+1);
							nonVusSansEntrepot.add(prochainSommet);
						}

						vus.remove(prochainSommet);
						nonVus.add(prochainSommet);

					}	




				}else{
					it = iterator(sommetCrt, nonVusSansEntrepot, cout,duree);

					while (it.hasNext()){
						Integer prochainSommet = it.next();
						
						if(nbSommetsVusDepuisEntrepot==0) {
							if(solutionCorrecte(vus,graphe.getDemandeDeLivraison().getPointsDeLivraison().size(),prochainSommet)) {
					
								vus.add(prochainSommet);
								nonVus.remove(prochainSommet);
								nonVusSansEntrepot.remove(prochainSommet);
								branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree, nbSommetsVusDepuisEntrepot+1);
								vus.remove(prochainSommet);
								nonVus.add(prochainSommet);
								nonVusSansEntrepot.add(prochainSommet);
	
							}else {
								
								
								return;
							}
						}else {
							vus.add(prochainSommet);
							nonVus.remove(prochainSommet);
							nonVusSansEntrepot.remove(prochainSommet);
							branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree,nbSommetsVusDepuisEntrepot+1);
							nonVusSansEntrepot.add(prochainSommet);
							vus.remove(prochainSommet);
							nonVus.add(prochainSommet);
						}


					}	





				}


			}else {
				if(nbSommetsVusDepuisEntrepot==nbLivraisonsParLivreur){
					it = iterator(sommetCrt, EntrepotsFictifsNonVus, cout, duree);
					if(it.hasNext()) {
						Integer prochainSommet = it.next();
						vus.add(prochainSommet);
						nonVus.remove(prochainSommet);
						EntrepotsFictifsNonVus.remove(prochainSommet);
						branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree, 0);
						EntrepotsFictifsNonVus.add(prochainSommet);
						vus.remove(prochainSommet);
						nonVus.add(prochainSommet);
					}


				}else{

					it = iterator(sommetCrt, nonVusSansEntrepot, cout,duree);
					while (it.hasNext()){
						Integer prochainSommet = it.next();
						if(nbSommetsVusDepuisEntrepot==0) {
							if(solutionCorrecte(vus,graphe.getDemandeDeLivraison().getPointsDeLivraison().size(),prochainSommet)) {
								
					
								vus.add(prochainSommet);
								nonVus.remove(prochainSommet);
								nonVusSansEntrepot.remove(prochainSommet);
								branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree, nbSommetsVusDepuisEntrepot+1);
								vus.remove(prochainSommet);
								nonVus.add(prochainSommet);
								nonVusSansEntrepot.add(prochainSommet);
	
							}else {
								return;
							}
						}else {
							vus.add(prochainSommet);
							nonVus.remove(prochainSommet);
							nonVusSansEntrepot.remove(prochainSommet);
							branchAndBound(prochainSommet, nonVus,nonVusSansEntrepot,EntrepotsFictifsNonVus, vus, coutVus + cout[sommetCrt][prochainSommet] + duree[prochainSommet], cout, duree,nbSommetsVusDepuisEntrepot+1);
							vus.remove(prochainSommet);
							nonVus.add(prochainSommet);
							nonVusSansEntrepot.add(prochainSommet);
						}


					}	


				}


			}
		}
	}


}
