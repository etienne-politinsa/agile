package tsp;

import java.util.ArrayList;
import java.util.Iterator;

public class TSP1 extends TemplateTSP {
	protected int dernierSommetAvantFictif=0;
	
	
	
	@Override
	protected Iterator<Integer> iterator(Integer sommetCrt, ArrayList<Integer> nonVus, double[][] cout, int[] duree) {
		return new IteratorSeq(nonVus, sommetCrt);
		
	}

	@Override
	protected double bound(Integer sommetCourant, ArrayList<Integer> nonVusSansEntrepots,ArrayList<Integer> EntrepotFictifs, double[][] cout) {
		
		double sommeMin = 0;
		double minTempo;
		double minRetourEntrepot = Double.MAX_VALUE;
		for(int i = 0;i<nonVusSansEntrepots.size();i++) {
			
			minTempo = cout[nonVusSansEntrepots.get(i)][0];
			if(cout[0][nonVusSansEntrepots.get(i)]<minRetourEntrepot) {
				
				minRetourEntrepot = cout[0][nonVusSansEntrepots.get(i)];
				
			}
			for(int j = 0;j<nonVusSansEntrepots.size();j++) {
				
				if(cout[nonVusSansEntrepots.get(i)][nonVusSansEntrepots.get(j)]<minTempo && i!=j) {
					
					minTempo = cout[nonVusSansEntrepots.get(i)][nonVusSansEntrepots.get(j)];
				}
				
			}
			sommeMin = sommeMin + minTempo;
			
		}
		sommeMin = sommeMin + (EntrepotFictifs.size()-1)*minRetourEntrepot;
		return sommeMin;
	}
}
