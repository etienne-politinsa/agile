# PLD_AGILE
Application capable d'afficher un plan tiré d'openstreetmap, de créer un graphe à partir de celui-ci, de l'afficher, et de calculer des trajets (livraisons) entre plusieurs points de ce plan.


Consignes
=========
NE PAS PUSH DES FICHIERS CONTENANT DES ERREURS

*Il faut run les tests avant de commit et de push*

Pour run les tests, changer la variable gloable dans le fichier src.tests.DeserialiseurXMLPlanTest



Installation
============

Le repos git est à l'adresse suivante https://gitlab.com/etienne-politinsa/agile.git

Pour l'utiliser: créer un projet Eclipse Java dans votre workspace perso.
Supprimer le package "src" s'il est présent.

Cloner où vous le voulez le repos git.

Sous Eclipse, click droit sur le projet, Build Path -> Link Source, sélectionner le dossier que vous avez récemment cloné (Agile/src) -> Finish


# PLD_AGILE

Lien moodle : https://moodle.insa-lyon.fr/course/view.php?id=4023  

On développe en JAVA  
**JAVA Swing**  
Eclipse *dernière version*  
